//////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2004 Mellanox Technologies LTD. All rights reserved.
//
// This software is available to you under a choice of one of two
// licenses.  You may choose to be licensed under the terms of the GNU
// General Public License (GPL) Version 2, available from the file
// COPYING in the main directory of this source tree, or the
// OpenIB.org BSD license below:
//
//     Redistribution and use in source and binary forms, with or
//     without modification, are permitted provided that the following
//     conditions are met:
//
//      - Redistributions of source code must retain the above
//        copyright notice, this list of conditions and the following
//        disclaimer.
//
//      - Redistributions in binary form must reproduce the above
//        copyright notice, this list of conditions and the following
//        disclaimer in the documentation and/or other materials
//        provided with the distribution.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////////
//
// The IBSink implements an IB endport FIFO that is filled by the push
// message and drained by the pop self-message.
// To simulate hiccups in PCI Exp bus, we schedule self-messages of type hiccup
// hiccup message alternate between an ON and OFF state. During ON state any
// drain message is ignored. On transition to OFF a new drain message can
// be generated
//
#include "ib_m.h"
#include "sink.h"

Define_Module( IBSink );

void IBSink::initialize()
{
    Q_packets = new cQueue*[SRC_IDS];

    for (int msg = 0; msg < SRC_IDS; msg++) {
        Q_packets[msg] = new cQueue[MSG_SAMPLES];
     }
    outOfOrder.setName("Out of order");
    waitStats.setName("Waiting time statistics");
	hiccupStats.setName("Hiccup Statistics");
	maxVL = par("maxVL");
	creditsNum = 0;
	packetsNum = 0;

	sequenceBreak = false;

	PakcetFabricTime.setName("Packet Fabric Time");
	PakcetFabricTime.setRangeAutoUpper(0, 10, 1.5);

    MeasuredBW.setName("Measured BW");
    ActualBW.setName("Actual BW");

    MeasuredBWStats.setName("Measured BW Vector");
    ActualBWStats.setName("Actual BW Vector");
    jitterTimeStats.setName("Credits Jitter Vector");

    jitterPacketTime.setName("Credits Jitter");

    IntervalBWStats.setName("Interval BW Vector");
    lastIntLogAccBytes = 0;
    lastIntLogTime = 0;
    logInterval = par("logInterval");

	// calculate the drain rate
	creditSize = par("creditSize");
	pciExpWidth = par("pciExpWidth"); // 1, 4, 8
	pciExpTransferRate = par("pciExpTransferRate"); // 2.5, 5.0, 8.0
	// drain rate is the num usec to drain one bit
	// =10 bits/byte * usec/sec / PCIExp Freq / NumLanes * 105% [usec/bit]
	byteDrainRate_us = 10 *1.0e6 / (pciExpTransferRate*1e9) / pciExpWidth * 1.05;
	WATCH(byteDrainRate_us);

	// we will allocate a drain messgae only on the first credit getting in
	// which consumed immediatly...
	p_drainMsg = NULL;
	FirstPktRcvTime = 0;
	firstPacketRcv = 1;
	AccBytesRcv = 0;
    LastBytesRcv = 0;
    prevTime = simTime();

	duringHiccup = 0;
	WATCH(duringHiccup);

	p_hiccupMsg = new cMessage("pop");
	p_hiccupMsg->setKind(IB_HICCUP_MSG);
	scheduleAt(simTime()+1e-9, p_hiccupMsg);

    // Start interval logging
    scheduleAt(simTime() + logInterval, new cMessage(NULL, IB_LOG_TIMER_MSG));

   // we track number of packets per VL:
   for (int vl = 0; vl < maxVL+1; vl++)
		VLPackets.push_back(0);

	WATCH_VECTOR(VLPackets);
}

// Init a new drain message and schedule it after delay
void IBSink::newDrainMessage(simtime_t delay_us) {
	p_drainMsg = new cMessage("pop", IB_POP_MSG);

	// we track the start time so we can hiccup left over...
	p_drainMsg->setTimestamp(simTime());
	scheduleAt(simTime()+delay_us*1e-6, p_drainMsg);
}

// track consumed messages and send "sent" event to the IBUF
void IBSink::consumeDataMsg(IBDataMsg *p_msg)
{
	IBDataMsg *p_dataMsg;
	creditsNum++;
	if (! ev.isDisabled())
		ev << "-I- " << getFullPath() << " consumed data:" << p_msg->getName() << endl;

	// track the absolute time this packet was consumed
	lastConsumedPakcet = simTime();
	const char* packet_name = p_msg->getName();
	int msgId = p_msg->getMsgId();
	if(p_msg->getCreditSn() == 0) {
	    firstCreditGenTime = p_msg->getTimestamp();
	}
	// track the time this credit waited in the HCA
   simtime_t d = lastConsumedPakcet - p_msg->getTimestamp();
   ev << "-I- " << getFullPath() << " packet time " << d << endl;
   waitStats.collect( d );


    ev << "-I- " << getFullPath() << " msg remain " << p_msg->getMsgRemaining()
        << " msg size " << p_msg->getMsgSize() << endl;

    //track the time this packet spent on the wire
    if(p_msg->getCreditSn() + 1 == p_msg->getPacketLength()) {
        d = lastConsumedPakcet - firstCreditGenTime;
        PakcetFabricTime.collect( d );

        double mBW;

        mBW = p_msg->getMsgSize() / (p_msg->getTimestamp() - p_msg->getMsgTimestamp());
        MeasuredBW.collect(mBW);
        MeasuredBWStats.record(mBW);
        ev << "-I- " << getFullPath() << " msg time " << d << " msgbw " << mBW << endl;

        //track the actual bandwidth
        double aBW;
        aBW = LastBytesRcv / (simTime() - prevTime);

        ActualBW.collect(aBW);
        ActualBWStats.record(aBW);
        LastBytesRcv = 0;
        prevTime = simTime();

        prevCreditJitterTime = lastConsumedPakcet - firstCreditGenTime;
        packetsNum++;
        // new packet

        IBDataMsg* tmpPck = new IBDataMsg(packet_name, IB_DATA_MSG);
        tmpPck->setPacketId(p_msg->getPacketId());
        tmpPck->setSrcLid(p_msg->getSrcLid());
        tmpPck->setDstLid(p_msg->getDstLid());
        tmpPck->setMsgTimestamp(d);
        if(msgId<MSG_SAMPLES)
            Q_packets[p_msg->getSrcLid() - 1][msgId].insert(tmpPck);



 //       d = lastConsumedPakcet - p_msg->getMsgTimestamp();
 //       PakcetFabricTime.collect( d );
        // collect out-of-order packets per sink, per message
        //   after that clean the packets destinated to this sink
    }
    if (msgId < MSG_SAMPLES) {
        int length = Q_packets[p_msg->getSrcLid() - 1][msgId].length() - 1;
        if (length == 31) {
            double outOfOrder_msg = 0;

            for (int i = 0; i < length; i++) {
                IBDataMsg* curPckt =
                        (IBDataMsg*) Q_packets[p_msg->getSrcLid() - 1][msgId].get(
                                i);
                IBDataMsg* nextPckt =
                        (IBDataMsg*) Q_packets[p_msg->getSrcLid() - 1][msgId].get(
                                i + 1);
                if (nextPckt->getPacketId() < curPckt->getPacketId()) {
                    sequenceBreak = true;
                    const char* moduleName =
                            getParentModule()->getFullName();
                    outOfOrder_msg++;
                    outOfOrder.collect(outOfOrder_msg / 32);

                } else if(!sequenceBreak){

                    creditJitterTime =  nextPckt->getMsgTimestamp() - curPckt->getMsgTimestamp();
                    double valTime =(creditJitterTime.dbl()>=0)?creditJitterTime.dbl():-1*creditJitterTime.dbl();
                    if(valTime !=0) {
                        jitterPacketTime.collect(valTime);
                        jitterTimeStats.record(valTime);
                    }
//                    packetJitterTime =  currentCreditJitterTime - prevCreditJitterTime;
//                    double valTime =(creditJitterTime.dbl()>=0)?creditJitterTime.dbl():-1*creditJitterTime.dbl();
//                    if(valTime !=0) {
//                        jitterPacketTime.collect(valTime);
//                        jitterTimeStats.record(valTime);
//                    }
//                    prevCreditJitterTime = currentCreditJitterTime;
                }
            }

            while (Q_packets[p_msg->getSrcLid() - 1][msgId].length() > 0) {
                IBDataMsg* curPckt =
                        (IBDataMsg*) Q_packets[p_msg->getSrcLid() - 1][msgId].get(
                                0);
                Q_packets[p_msg->getSrcLid() - 1][msgId].remove(curPckt);
            }
        }

    }
//    if ((p_msg->getCreditSn() + 1) == p_msg->getPacketLength() && packetsNum == 32) {
//        double outOfOrder_msg = 0;
//        packetsNum = 0;
//    }
   // track the time this message spent on the wire...
//   if (p_msg->getMsgRemaining() == 0 && p_msg->getCreditSn() + 1 == p_msg->getPacketLength())
//   {
//       //track the measured bandwidth of this message
//
//
//
//
//   }

	double iBW = AccBytesRcv / (simTime() - FirstPktRcvTime);
	ev << "STAT: " << getFullPath() << " Sink input BW (B/s):" << iBW  << endl;

	ev << "STAT: " << getFullPath() << " Actual BW: num/avg/max/std:"
		<< ActualBW.getCount() << " / " << ActualBW.getMean() << " / "
		<< ActualBW.getMax() << " / "  << ActualBW.getStddev() << endl;

	int vl = p_msg->getVL();
	VLPackets[vl]++;

	if (0) {
		if (p_msg->getPacketLength() == p_msg->getCreditSn() + 1)
		{
			for (int sn = p_msg->getPacketLength(); sn != 0; sn--)
			{
				// send the IBUF a "sent" messgae
				IBSentMsg *p_sentMsg = new IBSentMsg("hca_sent", IB_SENT_MSG);
				p_sentMsg->setVL(vl);
				p_sentMsg->setWasLast(sn == 1);
				send(p_sentMsg, "sent");
			}
		}
	} else {
		IBSentMsg *p_sentMsg = new IBSentMsg("hca_sent", IB_SENT_MSG);
		p_sentMsg->setVL(vl);
		p_sentMsg->setWasLast(p_msg->getPacketLength() == p_msg->getCreditSn() + 1);
		send(p_sentMsg, "sent");
	}
	if (! ev.isDisabled()) {
		ev << "-I- " << " CreditSn " << p_msg->getCreditSn() << " packetLength :" << p_msg->getPacketLength() << "End of Msg :"  << p_msg->getEoMsg()<< endl;
		ev << "-I- " << " srcLid " << p_msg->getSrcLid() << " ppRespond :" << p_msg->getPpResponse() << endl;
	}
	if ((p_msg->getCreditSn() + 1)== p_msg->getPacketLength() && (p_msg->getEoMsg())) {
    	if (p_msg->getPpResponse() == 0) { // if this is a normal data message
    		sprintf(name, "ACK");
    		p_dataMsg = new IBDataMsg(name, IB_DATA_MSG);
    		p_dataMsg->setPacketId(p_msg->getPacketId());
    		p_dataMsg->setSrcLid(p_msg->getSrcLid());
    		p_dataMsg->setCreditSn(0);
    		p_dataMsg->setPpResponse(1); // tell the generator to reply to the source
    		p_dataMsg->setSinkToGen(1);  // indicate this is the message from sink->Gen
    	} else {
    		sprintf(name, "ACK_DONE");   // received the ACK message from the destination
    		p_dataMsg = new IBDataMsg(name, IB_DATA_MSG);
    	}

        send(p_dataMsg, "ack"); // send data to the generator
    	if (! ev.isDisabled()) {
    		ev << "-I- " << " Sink ==" << " PpResponse :" << p_dataMsg->getPpResponse() << " source LID :" << p_dataMsg->getSrcLid() << endl;
    	}
    }

	delete p_msg;
}

void IBSink::handleData(IBDataMsg *p_msg)
{
	simtime_t delay;

	// for iBW calculations
	if (firstPacketRcv) {
		firstPacketRcv = 0;
		FirstPktRcvTime = simTime();
	}
	AccBytesRcv += p_msg->getBitLength()/8;
    LastBytesRcv += p_msg->getBitLength()/8;

   // we might be arriving on empty buffer:
   if ( ! p_drainMsg )
   {
		if (! ev.isDisabled())
			ev << "-I- " << getFullPath() << " data:" << p_msg->getName()
				<< " arrived on empty FIFO" << endl;
		// this credit should take this time consume:
		delay = p_msg->getBitLength() / 8.0 * byteDrainRate_us;
		newDrainMessage(delay);
   }

	if (! ev.isDisabled())
		ev << "-I- " << getFullPath() << " queued data:" << p_msg->getName() << endl;
	queue.insert(p_msg);

}

// simply consume one message from the Q or stop the drain if Q is empty
// also under hiccup do nothing
void IBSink::handlePop(cMessage *p_msg)
{
	// if we are under hiccup - do nothing or
   // got to pop from the queue if anything there
	if ( !queue.empty() && ! duringHiccup )
	{
		IBDataMsg *p_dataMsg = (IBDataMsg *)queue.pop();
		if (! ev.isDisabled())
			ev << "-I- " << getFullPath() << " De-queued data:" << p_dataMsg->getName() << endl;

		// when is our next pop event?
		simtime_t delay_us = p_dataMsg->getBitLength() / 8.0 * byteDrainRate_us;
		/*  	if (! ev.isDisabled())
			 ev << "-I- " << getFullPath() << " next drain in: " << delay_us << " usec" << endl; */

		// consume actually discards the message !!!
		consumeDataMsg(p_dataMsg);

		scheduleAt(simTime()+delay_us*1e-6, p_drainMsg);
   }
   else
   {
		// The queue is empty. Next message needs to immediatly pop
		// so we clean the drain event
		ev << "-I- " << getFullPath() << " Nothing to POP" << endl;
		cancelAndDelete(p_drainMsg);
		p_drainMsg = 0;
   }
}

// hickup really means we  drain and set another one.
void IBSink::handleHiccup(cMessage *p_msg)
{
	double delay;

	if ( duringHiccup )
	{
		// we are inside a hiccup - turn it off and schedule next ON
		duringHiccup = 0;
		delay = par("hiccupDelay_us");
		if (!ev.isDisabled())
			ev << "-I- " << getFullPath() << " Hiccup OFF for:" << delay << "usec" << endl;

		// as we are out of hiccup make sure we have at least one outstanding drain
		if (! p_drainMsg)
			newDrainMessage(1e-3); // 1ns
	}
	else
	{
		// we need to start a new hiccup
		duringHiccup = 1;
		delay = par("hiccupDuration_us");

		if (!ev.isDisabled())
			ev << "-I- " << getFullPath() << " Hiccup ON for:" << delay << "usec" << endl ;
	}

	hiccupStats.collect( simTime() );
	scheduleAt(simTime()+delay*1e-6, p_hiccupMsg);
}

void IBSink::handleMessage(cMessage *p_msg)
{
	simtime_t delay;
	int kind = p_msg->getKind();

	if ( kind == IB_DATA_MSG )
	{
		handleData((IBDataMsg *)p_msg);
	}
	else if ( kind == IB_POP_MSG )
	{
		handlePop(p_msg);
	}
	else if ( kind == IB_HICCUP_MSG )
	{
		handleHiccup(p_msg);
	}
	else if ( kind == IB_FLOWCTRL_MSG )
	{
		if (!ev.isDisabled())
			ev << "-I- " << getFullPath() << " Dropping flow control message";
		delete p_msg;
	}
	else if ( kind == IB_DONE_MSG )
	{
		delete p_msg;
	}
    else if ( kind == IB_LOG_TIMER_MSG )
    {
        handleIntervalLogging(p_msg);
    }
	else
	{
		ev << "-E- " << getFullPath() << " does not know what to with msg:"
		   <<  p_msg->getKind() << "is local:" << p_msg->isSelfMessage()
         << " senderModule:" << p_msg->getSenderModule()
         << endl;
		delete p_msg;
	}
}

void IBSink::handleIntervalLogging(cMessage *p_msg)
{
	simtime_t now = simTime();

	if(lastIntLogTime >= now)
		return; // Nothing new to log/something is rotten

	// The BW for the last interval
	double oBW = ((AccBytesRcv - lastIntLogAccBytes)/(now - lastIntLogTime));

	IntervalBWStats.record(oBW);

	ev << "STAT - INTERVAL (" << logInterval << ") : " << getFullPath() << " Gen Interval Output BW (B/s):  " << oBW << endl;

	// Update bookkeeping variables
	lastIntLogAccBytes = AccBytesRcv;
	lastIntLogTime = now;

    // Rescheduling interval logging
    scheduleAt(now + logInterval, p_msg);
}

void IBSink::finish()
{
	char bufVL[128]; // holds the string describing the HCA's VL
	char bufHCASink[128]; // holds the string describing the HCA's sink
	recordScalar("Time last packet consumed:", lastConsumedPakcet);
	ev << "STAT: " << getFullPath()
		<< " Data Packets Time: num/avg/max/std: "
		<< waitStats.getCount() << " / " << waitStats.getMean() << " / "
		<< waitStats.getMax() << " / "  << waitStats.getStddev() << endl;

	ev << "STAT: " << getFullPath() << " Message Time: num/avg/max/std:"
		<< PakcetFabricTime.getCount() << " / " << PakcetFabricTime.getMean() << " / "
		<< PakcetFabricTime.getMax() << " / "  << PakcetFabricTime.getStddev() << endl;

	ev << "STAT: " << getFullPath() << " Message BW: num/avg/max/std:"
		<< MeasuredBW.getCount() << " / " << MeasuredBW.getMean() << " / "
		<< MeasuredBW.getMax() << " / "  << MeasuredBW.getStddev() << endl;

	ev << "STAT: " << getFullPath() << " Received BW: num/avg/max/std:"
		<< ActualBW.getCount() << " / " << ActualBW.getMean() << " / "
		<< ActualBW.getMax() << " / "  << ActualBW.getStddev() << endl;

	double iBW = AccBytesRcv / (simTime() - FirstPktRcvTime);
	ev << "STAT: " << getFullPath() << " Sink input BW (B/s):" << iBW  << endl;

	sprintf(bufHCASink, "%s Sink Bandwidth (B/s)", getFullPath().c_str());
	recordScalar(bufHCASink, iBW);

   ev << "STAT: " << getFullPath() << " Packet per VL:";
	for (int vl = 0; vl < maxVL+1; vl++) {
	  ev << VLPackets[vl] << " ";
	  sprintf(bufVL, "%s VL:%d total packets", getFullPath().c_str() , vl);
	  recordScalar(bufVL, VLPackets[vl]);
	}
	ev << endl;

    ev << "STAT: " << getFullPath() << " Out of order: num/avg/max/std:"
        << outOfOrder.getCount() << " / " << outOfOrder.getMean() << " / "
        << outOfOrder.getMax() << " / "  << outOfOrder.getStddev() << endl;

    ev << "STAT: " << getFullPath() << " Credits jitter: num/avg/max/std:"
        << jitterPacketTime.getCount() << " / " << jitterPacketTime.getMean() << " / "
        << jitterPacketTime.getMax() << " / "  << jitterPacketTime.getStddev() << endl;

    outOfOrder.record();
	waitStats.record();
	PakcetFabricTime.record();
	jitterPacketTime.record();

}
