//
// Generated file, do not edit! Created by opp_msgc 4.2 from src/ib.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#include <iostream>
#include <sstream>
#include "ib_m.h"

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
std::ostream& operator<<(std::ostream& out,const T&) {return out;}

// Another default rule (prevents compiler from choosing base class' doPacking())
template<typename T>
void doPacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doPacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}

template<typename T>
void doUnpacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doUnpacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}




EXECUTE_ON_STARTUP(
    cEnum *e = cEnum::find("IB_MSGS");
    if (!e) enums.getInstance()->add(e = new cEnum("IB_MSGS"));
    e->insert(IB_DATA_MSG, "IB_DATA_MSG");
    e->insert(IB_FLOWCTRL_MSG, "IB_FLOWCTRL_MSG");
    e->insert(IB_SENT_MSG, "IB_SENT_MSG");
    e->insert(IB_DESC_MSG, "IB_DESC_MSG");
    e->insert(IB_TXCRED_MSG, "IB_TXCRED_MSG");
    e->insert(IB_RXCRED_MSG, "IB_RXCRED_MSG");
    e->insert(CLK_MSG, "CLK_MSG");
    e->insert(IB_FIFOSTAT_MSG, "IB_FIFOSTAT_MSG");
    e->insert(FDB_XDONE_MSG, "FDB_XDONE_MSG");
    e->insert(IB_POPDATA_MSG, "IB_POPDATA_MSG");
    e->insert(IB_ENDPKT_MSG, "IB_ENDPKT_MSG");
    e->insert(IB_MINTIME_MSG, "IB_MINTIME_MSG");
    e->insert(IB_POP_MSG, "IB_POP_MSG");
    e->insert(IB_HICCUP_MSG, "IB_HICCUP_MSG");
    e->insert(IB_FREE_MSG, "IB_FREE_MSG");
    e->insert(IB_DONE_MSG, "IB_DONE_MSG");
    e->insert(IB_FTDPPUSH_MSG, "IB_FTDPPUSH_MSG");
    e->insert(IB_TQVLPUSH_MSG, "IB_TQVLPUSH_MSG");
    e->insert(IB_VLARB_MSG, "IB_VLARB_MSG");
    e->insert(IB_RQARB_MSG, "IB_RQARB_MSG");
    e->insert(IB_FTDPPOP_MSG, "IB_FTDPPOP_MSG");
    e->insert(IB_TQPOPOP_MSG, "IB_TQPOPOP_MSG");
    e->insert(IB_VLARBINV_MSG, "IB_VLARBINV_MSG");
    e->insert(IB_DPRDY_MSG, "IB_DPRDY_MSG");
    e->insert(IB_TQVLPOP_MSG, "IB_TQVLPOP_MSG");
    e->insert(IBAFDB_MSG, "IBAFDB_MSG");
    e->insert(IB_CC_TIMER_MSG, "IB_CC_TIMER_MSG");
    e->insert(IB_LOG_TIMER_MSG, "IB_LOG_TIMER_MSG");
    e->insert(IB_GEN_RECONFIG, "IB_GEN_RECONFIG");
    e->insert(IB_GEN_FLOOD_REFILL, "IB_GEN_FLOOD_REFILL");
);

Register_Class(IBGenFloodRefill);

IBGenFloodRefill::IBGenFloodRefill(const char *name, int kind) : cPacket(name,kind)
{
    this->VL_var = 0;
}

IBGenFloodRefill::IBGenFloodRefill(const IBGenFloodRefill& other) : cPacket(other)
{
    copy(other);
}

IBGenFloodRefill::~IBGenFloodRefill()
{
}

IBGenFloodRefill& IBGenFloodRefill::operator=(const IBGenFloodRefill& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void IBGenFloodRefill::copy(const IBGenFloodRefill& other)
{
    this->VL_var = other.VL_var;
}

void IBGenFloodRefill::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->VL_var);
}

void IBGenFloodRefill::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->VL_var);
}

short IBGenFloodRefill::getVL() const
{
    return VL_var;
}

void IBGenFloodRefill::setVL(short VL)
{
    this->VL_var = VL;
}

class IBGenFloodRefillDescriptor : public cClassDescriptor
{
  public:
    IBGenFloodRefillDescriptor();
    virtual ~IBGenFloodRefillDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBGenFloodRefillDescriptor);

IBGenFloodRefillDescriptor::IBGenFloodRefillDescriptor() : cClassDescriptor("IBGenFloodRefill", "cPacket")
{
}

IBGenFloodRefillDescriptor::~IBGenFloodRefillDescriptor()
{
}

bool IBGenFloodRefillDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBGenFloodRefill *>(obj)!=NULL;
}

const char *IBGenFloodRefillDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBGenFloodRefillDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 1+basedesc->getFieldCount(object) : 1;
}

unsigned int IBGenFloodRefillDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
    };
    return (field>=0 && field<1) ? fieldTypeFlags[field] : 0;
}

const char *IBGenFloodRefillDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "VL",
    };
    return (field>=0 && field<1) ? fieldNames[field] : NULL;
}

int IBGenFloodRefillDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='V' && strcmp(fieldName, "VL")==0) return base+0;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBGenFloodRefillDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "short",
    };
    return (field>=0 && field<1) ? fieldTypeStrings[field] : NULL;
}

const char *IBGenFloodRefillDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBGenFloodRefillDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBGenFloodRefill *pp = (IBGenFloodRefill *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBGenFloodRefillDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBGenFloodRefill *pp = (IBGenFloodRefill *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getVL());
        default: return "";
    }
}

bool IBGenFloodRefillDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBGenFloodRefill *pp = (IBGenFloodRefill *)object; (void)pp;
    switch (field) {
        case 0: pp->setVL(string2long(value)); return true;
        default: return false;
    }
}

const char *IBGenFloodRefillDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
    };
    return (field>=0 && field<1) ? fieldStructNames[field] : NULL;
}

void *IBGenFloodRefillDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBGenFloodRefill *pp = (IBGenFloodRefill *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBGenReconfig);

IBGenReconfig::IBGenReconfig(const char *name, int kind) : cPacket(name,kind)
{
    this->dstLid_var = 0;
    this->dstHSPerc_var = 0;
}

IBGenReconfig::IBGenReconfig(const IBGenReconfig& other) : cPacket(other)
{
    copy(other);
}

IBGenReconfig::~IBGenReconfig()
{
}

IBGenReconfig& IBGenReconfig::operator=(const IBGenReconfig& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void IBGenReconfig::copy(const IBGenReconfig& other)
{
    this->dstLid_var = other.dstLid_var;
    this->dstHSPerc_var = other.dstHSPerc_var;
}

void IBGenReconfig::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->dstLid_var);
    doPacking(b,this->dstHSPerc_var);
}

void IBGenReconfig::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->dstLid_var);
    doUnpacking(b,this->dstHSPerc_var);
}

int IBGenReconfig::getDstLid() const
{
    return dstLid_var;
}

void IBGenReconfig::setDstLid(int dstLid)
{
    this->dstLid_var = dstLid;
}

double IBGenReconfig::getDstHSPerc() const
{
    return dstHSPerc_var;
}

void IBGenReconfig::setDstHSPerc(double dstHSPerc)
{
    this->dstHSPerc_var = dstHSPerc;
}

class IBGenReconfigDescriptor : public cClassDescriptor
{
  public:
    IBGenReconfigDescriptor();
    virtual ~IBGenReconfigDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBGenReconfigDescriptor);

IBGenReconfigDescriptor::IBGenReconfigDescriptor() : cClassDescriptor("IBGenReconfig", "cPacket")
{
}

IBGenReconfigDescriptor::~IBGenReconfigDescriptor()
{
}

bool IBGenReconfigDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBGenReconfig *>(obj)!=NULL;
}

const char *IBGenReconfigDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBGenReconfigDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 2+basedesc->getFieldCount(object) : 2;
}

unsigned int IBGenReconfigDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<2) ? fieldTypeFlags[field] : 0;
}

const char *IBGenReconfigDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "dstLid",
        "dstHSPerc",
    };
    return (field>=0 && field<2) ? fieldNames[field] : NULL;
}

int IBGenReconfigDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='d' && strcmp(fieldName, "dstLid")==0) return base+0;
    if (fieldName[0]=='d' && strcmp(fieldName, "dstHSPerc")==0) return base+1;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBGenReconfigDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "double",
    };
    return (field>=0 && field<2) ? fieldTypeStrings[field] : NULL;
}

const char *IBGenReconfigDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBGenReconfigDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBGenReconfig *pp = (IBGenReconfig *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBGenReconfigDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBGenReconfig *pp = (IBGenReconfig *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getDstLid());
        case 1: return double2string(pp->getDstHSPerc());
        default: return "";
    }
}

bool IBGenReconfigDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBGenReconfig *pp = (IBGenReconfig *)object; (void)pp;
    switch (field) {
        case 0: pp->setDstLid(string2long(value)); return true;
        case 1: pp->setDstHSPerc(string2double(value)); return true;
        default: return false;
    }
}

const char *IBGenReconfigDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
    };
    return (field>=0 && field<2) ? fieldStructNames[field] : NULL;
}

void *IBGenReconfigDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBGenReconfig *pp = (IBGenReconfig *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBAFDBMsg);

IBAFDBMsg::IBAFDBMsg(const char *name, int kind) : cPacket(name,kind)
{
    this->PacketID_var = 0;
    this->AccLen_var = 0;
}

IBAFDBMsg::IBAFDBMsg(const IBAFDBMsg& other) : cPacket(other)
{
    copy(other);
}

IBAFDBMsg::~IBAFDBMsg()
{
}

IBAFDBMsg& IBAFDBMsg::operator=(const IBAFDBMsg& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void IBAFDBMsg::copy(const IBAFDBMsg& other)
{
    this->PacketID_var = other.PacketID_var;
    this->AccLen_var = other.AccLen_var;
}

void IBAFDBMsg::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->PacketID_var);
    doPacking(b,this->AccLen_var);
}

void IBAFDBMsg::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->PacketID_var);
    doUnpacking(b,this->AccLen_var);
}

int IBAFDBMsg::getPacketID() const
{
    return PacketID_var;
}

void IBAFDBMsg::setPacketID(int PacketID)
{
    this->PacketID_var = PacketID;
}

long IBAFDBMsg::getAccLen() const
{
    return AccLen_var;
}

void IBAFDBMsg::setAccLen(long AccLen)
{
    this->AccLen_var = AccLen;
}

class IBAFDBMsgDescriptor : public cClassDescriptor
{
  public:
    IBAFDBMsgDescriptor();
    virtual ~IBAFDBMsgDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBAFDBMsgDescriptor);

IBAFDBMsgDescriptor::IBAFDBMsgDescriptor() : cClassDescriptor("IBAFDBMsg", "cPacket")
{
}

IBAFDBMsgDescriptor::~IBAFDBMsgDescriptor()
{
}

bool IBAFDBMsgDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBAFDBMsg *>(obj)!=NULL;
}

const char *IBAFDBMsgDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBAFDBMsgDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 2+basedesc->getFieldCount(object) : 2;
}

unsigned int IBAFDBMsgDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<2) ? fieldTypeFlags[field] : 0;
}

const char *IBAFDBMsgDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "PacketID",
        "AccLen",
    };
    return (field>=0 && field<2) ? fieldNames[field] : NULL;
}

int IBAFDBMsgDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='P' && strcmp(fieldName, "PacketID")==0) return base+0;
    if (fieldName[0]=='A' && strcmp(fieldName, "AccLen")==0) return base+1;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBAFDBMsgDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "long",
    };
    return (field>=0 && field<2) ? fieldTypeStrings[field] : NULL;
}

const char *IBAFDBMsgDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBAFDBMsgDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBAFDBMsg *pp = (IBAFDBMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBAFDBMsgDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBAFDBMsg *pp = (IBAFDBMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getPacketID());
        case 1: return long2string(pp->getAccLen());
        default: return "";
    }
}

bool IBAFDBMsgDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBAFDBMsg *pp = (IBAFDBMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setPacketID(string2long(value)); return true;
        case 1: pp->setAccLen(string2long(value)); return true;
        default: return false;
    }
}

const char *IBAFDBMsgDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
    };
    return (field>=0 && field<2) ? fieldStructNames[field] : NULL;
}

void *IBAFDBMsgDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBAFDBMsg *pp = (IBAFDBMsg *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBWireMsg);

IBWireMsg::IBWireMsg(const char *name, int kind) : cPacket(name,kind)
{
    this->VL_var = 0;
    this->FECN_var = 0;
    this->BECN_var = 0;
    this->creditSn_var = 0;
    this->packetId_var = 0;
    this->msgId_var = 0;
    this->PacketLength_var = 0;
    this->PacketLengthBytes_var = 0;
    this->dstLid_var = 0;
    this->srcLid_var = 0;
    this->SL_var = 0;
}

IBWireMsg::IBWireMsg(const IBWireMsg& other) : cPacket(other)
{
    copy(other);
}

IBWireMsg::~IBWireMsg()
{
}

IBWireMsg& IBWireMsg::operator=(const IBWireMsg& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void IBWireMsg::copy(const IBWireMsg& other)
{
    this->VL_var = other.VL_var;
    this->FECN_var = other.FECN_var;
    this->BECN_var = other.BECN_var;
    this->creditSn_var = other.creditSn_var;
    this->packetId_var = other.packetId_var;
    this->msgId_var = other.msgId_var;
    this->PacketLength_var = other.PacketLength_var;
    this->PacketLengthBytes_var = other.PacketLengthBytes_var;
    this->dstLid_var = other.dstLid_var;
    this->srcLid_var = other.srcLid_var;
    this->SL_var = other.SL_var;
}

void IBWireMsg::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->VL_var);
    doPacking(b,this->FECN_var);
    doPacking(b,this->BECN_var);
    doPacking(b,this->creditSn_var);
    doPacking(b,this->packetId_var);
    doPacking(b,this->msgId_var);
    doPacking(b,this->PacketLength_var);
    doPacking(b,this->PacketLengthBytes_var);
    doPacking(b,this->dstLid_var);
    doPacking(b,this->srcLid_var);
    doPacking(b,this->SL_var);
}

void IBWireMsg::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->VL_var);
    doUnpacking(b,this->FECN_var);
    doUnpacking(b,this->BECN_var);
    doUnpacking(b,this->creditSn_var);
    doUnpacking(b,this->packetId_var);
    doUnpacking(b,this->msgId_var);
    doUnpacking(b,this->PacketLength_var);
    doUnpacking(b,this->PacketLengthBytes_var);
    doUnpacking(b,this->dstLid_var);
    doUnpacking(b,this->srcLid_var);
    doUnpacking(b,this->SL_var);
}

short IBWireMsg::getVL() const
{
    return VL_var;
}

void IBWireMsg::setVL(short VL)
{
    this->VL_var = VL;
}

int IBWireMsg::getFECN() const
{
    return FECN_var;
}

void IBWireMsg::setFECN(int FECN)
{
    this->FECN_var = FECN;
}

int IBWireMsg::getBECN() const
{
    return BECN_var;
}

void IBWireMsg::setBECN(int BECN)
{
    this->BECN_var = BECN;
}

int IBWireMsg::getCreditSn() const
{
    return creditSn_var;
}

void IBWireMsg::setCreditSn(int creditSn)
{
    this->creditSn_var = creditSn;
}

int IBWireMsg::getPacketId() const
{
    return packetId_var;
}

void IBWireMsg::setPacketId(int packetId)
{
    this->packetId_var = packetId;
}

int IBWireMsg::getMsgId() const
{
    return msgId_var;
}

void IBWireMsg::setMsgId(int msgId)
{
    this->msgId_var = msgId;
}

int IBWireMsg::getPacketLength() const
{
    return PacketLength_var;
}

void IBWireMsg::setPacketLength(int PacketLength)
{
    this->PacketLength_var = PacketLength;
}

int IBWireMsg::getPacketLengthBytes() const
{
    return PacketLengthBytes_var;
}

void IBWireMsg::setPacketLengthBytes(int PacketLengthBytes)
{
    this->PacketLengthBytes_var = PacketLengthBytes;
}

int IBWireMsg::getDstLid() const
{
    return dstLid_var;
}

void IBWireMsg::setDstLid(int dstLid)
{
    this->dstLid_var = dstLid;
}

int IBWireMsg::getSrcLid() const
{
    return srcLid_var;
}

void IBWireMsg::setSrcLid(int srcLid)
{
    this->srcLid_var = srcLid;
}

int IBWireMsg::getSL() const
{
    return SL_var;
}

void IBWireMsg::setSL(int SL)
{
    this->SL_var = SL;
}

class IBWireMsgDescriptor : public cClassDescriptor
{
  public:
    IBWireMsgDescriptor();
    virtual ~IBWireMsgDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBWireMsgDescriptor);

IBWireMsgDescriptor::IBWireMsgDescriptor() : cClassDescriptor("IBWireMsg", "cPacket")
{
}

IBWireMsgDescriptor::~IBWireMsgDescriptor()
{
}

bool IBWireMsgDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBWireMsg *>(obj)!=NULL;
}

const char *IBWireMsgDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBWireMsgDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 11+basedesc->getFieldCount(object) : 11;
}

unsigned int IBWireMsgDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<11) ? fieldTypeFlags[field] : 0;
}

const char *IBWireMsgDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "VL",
        "FECN",
        "BECN",
        "creditSn",
        "packetId",
        "msgId",
        "PacketLength",
        "PacketLengthBytes",
        "dstLid",
        "srcLid",
        "SL",
    };
    return (field>=0 && field<11) ? fieldNames[field] : NULL;
}

int IBWireMsgDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='V' && strcmp(fieldName, "VL")==0) return base+0;
    if (fieldName[0]=='F' && strcmp(fieldName, "FECN")==0) return base+1;
    if (fieldName[0]=='B' && strcmp(fieldName, "BECN")==0) return base+2;
    if (fieldName[0]=='c' && strcmp(fieldName, "creditSn")==0) return base+3;
    if (fieldName[0]=='p' && strcmp(fieldName, "packetId")==0) return base+4;
    if (fieldName[0]=='m' && strcmp(fieldName, "msgId")==0) return base+5;
    if (fieldName[0]=='P' && strcmp(fieldName, "PacketLength")==0) return base+6;
    if (fieldName[0]=='P' && strcmp(fieldName, "PacketLengthBytes")==0) return base+7;
    if (fieldName[0]=='d' && strcmp(fieldName, "dstLid")==0) return base+8;
    if (fieldName[0]=='s' && strcmp(fieldName, "srcLid")==0) return base+9;
    if (fieldName[0]=='S' && strcmp(fieldName, "SL")==0) return base+10;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBWireMsgDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "short",
        "int",
        "int",
        "int",
        "int",
        "int",
        "int",
        "int",
        "int",
        "int",
        "int",
    };
    return (field>=0 && field<11) ? fieldTypeStrings[field] : NULL;
}

const char *IBWireMsgDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBWireMsgDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBWireMsg *pp = (IBWireMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBWireMsgDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBWireMsg *pp = (IBWireMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getVL());
        case 1: return long2string(pp->getFECN());
        case 2: return long2string(pp->getBECN());
        case 3: return long2string(pp->getCreditSn());
        case 4: return long2string(pp->getPacketId());
        case 5: return long2string(pp->getMsgId());
        case 6: return long2string(pp->getPacketLength());
        case 7: return long2string(pp->getPacketLengthBytes());
        case 8: return long2string(pp->getDstLid());
        case 9: return long2string(pp->getSrcLid());
        case 10: return long2string(pp->getSL());
        default: return "";
    }
}

bool IBWireMsgDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBWireMsg *pp = (IBWireMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setVL(string2long(value)); return true;
        case 1: pp->setFECN(string2long(value)); return true;
        case 2: pp->setBECN(string2long(value)); return true;
        case 3: pp->setCreditSn(string2long(value)); return true;
        case 4: pp->setPacketId(string2long(value)); return true;
        case 5: pp->setMsgId(string2long(value)); return true;
        case 6: pp->setPacketLength(string2long(value)); return true;
        case 7: pp->setPacketLengthBytes(string2long(value)); return true;
        case 8: pp->setDstLid(string2long(value)); return true;
        case 9: pp->setSrcLid(string2long(value)); return true;
        case 10: pp->setSL(string2long(value)); return true;
        default: return false;
    }
}

const char *IBWireMsgDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    };
    return (field>=0 && field<11) ? fieldStructNames[field] : NULL;
}

void *IBWireMsgDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBWireMsg *pp = (IBWireMsg *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBDataMsg);

IBDataMsg::IBDataMsg(const char *name, int kind) : IBWireMsg(name,kind)
{
    this->useStatic_var = 0;
    this->ppResponse_var = 0;
    this->eoMsg_var = 0;
    this->sinkToGen_var = 0;
    this->MsgTimestamp_var = 0;
    this->SwTimeStamp_var = 0;
    this->MsgRemaining_var = 0;
    this->MsgSize_var = 0;
}

IBDataMsg::IBDataMsg(const IBDataMsg& other) : IBWireMsg(other)
{
    copy(other);
}

IBDataMsg::~IBDataMsg()
{
}

IBDataMsg& IBDataMsg::operator=(const IBDataMsg& other)
{
    if (this==&other) return *this;
    IBWireMsg::operator=(other);
    copy(other);
    return *this;
}

void IBDataMsg::copy(const IBDataMsg& other)
{
    this->useStatic_var = other.useStatic_var;
    this->ppResponse_var = other.ppResponse_var;
    this->eoMsg_var = other.eoMsg_var;
    this->sinkToGen_var = other.sinkToGen_var;
    this->MsgTimestamp_var = other.MsgTimestamp_var;
    this->SwTimeStamp_var = other.SwTimeStamp_var;
    this->MsgRemaining_var = other.MsgRemaining_var;
    this->MsgSize_var = other.MsgSize_var;
}

void IBDataMsg::parsimPack(cCommBuffer *b)
{
    IBWireMsg::parsimPack(b);
    doPacking(b,this->useStatic_var);
    doPacking(b,this->ppResponse_var);
    doPacking(b,this->eoMsg_var);
    doPacking(b,this->sinkToGen_var);
    doPacking(b,this->MsgTimestamp_var);
    doPacking(b,this->SwTimeStamp_var);
    doPacking(b,this->MsgRemaining_var);
    doPacking(b,this->MsgSize_var);
}

void IBDataMsg::parsimUnpack(cCommBuffer *b)
{
    IBWireMsg::parsimUnpack(b);
    doUnpacking(b,this->useStatic_var);
    doUnpacking(b,this->ppResponse_var);
    doUnpacking(b,this->eoMsg_var);
    doUnpacking(b,this->sinkToGen_var);
    doUnpacking(b,this->MsgTimestamp_var);
    doUnpacking(b,this->SwTimeStamp_var);
    doUnpacking(b,this->MsgRemaining_var);
    doUnpacking(b,this->MsgSize_var);
}

int IBDataMsg::getUseStatic() const
{
    return useStatic_var;
}

void IBDataMsg::setUseStatic(int useStatic)
{
    this->useStatic_var = useStatic;
}

int IBDataMsg::getPpResponse() const
{
    return ppResponse_var;
}

void IBDataMsg::setPpResponse(int ppResponse)
{
    this->ppResponse_var = ppResponse;
}

int IBDataMsg::getEoMsg() const
{
    return eoMsg_var;
}

void IBDataMsg::setEoMsg(int eoMsg)
{
    this->eoMsg_var = eoMsg;
}

int IBDataMsg::getSinkToGen() const
{
    return sinkToGen_var;
}

void IBDataMsg::setSinkToGen(int sinkToGen)
{
    this->sinkToGen_var = sinkToGen;
}

simtime_t IBDataMsg::getMsgTimestamp() const
{
    return MsgTimestamp_var;
}

void IBDataMsg::setMsgTimestamp(simtime_t MsgTimestamp)
{
    this->MsgTimestamp_var = MsgTimestamp;
}

simtime_t IBDataMsg::getSwTimeStamp() const
{
    return SwTimeStamp_var;
}

void IBDataMsg::setSwTimeStamp(simtime_t SwTimeStamp)
{
    this->SwTimeStamp_var = SwTimeStamp;
}

int IBDataMsg::getMsgRemaining() const
{
    return MsgRemaining_var;
}

void IBDataMsg::setMsgRemaining(int MsgRemaining)
{
    this->MsgRemaining_var = MsgRemaining;
}

int IBDataMsg::getMsgSize() const
{
    return MsgSize_var;
}

void IBDataMsg::setMsgSize(int MsgSize)
{
    this->MsgSize_var = MsgSize;
}

class IBDataMsgDescriptor : public cClassDescriptor
{
  public:
    IBDataMsgDescriptor();
    virtual ~IBDataMsgDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBDataMsgDescriptor);

IBDataMsgDescriptor::IBDataMsgDescriptor() : cClassDescriptor("IBDataMsg", "IBWireMsg")
{
}

IBDataMsgDescriptor::~IBDataMsgDescriptor()
{
}

bool IBDataMsgDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBDataMsg *>(obj)!=NULL;
}

const char *IBDataMsgDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBDataMsgDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 8+basedesc->getFieldCount(object) : 8;
}

unsigned int IBDataMsgDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<8) ? fieldTypeFlags[field] : 0;
}

const char *IBDataMsgDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "useStatic",
        "ppResponse",
        "eoMsg",
        "sinkToGen",
        "MsgTimestamp",
        "SwTimeStamp",
        "MsgRemaining",
        "MsgSize",
    };
    return (field>=0 && field<8) ? fieldNames[field] : NULL;
}

int IBDataMsgDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='u' && strcmp(fieldName, "useStatic")==0) return base+0;
    if (fieldName[0]=='p' && strcmp(fieldName, "ppResponse")==0) return base+1;
    if (fieldName[0]=='e' && strcmp(fieldName, "eoMsg")==0) return base+2;
    if (fieldName[0]=='s' && strcmp(fieldName, "sinkToGen")==0) return base+3;
    if (fieldName[0]=='M' && strcmp(fieldName, "MsgTimestamp")==0) return base+4;
    if (fieldName[0]=='S' && strcmp(fieldName, "SwTimeStamp")==0) return base+5;
    if (fieldName[0]=='M' && strcmp(fieldName, "MsgRemaining")==0) return base+6;
    if (fieldName[0]=='M' && strcmp(fieldName, "MsgSize")==0) return base+7;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBDataMsgDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "int",
        "int",
        "int",
        "simtime_t",
        "simtime_t",
        "int",
        "int",
    };
    return (field>=0 && field<8) ? fieldTypeStrings[field] : NULL;
}

const char *IBDataMsgDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBDataMsgDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBDataMsg *pp = (IBDataMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBDataMsgDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBDataMsg *pp = (IBDataMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getUseStatic());
        case 1: return long2string(pp->getPpResponse());
        case 2: return long2string(pp->getEoMsg());
        case 3: return long2string(pp->getSinkToGen());
        case 4: return double2string(pp->getMsgTimestamp());
        case 5: return double2string(pp->getSwTimeStamp());
        case 6: return long2string(pp->getMsgRemaining());
        case 7: return long2string(pp->getMsgSize());
        default: return "";
    }
}

bool IBDataMsgDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBDataMsg *pp = (IBDataMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setUseStatic(string2long(value)); return true;
        case 1: pp->setPpResponse(string2long(value)); return true;
        case 2: pp->setEoMsg(string2long(value)); return true;
        case 3: pp->setSinkToGen(string2long(value)); return true;
        case 4: pp->setMsgTimestamp(string2double(value)); return true;
        case 5: pp->setSwTimeStamp(string2double(value)); return true;
        case 6: pp->setMsgRemaining(string2long(value)); return true;
        case 7: pp->setMsgSize(string2long(value)); return true;
        default: return false;
    }
}

const char *IBDataMsgDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    };
    return (field>=0 && field<8) ? fieldStructNames[field] : NULL;
}

void *IBDataMsgDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBDataMsg *pp = (IBDataMsg *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBCNPMsg);

IBCNPMsg::IBCNPMsg(const char *name, int kind) : IBWireMsg(name,kind)
{
    this->useStatic_var = 0;
    this->SwTimeStamp_var = 0;
}

IBCNPMsg::IBCNPMsg(const IBCNPMsg& other) : IBWireMsg(other)
{
    copy(other);
}

IBCNPMsg::~IBCNPMsg()
{
}

IBCNPMsg& IBCNPMsg::operator=(const IBCNPMsg& other)
{
    if (this==&other) return *this;
    IBWireMsg::operator=(other);
    copy(other);
    return *this;
}

void IBCNPMsg::copy(const IBCNPMsg& other)
{
    this->useStatic_var = other.useStatic_var;
    this->SwTimeStamp_var = other.SwTimeStamp_var;
}

void IBCNPMsg::parsimPack(cCommBuffer *b)
{
    IBWireMsg::parsimPack(b);
    doPacking(b,this->useStatic_var);
    doPacking(b,this->SwTimeStamp_var);
}

void IBCNPMsg::parsimUnpack(cCommBuffer *b)
{
    IBWireMsg::parsimUnpack(b);
    doUnpacking(b,this->useStatic_var);
    doUnpacking(b,this->SwTimeStamp_var);
}

int IBCNPMsg::getUseStatic() const
{
    return useStatic_var;
}

void IBCNPMsg::setUseStatic(int useStatic)
{
    this->useStatic_var = useStatic;
}

simtime_t IBCNPMsg::getSwTimeStamp() const
{
    return SwTimeStamp_var;
}

void IBCNPMsg::setSwTimeStamp(simtime_t SwTimeStamp)
{
    this->SwTimeStamp_var = SwTimeStamp;
}

class IBCNPMsgDescriptor : public cClassDescriptor
{
  public:
    IBCNPMsgDescriptor();
    virtual ~IBCNPMsgDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBCNPMsgDescriptor);

IBCNPMsgDescriptor::IBCNPMsgDescriptor() : cClassDescriptor("IBCNPMsg", "IBWireMsg")
{
}

IBCNPMsgDescriptor::~IBCNPMsgDescriptor()
{
}

bool IBCNPMsgDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBCNPMsg *>(obj)!=NULL;
}

const char *IBCNPMsgDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBCNPMsgDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 2+basedesc->getFieldCount(object) : 2;
}

unsigned int IBCNPMsgDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<2) ? fieldTypeFlags[field] : 0;
}

const char *IBCNPMsgDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "useStatic",
        "SwTimeStamp",
    };
    return (field>=0 && field<2) ? fieldNames[field] : NULL;
}

int IBCNPMsgDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='u' && strcmp(fieldName, "useStatic")==0) return base+0;
    if (fieldName[0]=='S' && strcmp(fieldName, "SwTimeStamp")==0) return base+1;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBCNPMsgDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "simtime_t",
    };
    return (field>=0 && field<2) ? fieldTypeStrings[field] : NULL;
}

const char *IBCNPMsgDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBCNPMsgDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBCNPMsg *pp = (IBCNPMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBCNPMsgDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBCNPMsg *pp = (IBCNPMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getUseStatic());
        case 1: return double2string(pp->getSwTimeStamp());
        default: return "";
    }
}

bool IBCNPMsgDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBCNPMsg *pp = (IBCNPMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setUseStatic(string2long(value)); return true;
        case 1: pp->setSwTimeStamp(string2double(value)); return true;
        default: return false;
    }
}

const char *IBCNPMsgDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
    };
    return (field>=0 && field<2) ? fieldStructNames[field] : NULL;
}

void *IBCNPMsgDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBCNPMsg *pp = (IBCNPMsg *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBCCIntMsg);

IBCCIntMsg::IBCCIntMsg(const char *name, int kind) : cPacket(name,kind)
{
    this->VL_var = 0;
    this->type_var = 0;
    this->packetId_var = 0;
    this->dstLid_var = 0;
    this->srcLid_var = 0;
    this->SL_var = 0;
    this->fillvalue_var = 0;
}

IBCCIntMsg::IBCCIntMsg(const IBCCIntMsg& other) : cPacket(other)
{
    copy(other);
}

IBCCIntMsg::~IBCCIntMsg()
{
}

IBCCIntMsg& IBCCIntMsg::operator=(const IBCCIntMsg& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void IBCCIntMsg::copy(const IBCCIntMsg& other)
{
    this->VL_var = other.VL_var;
    this->type_var = other.type_var;
    this->packetId_var = other.packetId_var;
    this->dstLid_var = other.dstLid_var;
    this->srcLid_var = other.srcLid_var;
    this->SL_var = other.SL_var;
    this->fillvalue_var = other.fillvalue_var;
}

void IBCCIntMsg::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->VL_var);
    doPacking(b,this->type_var);
    doPacking(b,this->packetId_var);
    doPacking(b,this->dstLid_var);
    doPacking(b,this->srcLid_var);
    doPacking(b,this->SL_var);
    doPacking(b,this->fillvalue_var);
}

void IBCCIntMsg::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->VL_var);
    doUnpacking(b,this->type_var);
    doUnpacking(b,this->packetId_var);
    doUnpacking(b,this->dstLid_var);
    doUnpacking(b,this->srcLid_var);
    doUnpacking(b,this->SL_var);
    doUnpacking(b,this->fillvalue_var);
}

short IBCCIntMsg::getVL() const
{
    return VL_var;
}

void IBCCIntMsg::setVL(short VL)
{
    this->VL_var = VL;
}

int IBCCIntMsg::getType() const
{
    return type_var;
}

void IBCCIntMsg::setType(int type)
{
    this->type_var = type;
}

int IBCCIntMsg::getPacketId() const
{
    return packetId_var;
}

void IBCCIntMsg::setPacketId(int packetId)
{
    this->packetId_var = packetId;
}

int IBCCIntMsg::getDstLid() const
{
    return dstLid_var;
}

void IBCCIntMsg::setDstLid(int dstLid)
{
    this->dstLid_var = dstLid;
}

int IBCCIntMsg::getSrcLid() const
{
    return srcLid_var;
}

void IBCCIntMsg::setSrcLid(int srcLid)
{
    this->srcLid_var = srcLid;
}

int IBCCIntMsg::getSL() const
{
    return SL_var;
}

void IBCCIntMsg::setSL(int SL)
{
    this->SL_var = SL;
}

int IBCCIntMsg::getFillvalue() const
{
    return fillvalue_var;
}

void IBCCIntMsg::setFillvalue(int fillvalue)
{
    this->fillvalue_var = fillvalue;
}

class IBCCIntMsgDescriptor : public cClassDescriptor
{
  public:
    IBCCIntMsgDescriptor();
    virtual ~IBCCIntMsgDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBCCIntMsgDescriptor);

IBCCIntMsgDescriptor::IBCCIntMsgDescriptor() : cClassDescriptor("IBCCIntMsg", "cPacket")
{
}

IBCCIntMsgDescriptor::~IBCCIntMsgDescriptor()
{
}

bool IBCCIntMsgDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBCCIntMsg *>(obj)!=NULL;
}

const char *IBCCIntMsgDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBCCIntMsgDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 7+basedesc->getFieldCount(object) : 7;
}

unsigned int IBCCIntMsgDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<7) ? fieldTypeFlags[field] : 0;
}

const char *IBCCIntMsgDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "VL",
        "type",
        "packetId",
        "dstLid",
        "srcLid",
        "SL",
        "fillvalue",
    };
    return (field>=0 && field<7) ? fieldNames[field] : NULL;
}

int IBCCIntMsgDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='V' && strcmp(fieldName, "VL")==0) return base+0;
    if (fieldName[0]=='t' && strcmp(fieldName, "type")==0) return base+1;
    if (fieldName[0]=='p' && strcmp(fieldName, "packetId")==0) return base+2;
    if (fieldName[0]=='d' && strcmp(fieldName, "dstLid")==0) return base+3;
    if (fieldName[0]=='s' && strcmp(fieldName, "srcLid")==0) return base+4;
    if (fieldName[0]=='S' && strcmp(fieldName, "SL")==0) return base+5;
    if (fieldName[0]=='f' && strcmp(fieldName, "fillvalue")==0) return base+6;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBCCIntMsgDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "short",
        "int",
        "int",
        "int",
        "int",
        "int",
        "int",
    };
    return (field>=0 && field<7) ? fieldTypeStrings[field] : NULL;
}

const char *IBCCIntMsgDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBCCIntMsgDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBCCIntMsg *pp = (IBCCIntMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBCCIntMsgDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBCCIntMsg *pp = (IBCCIntMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getVL());
        case 1: return long2string(pp->getType());
        case 2: return long2string(pp->getPacketId());
        case 3: return long2string(pp->getDstLid());
        case 4: return long2string(pp->getSrcLid());
        case 5: return long2string(pp->getSL());
        case 6: return long2string(pp->getFillvalue());
        default: return "";
    }
}

bool IBCCIntMsgDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBCCIntMsg *pp = (IBCCIntMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setVL(string2long(value)); return true;
        case 1: pp->setType(string2long(value)); return true;
        case 2: pp->setPacketId(string2long(value)); return true;
        case 3: pp->setDstLid(string2long(value)); return true;
        case 4: pp->setSrcLid(string2long(value)); return true;
        case 5: pp->setSL(string2long(value)); return true;
        case 6: pp->setFillvalue(string2long(value)); return true;
        default: return false;
    }
}

const char *IBCCIntMsgDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    };
    return (field>=0 && field<7) ? fieldStructNames[field] : NULL;
}

void *IBCCIntMsgDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBCCIntMsg *pp = (IBCCIntMsg *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBFlowControl);

IBFlowControl::IBFlowControl(const char *name, int kind) : IBWireMsg(name,kind)
{
    this->FCTBS_var = 0;
    this->FCCL_var = 0;
}

IBFlowControl::IBFlowControl(const IBFlowControl& other) : IBWireMsg(other)
{
    copy(other);
}

IBFlowControl::~IBFlowControl()
{
}

IBFlowControl& IBFlowControl::operator=(const IBFlowControl& other)
{
    if (this==&other) return *this;
    IBWireMsg::operator=(other);
    copy(other);
    return *this;
}

void IBFlowControl::copy(const IBFlowControl& other)
{
    this->FCTBS_var = other.FCTBS_var;
    this->FCCL_var = other.FCCL_var;
}

void IBFlowControl::parsimPack(cCommBuffer *b)
{
    IBWireMsg::parsimPack(b);
    doPacking(b,this->FCTBS_var);
    doPacking(b,this->FCCL_var);
}

void IBFlowControl::parsimUnpack(cCommBuffer *b)
{
    IBWireMsg::parsimUnpack(b);
    doUnpacking(b,this->FCTBS_var);
    doUnpacking(b,this->FCCL_var);
}

long IBFlowControl::getFCTBS() const
{
    return FCTBS_var;
}

void IBFlowControl::setFCTBS(long FCTBS)
{
    this->FCTBS_var = FCTBS;
}

long IBFlowControl::getFCCL() const
{
    return FCCL_var;
}

void IBFlowControl::setFCCL(long FCCL)
{
    this->FCCL_var = FCCL;
}

class IBFlowControlDescriptor : public cClassDescriptor
{
  public:
    IBFlowControlDescriptor();
    virtual ~IBFlowControlDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBFlowControlDescriptor);

IBFlowControlDescriptor::IBFlowControlDescriptor() : cClassDescriptor("IBFlowControl", "IBWireMsg")
{
}

IBFlowControlDescriptor::~IBFlowControlDescriptor()
{
}

bool IBFlowControlDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBFlowControl *>(obj)!=NULL;
}

const char *IBFlowControlDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBFlowControlDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 2+basedesc->getFieldCount(object) : 2;
}

unsigned int IBFlowControlDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<2) ? fieldTypeFlags[field] : 0;
}

const char *IBFlowControlDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "FCTBS",
        "FCCL",
    };
    return (field>=0 && field<2) ? fieldNames[field] : NULL;
}

int IBFlowControlDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='F' && strcmp(fieldName, "FCTBS")==0) return base+0;
    if (fieldName[0]=='F' && strcmp(fieldName, "FCCL")==0) return base+1;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBFlowControlDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "long",
        "long",
    };
    return (field>=0 && field<2) ? fieldTypeStrings[field] : NULL;
}

const char *IBFlowControlDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBFlowControlDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBFlowControl *pp = (IBFlowControl *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBFlowControlDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBFlowControl *pp = (IBFlowControl *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getFCTBS());
        case 1: return long2string(pp->getFCCL());
        default: return "";
    }
}

bool IBFlowControlDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBFlowControl *pp = (IBFlowControl *)object; (void)pp;
    switch (field) {
        case 0: pp->setFCTBS(string2long(value)); return true;
        case 1: pp->setFCCL(string2long(value)); return true;
        default: return false;
    }
}

const char *IBFlowControlDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
    };
    return (field>=0 && field<2) ? fieldStructNames[field] : NULL;
}

void *IBFlowControlDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBFlowControl *pp = (IBFlowControl *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBTQxVLPushPopMsg);

IBTQxVLPushPopMsg::IBTQxVLPushPopMsg(const char *name, int kind) : cPacket(name,kind)
{
    this->port_var = 0;
}

IBTQxVLPushPopMsg::IBTQxVLPushPopMsg(const IBTQxVLPushPopMsg& other) : cPacket(other)
{
    copy(other);
}

IBTQxVLPushPopMsg::~IBTQxVLPushPopMsg()
{
}

IBTQxVLPushPopMsg& IBTQxVLPushPopMsg::operator=(const IBTQxVLPushPopMsg& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void IBTQxVLPushPopMsg::copy(const IBTQxVLPushPopMsg& other)
{
    this->port_var = other.port_var;
}

void IBTQxVLPushPopMsg::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->port_var);
}

void IBTQxVLPushPopMsg::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->port_var);
}

int IBTQxVLPushPopMsg::getPort() const
{
    return port_var;
}

void IBTQxVLPushPopMsg::setPort(int port)
{
    this->port_var = port;
}

class IBTQxVLPushPopMsgDescriptor : public cClassDescriptor
{
  public:
    IBTQxVLPushPopMsgDescriptor();
    virtual ~IBTQxVLPushPopMsgDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBTQxVLPushPopMsgDescriptor);

IBTQxVLPushPopMsgDescriptor::IBTQxVLPushPopMsgDescriptor() : cClassDescriptor("IBTQxVLPushPopMsg", "cPacket")
{
}

IBTQxVLPushPopMsgDescriptor::~IBTQxVLPushPopMsgDescriptor()
{
}

bool IBTQxVLPushPopMsgDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBTQxVLPushPopMsg *>(obj)!=NULL;
}

const char *IBTQxVLPushPopMsgDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBTQxVLPushPopMsgDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 1+basedesc->getFieldCount(object) : 1;
}

unsigned int IBTQxVLPushPopMsgDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
    };
    return (field>=0 && field<1) ? fieldTypeFlags[field] : 0;
}

const char *IBTQxVLPushPopMsgDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "port",
    };
    return (field>=0 && field<1) ? fieldNames[field] : NULL;
}

int IBTQxVLPushPopMsgDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='p' && strcmp(fieldName, "port")==0) return base+0;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBTQxVLPushPopMsgDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
    };
    return (field>=0 && field<1) ? fieldTypeStrings[field] : NULL;
}

const char *IBTQxVLPushPopMsgDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBTQxVLPushPopMsgDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBTQxVLPushPopMsg *pp = (IBTQxVLPushPopMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBTQxVLPushPopMsgDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBTQxVLPushPopMsg *pp = (IBTQxVLPushPopMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getPort());
        default: return "";
    }
}

bool IBTQxVLPushPopMsgDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBTQxVLPushPopMsg *pp = (IBTQxVLPushPopMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setPort(string2long(value)); return true;
        default: return false;
    }
}

const char *IBTQxVLPushPopMsgDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
    };
    return (field>=0 && field<1) ? fieldStructNames[field] : NULL;
}

void *IBTQxVLPushPopMsgDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBTQxVLPushPopMsg *pp = (IBTQxVLPushPopMsg *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBArbInvMsg);

IBArbInvMsg::IBArbInvMsg(const char *name, int kind) : cPacket(name,kind)
{
    this->idx_var = 0;
}

IBArbInvMsg::IBArbInvMsg(const IBArbInvMsg& other) : cPacket(other)
{
    copy(other);
}

IBArbInvMsg::~IBArbInvMsg()
{
}

IBArbInvMsg& IBArbInvMsg::operator=(const IBArbInvMsg& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void IBArbInvMsg::copy(const IBArbInvMsg& other)
{
    this->idx_var = other.idx_var;
}

void IBArbInvMsg::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->idx_var);
}

void IBArbInvMsg::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->idx_var);
}

int IBArbInvMsg::getIdx() const
{
    return idx_var;
}

void IBArbInvMsg::setIdx(int idx)
{
    this->idx_var = idx;
}

class IBArbInvMsgDescriptor : public cClassDescriptor
{
  public:
    IBArbInvMsgDescriptor();
    virtual ~IBArbInvMsgDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBArbInvMsgDescriptor);

IBArbInvMsgDescriptor::IBArbInvMsgDescriptor() : cClassDescriptor("IBArbInvMsg", "cPacket")
{
}

IBArbInvMsgDescriptor::~IBArbInvMsgDescriptor()
{
}

bool IBArbInvMsgDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBArbInvMsg *>(obj)!=NULL;
}

const char *IBArbInvMsgDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBArbInvMsgDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 1+basedesc->getFieldCount(object) : 1;
}

unsigned int IBArbInvMsgDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
    };
    return (field>=0 && field<1) ? fieldTypeFlags[field] : 0;
}

const char *IBArbInvMsgDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "idx",
    };
    return (field>=0 && field<1) ? fieldNames[field] : NULL;
}

int IBArbInvMsgDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='i' && strcmp(fieldName, "idx")==0) return base+0;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBArbInvMsgDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
    };
    return (field>=0 && field<1) ? fieldTypeStrings[field] : NULL;
}

const char *IBArbInvMsgDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBArbInvMsgDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBArbInvMsg *pp = (IBArbInvMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBArbInvMsgDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBArbInvMsg *pp = (IBArbInvMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getIdx());
        default: return "";
    }
}

bool IBArbInvMsgDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBArbInvMsg *pp = (IBArbInvMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setIdx(string2long(value)); return true;
        default: return false;
    }
}

const char *IBArbInvMsgDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
    };
    return (field>=0 && field<1) ? fieldStructNames[field] : NULL;
}

void *IBArbInvMsgDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBArbInvMsg *pp = (IBArbInvMsg *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBDescMsg);

IBDescMsg::IBDescMsg(const char *name, int kind) : IBWireMsg(name,kind)
{
    this->sPortID_var = 0;
    this->dPortID_var = 0;
    this->oVL_var = 0;
    this->SwTimeStamp_var = 0;
    this->stfwd_var = 0;
    this->ratio_var = 0;
}

IBDescMsg::IBDescMsg(const IBDescMsg& other) : IBWireMsg(other)
{
    copy(other);
}

IBDescMsg::~IBDescMsg()
{
}

IBDescMsg& IBDescMsg::operator=(const IBDescMsg& other)
{
    if (this==&other) return *this;
    IBWireMsg::operator=(other);
    copy(other);
    return *this;
}

void IBDescMsg::copy(const IBDescMsg& other)
{
    this->sPortID_var = other.sPortID_var;
    this->dPortID_var = other.dPortID_var;
    this->oVL_var = other.oVL_var;
    this->SwTimeStamp_var = other.SwTimeStamp_var;
    this->stfwd_var = other.stfwd_var;
    this->ratio_var = other.ratio_var;
}

void IBDescMsg::parsimPack(cCommBuffer *b)
{
    IBWireMsg::parsimPack(b);
    doPacking(b,this->sPortID_var);
    doPacking(b,this->dPortID_var);
    doPacking(b,this->oVL_var);
    doPacking(b,this->SwTimeStamp_var);
    doPacking(b,this->stfwd_var);
    doPacking(b,this->ratio_var);
}

void IBDescMsg::parsimUnpack(cCommBuffer *b)
{
    IBWireMsg::parsimUnpack(b);
    doUnpacking(b,this->sPortID_var);
    doUnpacking(b,this->dPortID_var);
    doUnpacking(b,this->oVL_var);
    doUnpacking(b,this->SwTimeStamp_var);
    doUnpacking(b,this->stfwd_var);
    doUnpacking(b,this->ratio_var);
}

int IBDescMsg::getSPortID() const
{
    return sPortID_var;
}

void IBDescMsg::setSPortID(int sPortID)
{
    this->sPortID_var = sPortID;
}

int IBDescMsg::getDPortID() const
{
    return dPortID_var;
}

void IBDescMsg::setDPortID(int dPortID)
{
    this->dPortID_var = dPortID;
}

short IBDescMsg::getOVL() const
{
    return oVL_var;
}

void IBDescMsg::setOVL(short oVL)
{
    this->oVL_var = oVL;
}

simtime_t IBDescMsg::getSwTimeStamp() const
{
    return SwTimeStamp_var;
}

void IBDescMsg::setSwTimeStamp(simtime_t SwTimeStamp)
{
    this->SwTimeStamp_var = SwTimeStamp;
}

bool IBDescMsg::getStfwd() const
{
    return stfwd_var;
}

void IBDescMsg::setStfwd(bool stfwd)
{
    this->stfwd_var = stfwd;
}

double IBDescMsg::getRatio() const
{
    return ratio_var;
}

void IBDescMsg::setRatio(double ratio)
{
    this->ratio_var = ratio;
}

class IBDescMsgDescriptor : public cClassDescriptor
{
  public:
    IBDescMsgDescriptor();
    virtual ~IBDescMsgDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBDescMsgDescriptor);

IBDescMsgDescriptor::IBDescMsgDescriptor() : cClassDescriptor("IBDescMsg", "IBWireMsg")
{
}

IBDescMsgDescriptor::~IBDescMsgDescriptor()
{
}

bool IBDescMsgDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBDescMsg *>(obj)!=NULL;
}

const char *IBDescMsgDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBDescMsgDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 6+basedesc->getFieldCount(object) : 6;
}

unsigned int IBDescMsgDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<6) ? fieldTypeFlags[field] : 0;
}

const char *IBDescMsgDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "sPortID",
        "dPortID",
        "oVL",
        "SwTimeStamp",
        "stfwd",
        "ratio",
    };
    return (field>=0 && field<6) ? fieldNames[field] : NULL;
}

int IBDescMsgDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='s' && strcmp(fieldName, "sPortID")==0) return base+0;
    if (fieldName[0]=='d' && strcmp(fieldName, "dPortID")==0) return base+1;
    if (fieldName[0]=='o' && strcmp(fieldName, "oVL")==0) return base+2;
    if (fieldName[0]=='S' && strcmp(fieldName, "SwTimeStamp")==0) return base+3;
    if (fieldName[0]=='s' && strcmp(fieldName, "stfwd")==0) return base+4;
    if (fieldName[0]=='r' && strcmp(fieldName, "ratio")==0) return base+5;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBDescMsgDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "int",
        "short",
        "simtime_t",
        "bool",
        "double",
    };
    return (field>=0 && field<6) ? fieldTypeStrings[field] : NULL;
}

const char *IBDescMsgDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBDescMsgDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBDescMsg *pp = (IBDescMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBDescMsgDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBDescMsg *pp = (IBDescMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getSPortID());
        case 1: return long2string(pp->getDPortID());
        case 2: return long2string(pp->getOVL());
        case 3: return double2string(pp->getSwTimeStamp());
        case 4: return bool2string(pp->getStfwd());
        case 5: return double2string(pp->getRatio());
        default: return "";
    }
}

bool IBDescMsgDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBDescMsg *pp = (IBDescMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setSPortID(string2long(value)); return true;
        case 1: pp->setDPortID(string2long(value)); return true;
        case 2: pp->setOVL(string2long(value)); return true;
        case 3: pp->setSwTimeStamp(string2double(value)); return true;
        case 4: pp->setStfwd(string2bool(value)); return true;
        case 5: pp->setRatio(string2double(value)); return true;
        default: return false;
    }
}

const char *IBDescMsgDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    };
    return (field>=0 && field<6) ? fieldStructNames[field] : NULL;
}

void *IBDescMsgDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBDescMsg *pp = (IBDescMsg *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBTxCredMsg);

IBTxCredMsg::IBTxCredMsg(const char *name, int kind) : cPacket(name,kind)
{
    this->VL_var = 0;
    this->FCCL_var = 0;
    this->FCTBS_var = 0;
}

IBTxCredMsg::IBTxCredMsg(const IBTxCredMsg& other) : cPacket(other)
{
    copy(other);
}

IBTxCredMsg::~IBTxCredMsg()
{
}

IBTxCredMsg& IBTxCredMsg::operator=(const IBTxCredMsg& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void IBTxCredMsg::copy(const IBTxCredMsg& other)
{
    this->VL_var = other.VL_var;
    this->FCCL_var = other.FCCL_var;
    this->FCTBS_var = other.FCTBS_var;
}

void IBTxCredMsg::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->VL_var);
    doPacking(b,this->FCCL_var);
    doPacking(b,this->FCTBS_var);
}

void IBTxCredMsg::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->VL_var);
    doUnpacking(b,this->FCCL_var);
    doUnpacking(b,this->FCTBS_var);
}

int IBTxCredMsg::getVL() const
{
    return VL_var;
}

void IBTxCredMsg::setVL(int VL)
{
    this->VL_var = VL;
}

long IBTxCredMsg::getFCCL() const
{
    return FCCL_var;
}

void IBTxCredMsg::setFCCL(long FCCL)
{
    this->FCCL_var = FCCL;
}

long IBTxCredMsg::getFCTBS() const
{
    return FCTBS_var;
}

void IBTxCredMsg::setFCTBS(long FCTBS)
{
    this->FCTBS_var = FCTBS;
}

class IBTxCredMsgDescriptor : public cClassDescriptor
{
  public:
    IBTxCredMsgDescriptor();
    virtual ~IBTxCredMsgDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBTxCredMsgDescriptor);

IBTxCredMsgDescriptor::IBTxCredMsgDescriptor() : cClassDescriptor("IBTxCredMsg", "cPacket")
{
}

IBTxCredMsgDescriptor::~IBTxCredMsgDescriptor()
{
}

bool IBTxCredMsgDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBTxCredMsg *>(obj)!=NULL;
}

const char *IBTxCredMsgDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBTxCredMsgDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 3+basedesc->getFieldCount(object) : 3;
}

unsigned int IBTxCredMsgDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<3) ? fieldTypeFlags[field] : 0;
}

const char *IBTxCredMsgDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "VL",
        "FCCL",
        "FCTBS",
    };
    return (field>=0 && field<3) ? fieldNames[field] : NULL;
}

int IBTxCredMsgDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='V' && strcmp(fieldName, "VL")==0) return base+0;
    if (fieldName[0]=='F' && strcmp(fieldName, "FCCL")==0) return base+1;
    if (fieldName[0]=='F' && strcmp(fieldName, "FCTBS")==0) return base+2;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBTxCredMsgDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "long",
        "long",
    };
    return (field>=0 && field<3) ? fieldTypeStrings[field] : NULL;
}

const char *IBTxCredMsgDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBTxCredMsgDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBTxCredMsg *pp = (IBTxCredMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBTxCredMsgDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBTxCredMsg *pp = (IBTxCredMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getVL());
        case 1: return long2string(pp->getFCCL());
        case 2: return long2string(pp->getFCTBS());
        default: return "";
    }
}

bool IBTxCredMsgDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBTxCredMsg *pp = (IBTxCredMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setVL(string2long(value)); return true;
        case 1: pp->setFCCL(string2long(value)); return true;
        case 2: pp->setFCTBS(string2long(value)); return true;
        default: return false;
    }
}

const char *IBTxCredMsgDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
        NULL,
    };
    return (field>=0 && field<3) ? fieldStructNames[field] : NULL;
}

void *IBTxCredMsgDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBTxCredMsg *pp = (IBTxCredMsg *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBRxCredMsg);

IBRxCredMsg::IBRxCredMsg(const char *name, int kind) : cPacket(name,kind)
{
    this->VL_var = 0;
    this->FCCL_var = 0;
}

IBRxCredMsg::IBRxCredMsg(const IBRxCredMsg& other) : cPacket(other)
{
    copy(other);
}

IBRxCredMsg::~IBRxCredMsg()
{
}

IBRxCredMsg& IBRxCredMsg::operator=(const IBRxCredMsg& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void IBRxCredMsg::copy(const IBRxCredMsg& other)
{
    this->VL_var = other.VL_var;
    this->FCCL_var = other.FCCL_var;
}

void IBRxCredMsg::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->VL_var);
    doPacking(b,this->FCCL_var);
}

void IBRxCredMsg::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->VL_var);
    doUnpacking(b,this->FCCL_var);
}

int IBRxCredMsg::getVL() const
{
    return VL_var;
}

void IBRxCredMsg::setVL(int VL)
{
    this->VL_var = VL;
}

long IBRxCredMsg::getFCCL() const
{
    return FCCL_var;
}

void IBRxCredMsg::setFCCL(long FCCL)
{
    this->FCCL_var = FCCL;
}

class IBRxCredMsgDescriptor : public cClassDescriptor
{
  public:
    IBRxCredMsgDescriptor();
    virtual ~IBRxCredMsgDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBRxCredMsgDescriptor);

IBRxCredMsgDescriptor::IBRxCredMsgDescriptor() : cClassDescriptor("IBRxCredMsg", "cPacket")
{
}

IBRxCredMsgDescriptor::~IBRxCredMsgDescriptor()
{
}

bool IBRxCredMsgDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBRxCredMsg *>(obj)!=NULL;
}

const char *IBRxCredMsgDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBRxCredMsgDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 2+basedesc->getFieldCount(object) : 2;
}

unsigned int IBRxCredMsgDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<2) ? fieldTypeFlags[field] : 0;
}

const char *IBRxCredMsgDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "VL",
        "FCCL",
    };
    return (field>=0 && field<2) ? fieldNames[field] : NULL;
}

int IBRxCredMsgDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='V' && strcmp(fieldName, "VL")==0) return base+0;
    if (fieldName[0]=='F' && strcmp(fieldName, "FCCL")==0) return base+1;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBRxCredMsgDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "long",
    };
    return (field>=0 && field<2) ? fieldTypeStrings[field] : NULL;
}

const char *IBRxCredMsgDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBRxCredMsgDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBRxCredMsg *pp = (IBRxCredMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBRxCredMsgDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBRxCredMsg *pp = (IBRxCredMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getVL());
        case 1: return long2string(pp->getFCCL());
        default: return "";
    }
}

bool IBRxCredMsgDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBRxCredMsg *pp = (IBRxCredMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setVL(string2long(value)); return true;
        case 1: pp->setFCCL(string2long(value)); return true;
        default: return false;
    }
}

const char *IBRxCredMsgDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
    };
    return (field>=0 && field<2) ? fieldStructNames[field] : NULL;
}

void *IBRxCredMsgDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBRxCredMsg *pp = (IBRxCredMsg *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBFifoStatus);

IBFifoStatus::IBFifoStatus(const char *name, int kind) : cPacket(name,kind)
{
    this->full_var = 0;
    this->FIFOindex_var = 0;
}

IBFifoStatus::IBFifoStatus(const IBFifoStatus& other) : cPacket(other)
{
    copy(other);
}

IBFifoStatus::~IBFifoStatus()
{
}

IBFifoStatus& IBFifoStatus::operator=(const IBFifoStatus& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void IBFifoStatus::copy(const IBFifoStatus& other)
{
    this->full_var = other.full_var;
    this->FIFOindex_var = other.FIFOindex_var;
}

void IBFifoStatus::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->full_var);
    doPacking(b,this->FIFOindex_var);
}

void IBFifoStatus::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->full_var);
    doUnpacking(b,this->FIFOindex_var);
}

bool IBFifoStatus::getFull() const
{
    return full_var;
}

void IBFifoStatus::setFull(bool full)
{
    this->full_var = full;
}

int IBFifoStatus::getFIFOindex() const
{
    return FIFOindex_var;
}

void IBFifoStatus::setFIFOindex(int FIFOindex)
{
    this->FIFOindex_var = FIFOindex;
}

class IBFifoStatusDescriptor : public cClassDescriptor
{
  public:
    IBFifoStatusDescriptor();
    virtual ~IBFifoStatusDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBFifoStatusDescriptor);

IBFifoStatusDescriptor::IBFifoStatusDescriptor() : cClassDescriptor("IBFifoStatus", "cPacket")
{
}

IBFifoStatusDescriptor::~IBFifoStatusDescriptor()
{
}

bool IBFifoStatusDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBFifoStatus *>(obj)!=NULL;
}

const char *IBFifoStatusDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBFifoStatusDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 2+basedesc->getFieldCount(object) : 2;
}

unsigned int IBFifoStatusDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<2) ? fieldTypeFlags[field] : 0;
}

const char *IBFifoStatusDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "full",
        "FIFOindex",
    };
    return (field>=0 && field<2) ? fieldNames[field] : NULL;
}

int IBFifoStatusDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='f' && strcmp(fieldName, "full")==0) return base+0;
    if (fieldName[0]=='F' && strcmp(fieldName, "FIFOindex")==0) return base+1;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBFifoStatusDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "bool",
        "int",
    };
    return (field>=0 && field<2) ? fieldTypeStrings[field] : NULL;
}

const char *IBFifoStatusDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBFifoStatusDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBFifoStatus *pp = (IBFifoStatus *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBFifoStatusDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBFifoStatus *pp = (IBFifoStatus *)object; (void)pp;
    switch (field) {
        case 0: return bool2string(pp->getFull());
        case 1: return long2string(pp->getFIFOindex());
        default: return "";
    }
}

bool IBFifoStatusDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBFifoStatus *pp = (IBFifoStatus *)object; (void)pp;
    switch (field) {
        case 0: pp->setFull(string2bool(value)); return true;
        case 1: pp->setFIFOindex(string2long(value)); return true;
        default: return false;
    }
}

const char *IBFifoStatusDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
    };
    return (field>=0 && field<2) ? fieldStructNames[field] : NULL;
}

void *IBFifoStatusDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBFifoStatus *pp = (IBFifoStatus *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(FDBXDone);

FDBXDone::FDBXDone(const char *name, int kind) : cPacket(name,kind)
{
    this->FDBindex_var = 0;
    this->FTnumber_var = 0;
}

FDBXDone::FDBXDone(const FDBXDone& other) : cPacket(other)
{
    copy(other);
}

FDBXDone::~FDBXDone()
{
}

FDBXDone& FDBXDone::operator=(const FDBXDone& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void FDBXDone::copy(const FDBXDone& other)
{
    this->FDBindex_var = other.FDBindex_var;
    this->FTnumber_var = other.FTnumber_var;
}

void FDBXDone::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->FDBindex_var);
    doPacking(b,this->FTnumber_var);
}

void FDBXDone::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->FDBindex_var);
    doUnpacking(b,this->FTnumber_var);
}

int FDBXDone::getFDBindex() const
{
    return FDBindex_var;
}

void FDBXDone::setFDBindex(int FDBindex)
{
    this->FDBindex_var = FDBindex;
}

int FDBXDone::getFTnumber() const
{
    return FTnumber_var;
}

void FDBXDone::setFTnumber(int FTnumber)
{
    this->FTnumber_var = FTnumber;
}

class FDBXDoneDescriptor : public cClassDescriptor
{
  public:
    FDBXDoneDescriptor();
    virtual ~FDBXDoneDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(FDBXDoneDescriptor);

FDBXDoneDescriptor::FDBXDoneDescriptor() : cClassDescriptor("FDBXDone", "cPacket")
{
}

FDBXDoneDescriptor::~FDBXDoneDescriptor()
{
}

bool FDBXDoneDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<FDBXDone *>(obj)!=NULL;
}

const char *FDBXDoneDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int FDBXDoneDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 2+basedesc->getFieldCount(object) : 2;
}

unsigned int FDBXDoneDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<2) ? fieldTypeFlags[field] : 0;
}

const char *FDBXDoneDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "FDBindex",
        "FTnumber",
    };
    return (field>=0 && field<2) ? fieldNames[field] : NULL;
}

int FDBXDoneDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='F' && strcmp(fieldName, "FDBindex")==0) return base+0;
    if (fieldName[0]=='F' && strcmp(fieldName, "FTnumber")==0) return base+1;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *FDBXDoneDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "int",
    };
    return (field>=0 && field<2) ? fieldTypeStrings[field] : NULL;
}

const char *FDBXDoneDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int FDBXDoneDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    FDBXDone *pp = (FDBXDone *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string FDBXDoneDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    FDBXDone *pp = (FDBXDone *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getFDBindex());
        case 1: return long2string(pp->getFTnumber());
        default: return "";
    }
}

bool FDBXDoneDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    FDBXDone *pp = (FDBXDone *)object; (void)pp;
    switch (field) {
        case 0: pp->setFDBindex(string2long(value)); return true;
        case 1: pp->setFTnumber(string2long(value)); return true;
        default: return false;
    }
}

const char *FDBXDoneDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
    };
    return (field>=0 && field<2) ? fieldStructNames[field] : NULL;
}

void *FDBXDoneDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    FDBXDone *pp = (FDBXDone *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBAPacketID);

IBAPacketID::IBAPacketID(const char *name, int kind) : cPacket(name,kind)
{
    this->PacketID_var = 0;
}

IBAPacketID::IBAPacketID(const IBAPacketID& other) : cPacket(other)
{
    copy(other);
}

IBAPacketID::~IBAPacketID()
{
}

IBAPacketID& IBAPacketID::operator=(const IBAPacketID& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void IBAPacketID::copy(const IBAPacketID& other)
{
    this->PacketID_var = other.PacketID_var;
}

void IBAPacketID::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->PacketID_var);
}

void IBAPacketID::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->PacketID_var);
}

int IBAPacketID::getPacketID() const
{
    return PacketID_var;
}

void IBAPacketID::setPacketID(int PacketID)
{
    this->PacketID_var = PacketID;
}

class IBAPacketIDDescriptor : public cClassDescriptor
{
  public:
    IBAPacketIDDescriptor();
    virtual ~IBAPacketIDDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBAPacketIDDescriptor);

IBAPacketIDDescriptor::IBAPacketIDDescriptor() : cClassDescriptor("IBAPacketID", "cPacket")
{
}

IBAPacketIDDescriptor::~IBAPacketIDDescriptor()
{
}

bool IBAPacketIDDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBAPacketID *>(obj)!=NULL;
}

const char *IBAPacketIDDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBAPacketIDDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 1+basedesc->getFieldCount(object) : 1;
}

unsigned int IBAPacketIDDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
    };
    return (field>=0 && field<1) ? fieldTypeFlags[field] : 0;
}

const char *IBAPacketIDDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "PacketID",
    };
    return (field>=0 && field<1) ? fieldNames[field] : NULL;
}

int IBAPacketIDDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='P' && strcmp(fieldName, "PacketID")==0) return base+0;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBAPacketIDDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
    };
    return (field>=0 && field<1) ? fieldTypeStrings[field] : NULL;
}

const char *IBAPacketIDDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBAPacketIDDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBAPacketID *pp = (IBAPacketID *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBAPacketIDDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBAPacketID *pp = (IBAPacketID *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getPacketID());
        default: return "";
    }
}

bool IBAPacketIDDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBAPacketID *pp = (IBAPacketID *)object; (void)pp;
    switch (field) {
        case 0: pp->setPacketID(string2long(value)); return true;
        default: return false;
    }
}

const char *IBAPacketIDDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
    };
    return (field>=0 && field<1) ? fieldStructNames[field] : NULL;
}

void *IBAPacketIDDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBAPacketID *pp = (IBAPacketID *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(RQxTQxVLMsg);

RQxTQxVLMsg::RQxTQxVLMsg(const char *name, int kind) : cPacket(name,kind)
{
    this->RQ_var = 0;
    this->VL_var = 0;
    this->TQ_var = 0;
    this->PacketID_var = 0;
}

RQxTQxVLMsg::RQxTQxVLMsg(const RQxTQxVLMsg& other) : cPacket(other)
{
    copy(other);
}

RQxTQxVLMsg::~RQxTQxVLMsg()
{
}

RQxTQxVLMsg& RQxTQxVLMsg::operator=(const RQxTQxVLMsg& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void RQxTQxVLMsg::copy(const RQxTQxVLMsg& other)
{
    this->RQ_var = other.RQ_var;
    this->VL_var = other.VL_var;
    this->TQ_var = other.TQ_var;
    this->PacketID_var = other.PacketID_var;
}

void RQxTQxVLMsg::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->RQ_var);
    doPacking(b,this->VL_var);
    doPacking(b,this->TQ_var);
    doPacking(b,this->PacketID_var);
}

void RQxTQxVLMsg::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->RQ_var);
    doUnpacking(b,this->VL_var);
    doUnpacking(b,this->TQ_var);
    doUnpacking(b,this->PacketID_var);
}

int RQxTQxVLMsg::getRQ() const
{
    return RQ_var;
}

void RQxTQxVLMsg::setRQ(int RQ)
{
    this->RQ_var = RQ;
}

int RQxTQxVLMsg::getVL() const
{
    return VL_var;
}

void RQxTQxVLMsg::setVL(int VL)
{
    this->VL_var = VL;
}

int RQxTQxVLMsg::getTQ() const
{
    return TQ_var;
}

void RQxTQxVLMsg::setTQ(int TQ)
{
    this->TQ_var = TQ;
}

int RQxTQxVLMsg::getPacketID() const
{
    return PacketID_var;
}

void RQxTQxVLMsg::setPacketID(int PacketID)
{
    this->PacketID_var = PacketID;
}

class RQxTQxVLMsgDescriptor : public cClassDescriptor
{
  public:
    RQxTQxVLMsgDescriptor();
    virtual ~RQxTQxVLMsgDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(RQxTQxVLMsgDescriptor);

RQxTQxVLMsgDescriptor::RQxTQxVLMsgDescriptor() : cClassDescriptor("RQxTQxVLMsg", "cPacket")
{
}

RQxTQxVLMsgDescriptor::~RQxTQxVLMsgDescriptor()
{
}

bool RQxTQxVLMsgDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<RQxTQxVLMsg *>(obj)!=NULL;
}

const char *RQxTQxVLMsgDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int RQxTQxVLMsgDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 4+basedesc->getFieldCount(object) : 4;
}

unsigned int RQxTQxVLMsgDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<4) ? fieldTypeFlags[field] : 0;
}

const char *RQxTQxVLMsgDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "RQ",
        "VL",
        "TQ",
        "PacketID",
    };
    return (field>=0 && field<4) ? fieldNames[field] : NULL;
}

int RQxTQxVLMsgDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='R' && strcmp(fieldName, "RQ")==0) return base+0;
    if (fieldName[0]=='V' && strcmp(fieldName, "VL")==0) return base+1;
    if (fieldName[0]=='T' && strcmp(fieldName, "TQ")==0) return base+2;
    if (fieldName[0]=='P' && strcmp(fieldName, "PacketID")==0) return base+3;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *RQxTQxVLMsgDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "int",
        "int",
        "int",
    };
    return (field>=0 && field<4) ? fieldTypeStrings[field] : NULL;
}

const char *RQxTQxVLMsgDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int RQxTQxVLMsgDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    RQxTQxVLMsg *pp = (RQxTQxVLMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string RQxTQxVLMsgDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    RQxTQxVLMsg *pp = (RQxTQxVLMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getRQ());
        case 1: return long2string(pp->getVL());
        case 2: return long2string(pp->getTQ());
        case 3: return long2string(pp->getPacketID());
        default: return "";
    }
}

bool RQxTQxVLMsgDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    RQxTQxVLMsg *pp = (RQxTQxVLMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setRQ(string2long(value)); return true;
        case 1: pp->setVL(string2long(value)); return true;
        case 2: pp->setTQ(string2long(value)); return true;
        case 3: pp->setPacketID(string2long(value)); return true;
        default: return false;
    }
}

const char *RQxTQxVLMsgDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
        NULL,
        NULL,
    };
    return (field>=0 && field<4) ? fieldStructNames[field] : NULL;
}

void *RQxTQxVLMsgDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    RQxTQxVLMsg *pp = (RQxTQxVLMsg *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBSentMsg);

IBSentMsg::IBSentMsg(const char *name, int kind) : cPacket(name,kind)
{
    this->VL_var = 0;
    this->usedStatic_var = 0;
    this->wasLast_var = 0;
}

IBSentMsg::IBSentMsg(const IBSentMsg& other) : cPacket(other)
{
    copy(other);
}

IBSentMsg::~IBSentMsg()
{
}

IBSentMsg& IBSentMsg::operator=(const IBSentMsg& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void IBSentMsg::copy(const IBSentMsg& other)
{
    this->VL_var = other.VL_var;
    this->usedStatic_var = other.usedStatic_var;
    this->wasLast_var = other.wasLast_var;
}

void IBSentMsg::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->VL_var);
    doPacking(b,this->usedStatic_var);
    doPacking(b,this->wasLast_var);
}

void IBSentMsg::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->VL_var);
    doUnpacking(b,this->usedStatic_var);
    doUnpacking(b,this->wasLast_var);
}

short IBSentMsg::getVL() const
{
    return VL_var;
}

void IBSentMsg::setVL(short VL)
{
    this->VL_var = VL;
}

short IBSentMsg::getUsedStatic() const
{
    return usedStatic_var;
}

void IBSentMsg::setUsedStatic(short usedStatic)
{
    this->usedStatic_var = usedStatic;
}

short IBSentMsg::getWasLast() const
{
    return wasLast_var;
}

void IBSentMsg::setWasLast(short wasLast)
{
    this->wasLast_var = wasLast;
}

class IBSentMsgDescriptor : public cClassDescriptor
{
  public:
    IBSentMsgDescriptor();
    virtual ~IBSentMsgDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBSentMsgDescriptor);

IBSentMsgDescriptor::IBSentMsgDescriptor() : cClassDescriptor("IBSentMsg", "cPacket")
{
}

IBSentMsgDescriptor::~IBSentMsgDescriptor()
{
}

bool IBSentMsgDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBSentMsg *>(obj)!=NULL;
}

const char *IBSentMsgDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBSentMsgDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 3+basedesc->getFieldCount(object) : 3;
}

unsigned int IBSentMsgDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<3) ? fieldTypeFlags[field] : 0;
}

const char *IBSentMsgDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "VL",
        "usedStatic",
        "wasLast",
    };
    return (field>=0 && field<3) ? fieldNames[field] : NULL;
}

int IBSentMsgDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='V' && strcmp(fieldName, "VL")==0) return base+0;
    if (fieldName[0]=='u' && strcmp(fieldName, "usedStatic")==0) return base+1;
    if (fieldName[0]=='w' && strcmp(fieldName, "wasLast")==0) return base+2;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBSentMsgDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "short",
        "short",
        "short",
    };
    return (field>=0 && field<3) ? fieldTypeStrings[field] : NULL;
}

const char *IBSentMsgDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBSentMsgDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBSentMsg *pp = (IBSentMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBSentMsgDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBSentMsg *pp = (IBSentMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getVL());
        case 1: return long2string(pp->getUsedStatic());
        case 2: return long2string(pp->getWasLast());
        default: return "";
    }
}

bool IBSentMsgDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBSentMsg *pp = (IBSentMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setVL(string2long(value)); return true;
        case 1: pp->setUsedStatic(string2long(value)); return true;
        case 2: pp->setWasLast(string2long(value)); return true;
        default: return false;
    }
}

const char *IBSentMsgDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
        NULL,
    };
    return (field>=0 && field<3) ? fieldStructNames[field] : NULL;
}

void *IBSentMsgDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBSentMsg *pp = (IBSentMsg *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBFreeMsg);

IBFreeMsg::IBFreeMsg(const char *name, int kind) : cPacket(name,kind)
{
    this->VL_var = 0;
}

IBFreeMsg::IBFreeMsg(const IBFreeMsg& other) : cPacket(other)
{
    copy(other);
}

IBFreeMsg::~IBFreeMsg()
{
}

IBFreeMsg& IBFreeMsg::operator=(const IBFreeMsg& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void IBFreeMsg::copy(const IBFreeMsg& other)
{
    this->VL_var = other.VL_var;
}

void IBFreeMsg::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->VL_var);
}

void IBFreeMsg::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->VL_var);
}

short IBFreeMsg::getVL() const
{
    return VL_var;
}

void IBFreeMsg::setVL(short VL)
{
    this->VL_var = VL;
}

class IBFreeMsgDescriptor : public cClassDescriptor
{
  public:
    IBFreeMsgDescriptor();
    virtual ~IBFreeMsgDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBFreeMsgDescriptor);

IBFreeMsgDescriptor::IBFreeMsgDescriptor() : cClassDescriptor("IBFreeMsg", "cPacket")
{
}

IBFreeMsgDescriptor::~IBFreeMsgDescriptor()
{
}

bool IBFreeMsgDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBFreeMsg *>(obj)!=NULL;
}

const char *IBFreeMsgDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBFreeMsgDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 1+basedesc->getFieldCount(object) : 1;
}

unsigned int IBFreeMsgDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
    };
    return (field>=0 && field<1) ? fieldTypeFlags[field] : 0;
}

const char *IBFreeMsgDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "VL",
    };
    return (field>=0 && field<1) ? fieldNames[field] : NULL;
}

int IBFreeMsgDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='V' && strcmp(fieldName, "VL")==0) return base+0;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBFreeMsgDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "short",
    };
    return (field>=0 && field<1) ? fieldTypeStrings[field] : NULL;
}

const char *IBFreeMsgDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBFreeMsgDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBFreeMsg *pp = (IBFreeMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBFreeMsgDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBFreeMsg *pp = (IBFreeMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getVL());
        default: return "";
    }
}

bool IBFreeMsgDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBFreeMsg *pp = (IBFreeMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setVL(string2long(value)); return true;
        default: return false;
    }
}

const char *IBFreeMsgDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
    };
    return (field>=0 && field<1) ? fieldStructNames[field] : NULL;
}

void *IBFreeMsgDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBFreeMsg *pp = (IBFreeMsg *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBDoneMsg);

IBDoneMsg::IBDoneMsg(const char *name, int kind) : cPacket(name,kind)
{
    this->VL_var = 0;
}

IBDoneMsg::IBDoneMsg(const IBDoneMsg& other) : cPacket(other)
{
    copy(other);
}

IBDoneMsg::~IBDoneMsg()
{
}

IBDoneMsg& IBDoneMsg::operator=(const IBDoneMsg& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void IBDoneMsg::copy(const IBDoneMsg& other)
{
    this->VL_var = other.VL_var;
}

void IBDoneMsg::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->VL_var);
}

void IBDoneMsg::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->VL_var);
}

short IBDoneMsg::getVL() const
{
    return VL_var;
}

void IBDoneMsg::setVL(short VL)
{
    this->VL_var = VL;
}

class IBDoneMsgDescriptor : public cClassDescriptor
{
  public:
    IBDoneMsgDescriptor();
    virtual ~IBDoneMsgDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBDoneMsgDescriptor);

IBDoneMsgDescriptor::IBDoneMsgDescriptor() : cClassDescriptor("IBDoneMsg", "cPacket")
{
}

IBDoneMsgDescriptor::~IBDoneMsgDescriptor()
{
}

bool IBDoneMsgDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBDoneMsg *>(obj)!=NULL;
}

const char *IBDoneMsgDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBDoneMsgDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 1+basedesc->getFieldCount(object) : 1;
}

unsigned int IBDoneMsgDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
    };
    return (field>=0 && field<1) ? fieldTypeFlags[field] : 0;
}

const char *IBDoneMsgDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "VL",
    };
    return (field>=0 && field<1) ? fieldNames[field] : NULL;
}

int IBDoneMsgDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='V' && strcmp(fieldName, "VL")==0) return base+0;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBDoneMsgDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "short",
    };
    return (field>=0 && field<1) ? fieldTypeStrings[field] : NULL;
}

const char *IBDoneMsgDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBDoneMsgDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBDoneMsg *pp = (IBDoneMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBDoneMsgDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBDoneMsg *pp = (IBDoneMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getVL());
        default: return "";
    }
}

bool IBDoneMsgDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBDoneMsg *pp = (IBDoneMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setVL(string2long(value)); return true;
        default: return false;
    }
}

const char *IBDoneMsgDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
    };
    return (field>=0 && field<1) ? fieldStructNames[field] : NULL;
}

void *IBDoneMsgDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBDoneMsg *pp = (IBDoneMsg *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(IBRxNumMsg);

IBRxNumMsg::IBRxNumMsg(const char *name, int kind) : cPacket(name,kind)
{
    this->RXPortId_var = 0;
}

IBRxNumMsg::IBRxNumMsg(const IBRxNumMsg& other) : cPacket(other)
{
    copy(other);
}

IBRxNumMsg::~IBRxNumMsg()
{
}

IBRxNumMsg& IBRxNumMsg::operator=(const IBRxNumMsg& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void IBRxNumMsg::copy(const IBRxNumMsg& other)
{
    this->RXPortId_var = other.RXPortId_var;
}

void IBRxNumMsg::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->RXPortId_var);
}

void IBRxNumMsg::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->RXPortId_var);
}

int IBRxNumMsg::getRXPortId() const
{
    return RXPortId_var;
}

void IBRxNumMsg::setRXPortId(int RXPortId)
{
    this->RXPortId_var = RXPortId;
}

class IBRxNumMsgDescriptor : public cClassDescriptor
{
  public:
    IBRxNumMsgDescriptor();
    virtual ~IBRxNumMsgDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(IBRxNumMsgDescriptor);

IBRxNumMsgDescriptor::IBRxNumMsgDescriptor() : cClassDescriptor("IBRxNumMsg", "cPacket")
{
}

IBRxNumMsgDescriptor::~IBRxNumMsgDescriptor()
{
}

bool IBRxNumMsgDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<IBRxNumMsg *>(obj)!=NULL;
}

const char *IBRxNumMsgDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int IBRxNumMsgDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 1+basedesc->getFieldCount(object) : 1;
}

unsigned int IBRxNumMsgDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
    };
    return (field>=0 && field<1) ? fieldTypeFlags[field] : 0;
}

const char *IBRxNumMsgDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "RXPortId",
    };
    return (field>=0 && field<1) ? fieldNames[field] : NULL;
}

int IBRxNumMsgDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='R' && strcmp(fieldName, "RXPortId")==0) return base+0;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *IBRxNumMsgDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
    };
    return (field>=0 && field<1) ? fieldTypeStrings[field] : NULL;
}

const char *IBRxNumMsgDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int IBRxNumMsgDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    IBRxNumMsg *pp = (IBRxNumMsg *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string IBRxNumMsgDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    IBRxNumMsg *pp = (IBRxNumMsg *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getRXPortId());
        default: return "";
    }
}

bool IBRxNumMsgDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    IBRxNumMsg *pp = (IBRxNumMsg *)object; (void)pp;
    switch (field) {
        case 0: pp->setRXPortId(string2long(value)); return true;
        default: return false;
    }
}

const char *IBRxNumMsgDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
    };
    return (field>=0 && field<1) ? fieldStructNames[field] : NULL;
}

void *IBRxNumMsgDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    IBRxNumMsg *pp = (IBRxNumMsg *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(FifoFree);

FifoFree::FifoFree(const char *name, int kind) : cPacket(name,kind)
{
    this->NumFreeCredits_var = 0;
}

FifoFree::FifoFree(const FifoFree& other) : cPacket(other)
{
    copy(other);
}

FifoFree::~FifoFree()
{
}

FifoFree& FifoFree::operator=(const FifoFree& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void FifoFree::copy(const FifoFree& other)
{
    this->NumFreeCredits_var = other.NumFreeCredits_var;
}

void FifoFree::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->NumFreeCredits_var);
}

void FifoFree::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->NumFreeCredits_var);
}

int FifoFree::getNumFreeCredits() const
{
    return NumFreeCredits_var;
}

void FifoFree::setNumFreeCredits(int NumFreeCredits)
{
    this->NumFreeCredits_var = NumFreeCredits;
}

class FifoFreeDescriptor : public cClassDescriptor
{
  public:
    FifoFreeDescriptor();
    virtual ~FifoFreeDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(FifoFreeDescriptor);

FifoFreeDescriptor::FifoFreeDescriptor() : cClassDescriptor("FifoFree", "cPacket")
{
}

FifoFreeDescriptor::~FifoFreeDescriptor()
{
}

bool FifoFreeDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<FifoFree *>(obj)!=NULL;
}

const char *FifoFreeDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int FifoFreeDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 1+basedesc->getFieldCount(object) : 1;
}

unsigned int FifoFreeDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
    };
    return (field>=0 && field<1) ? fieldTypeFlags[field] : 0;
}

const char *FifoFreeDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "NumFreeCredits",
    };
    return (field>=0 && field<1) ? fieldNames[field] : NULL;
}

int FifoFreeDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='N' && strcmp(fieldName, "NumFreeCredits")==0) return base+0;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *FifoFreeDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
    };
    return (field>=0 && field<1) ? fieldTypeStrings[field] : NULL;
}

const char *FifoFreeDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int FifoFreeDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    FifoFree *pp = (FifoFree *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string FifoFreeDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    FifoFree *pp = (FifoFree *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getNumFreeCredits());
        default: return "";
    }
}

bool FifoFreeDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    FifoFree *pp = (FifoFree *)object; (void)pp;
    switch (field) {
        case 0: pp->setNumFreeCredits(string2long(value)); return true;
        default: return false;
    }
}

const char *FifoFreeDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
    };
    return (field>=0 && field<1) ? fieldStructNames[field] : NULL;
}

void *FifoFreeDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    FifoFree *pp = (FifoFree *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}


