//////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2004 Mellanox Technologies LTD. All rights reserved.
//
// This software is available to you under a choice of one of two
// licenses.  You may choose to be licensed under the terms of the GNU
// General Public License (GPL) Version 2, available from the file
// COPYING in the main directory of this source tree, or the
// OpenIB.org BSD license below:
//
//     Redistribution and use in source and binary forms, with or
//     without modification, are permitted provided that the following
//     conditions are met:
//
//      - Redistributions of source code must retain the above
//        copyright notice, this list of conditions and the following
//        disclaimer.
//
//      - Redistributions in binary form must reproduce the above
//        copyright notice, this list of conditions and the following
//        disclaimer in the documentation and/or other materials
//        provided with the distribution.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////////
//
//
// The IBVLArb implements an IB VL Arbiter
// See functional description in the header file.
//
#include "ib_m.h"
#include "vlarb.h"
#include "obuf.h"
#include "ibuf.h"
#include <iomanip>
using namespace std;

// TODO: Actually fillin the portXmitWaitHost

Define_Module( IBVLArb );

void IBVLArb::setVLArbParams(const char *cfgStr, ArbTableEntry *tbl)
{
   int idx = 0;
   char *buf, *p_vl, *p_weight;
   buf = new char[strlen(cfgStr)+1];
   strcpy(buf, cfgStr);
   p_vl = strtok(buf, ":");
   while (p_vl && (idx < 8))
   {
		int vl = atoi(p_vl);
		if (vl > 8)
		{
			ev << "-E- " << getFullPath() << " VL:" << vl << " > 8 in VLA:" << cfgStr << endl;
			ev.flush();
			exit(1);
		}

		int weight;
		p_weight = strtok(NULL, ", ");
		if (! p_weight)
		{
			ev << "-E- " << getFullPath() << " badly formatted VLA:" << cfgStr << endl;
			ev.flush();
			exit(1);
		}
		weight = atoi(p_weight);
		if (weight > 255)
		{
			ev << "-E- " << getFullPath() << " weight:" << weight << " > 255 in VLA:" << cfgStr << endl;
			ev.flush();
			exit(1);
		}

		tbl[idx].VL = vl;
		tbl[idx].weight = weight;
		tbl[idx].used = 0;
		idx++;
		p_vl = strtok(NULL, ":");
	}

   // the rest are zeros
   for (;idx < 8; idx++ )
   {
		tbl[idx].VL = 0;
		tbl[idx].weight = 0;
		tbl[idx].used = 0;
   }
   delete [] buf;
}

void IBVLArb::initialize()
{
	double coreFreq_mh = par("coreFreq_MH");
	// read parameters
	hcaArb = par("isHcaArbiter");
	width = par("width");
	recordVectors = par("recordVectors");
	maxVL = par("maxVL");
    if (!hcaArb) {
		ev << "-I- " << getFullPath() << " is Switch IBuf " << getId() <<  endl;
		cModule*    sw = getParentModule()->getParentModule();
        VSWDelay = sw->par("VSWDelay");
    }
	setVLArbParams(par("highVLArbEntries"), HighTbl);
	setVLArbParams(par("lowVLArbEntries"), LowTbl);
	vlHighLimit = par("vlHighLimit");
	// for 4x we can send 8 bytes per cycle
	popDelayPerByte_us =  1 / (2.0 * width) / coreFreq_mh;
	WATCH(popDelayPerByte_us);

	// Initiazlize the statistical collection elements
	portXmitWaitHist.setName("Packet Waits for Credits");
	portXmitWaitHist.setRangeAutoUpper(0, 10, 1);
	vl0Credits.setName("free credits on VL0");
	vl1Credits.setName("free credits on VL1");
	readyData.setName("Binary coded VL's with data");
	arbDecision.setName("arbitrated VL");

	// Initialize the ready to be sent credit pointers
	numInPorts = gateSize("in");
	unsigned numSentPorts = gateSize("sent");
	ASSERT(numInPorts == numSentPorts);

	// we need a two dimentional array of data packets
	inPktHoqPerVL = new IBDataMsg**[numInPorts];
	for (unsigned pn = 0; pn < numInPorts; pn++)
		inPktHoqPerVL[pn] = new IBDataMsg*[maxVL+1];

	for (unsigned pn = 0; pn < numInPorts; pn++ )
		for ( int vl = 0; vl < maxVL+1; vl++ )
		{
			inPktHoqPerVL[pn][vl] = NULL;
			WATCH(inPktHoqPerVL[pn][vl]);
		}

	// we also need a two dim array for tracking our promise
	// to bufs such to avoid a race betwen two requets
	hoqFreeProvided = new short*[numInPorts];
	for ( unsigned pn = 0; pn < numInPorts; pn++)
		hoqFreeProvided[pn] = new short[maxVL+1];
	for ( unsigned pn = 0; pn < numInPorts; pn++ )
		for ( int vl = 0; vl < maxVL+1; vl++ )
			hoqFreeProvided[pn][vl] = 0;

	// Init FCCL and FCTBS and the last sent port...
	for ( int vl = 0; vl < maxVL+1; vl++ )
	{
		LastSentPort.push_back(0);
		FCTBS.push_back(0);
		FCCL.push_back(0);
	}

	WATCH_VECTOR(LastSentPort);
	WATCH_VECTOR(FCTBS);
	WATCH_VECTOR(FCCL);

	LastSentVL = 0;
	LastSentWasHigh = 0;
	InsidePacket = 0;
	LowIndex = 0;
	HighIndex = 0;
	SentHighCounter = vlHighLimit*4096/64;

	// arbitration port shuffling
	arbitrationPortShuffle = new int[numInPorts]; // Contains the current permutation of ports.
	nextArbitrationPortIndex = 0; // Index into arbitrationPortShuffle. (value == in port number)
	shuffleArbitrationPorts();
	alwaysShuffle = par("arbAlwaysShuffle");

    // CC
    cc = par("cc_enabled");

    if(cc)
    {
        cModule *calleeModule = getParentModule()->getSubmodule("ccmgr");
        ccmgr = check_and_cast<CCMgr *> (calleeModule);
    }

	// The pop message is set every time we send a packet
	// when it is null we are ready for arbitration
	p_popMsg = NULL;

	// LRU
	lru_cnt = 0;
}

bool IBVLArb::ccEnabled()
{
	return cc;
}

// Using Knuth's shuffle to shuffle the array containing the order of in ports
// to check during arbitration. Each walk-through of ports related to arbitration
// uses a (most likely) new random ordering of the ports.
void IBVLArb::shuffleArbitrationPorts()
{
	int swap = 0;
	int t = 0;

	// reset the array values
	for(int i=0;i<numInPorts;i++)
		arbitrationPortShuffle[i] = i;

    // Shuffle the array (Knuth's shuffle)
	for(int i=numInPorts;i>1;i--){
		t = intrand(i); //random between 0 (inclusive) and i (exclusive).
		swap = arbitrationPortShuffle[t];
		arbitrationPortShuffle[t] = arbitrationPortShuffle[i-1];
		arbitrationPortShuffle[i-1] = swap;
	}
}

// returns next in port to check during arbitration (based on a fair,
// but "random" scheme)
int IBVLArb::getNextArbitrationPortNumToCheck()
{
	// Init new permutation of in ports if needed
	if(nextArbitrationPortIndex >= numInPorts)
	{
		nextArbitrationPortIndex = 0;
		shuffleArbitrationPorts();
	}

	return arbitrationPortShuffle[nextArbitrationPortIndex++];
}

// return the FCTBS of the OBUF driven by the VLA
// The hardware does not use this model
int IBVLArb::getOBufFCTBS(int vl)
{
   cGate *p_gate = gate("out")->getPathEndGate();
   IBOutBuf *p_oBuf = dynamic_cast<IBOutBuf *>(p_gate->getOwnerModule());
   if ((p_oBuf == NULL) || strcmp(p_oBuf->getName(), "obuf"))
   {
		ev << "-E- " << getFullPath() << " fail to get OBUF from out port" << endl;
		ev.flush();
		exit(3);
   }

   return(p_oBuf->getFCTBS(vl));
}

// return 1 if the HoQ for that port/VL is free
int IBVLArb::isHoQFree(unsigned pn, int vl)
{
	if ( pn >= numInPorts )
	{
		ev << "-E- " << getFullPath() << " got out of range port num:" << pn << endl;
		ev.flush();
		exit(1);
	}
	if ( (vl < 0) || (vl >= maxVL+1))
	{
		opp_error(" got out of range vl: %d" , vl);
	}

	// since there might be races between simultanous requests
	// and when they actually send we keep a parallel array
	// for tracking "free" HoQ returned and clear them on
	// message push
	if ((inPktHoqPerVL[pn][vl] == NULL) && (hoqFreeProvided[pn][vl] == 0))
	{
		hoqFreeProvided[pn][vl] = 1;
		return(1);
	}
	return(0);
}

short
IBVLArb::getFreeSpots(int vl) {

    if ( (vl < 0) || (vl >= maxVL+1))
    {
        opp_error(" got out of range vl: %d" , vl);
    }

    int free = 0;
    for(int pn=0; pn < numInPorts; pn++) {
        if ((inPktHoqPerVL[pn][vl] == NULL) && (hoqFreeProvided[pn][vl] == 0))
        {
            free++;
        }
    }
    return free;
}

// Sending the message out:
// Update FCTBS
void IBVLArb::sendOutMessage(IBDataMsg *p_msg)
{
	simtime_t delay;
    simtime_t swdelay;

    delay = (p_msg->getBitLength() / 8.0 * popDelayPerByte_us) * 1e-6;
    if(!hcaArb)
        swdelay = VSWDelay*1e-9;

	// we can only send if there is no such message as we use
	// it to flag the port is clear to send.
	if ( ! p_popMsg )
	{
		p_popMsg = new cMessage("pop", IB_POP_MSG);
		scheduleAt(simTime() + delay, p_popMsg);
	}
	else
	{
		ev << "-E- " << getFullPath() << " How can we have two messages leaving at the same time" << endl;
		return;
	}

	cGate *p_gate = gate("out")->getPathEndGate();
	IBOutBuf *p_oBuf = dynamic_cast<IBOutBuf *>(p_gate->getOwnerModule());
	if ((p_oBuf == NULL) || strcmp(p_oBuf->getName(), "obuf"))
	{
		ev << "-E- " << getFullPath() << " fail to get OBUF from out port" << endl;
		ev.flush();
		exit(3);
	}

    p_oBuf->incPacketsInFlight();

	// remember if last send was last of packet:
	LastSentWasLast = (p_msg->getCreditSn() + 1 == p_msg->getPacketLength());

    if (!hcaArb) {
        sendDelayed(p_msg, swdelay, "out");
    }
    else
    {
        send(p_msg, "out");
    }

	FCTBS[p_msg->getVL()]++;
}

// Notify the IBUF that the credit was sent out
void IBVLArb::sendSentMessage(int portNum, int vl)
{
	if (!ev.isDisabled())
		ev << "-I- " << getFullPath() << " informing ibuf with 'sent' message through:" << portNum
			<< " vl:" << vl << " last:" << LastSentWasLast << endl;
	IBSentMsg *p_sentMsg = new IBSentMsg("sent", IB_SENT_MSG);
	p_sentMsg->setVL(vl);
	p_sentMsg->setWasLast(LastSentWasLast);
	send(p_sentMsg, "sent", portNum );
}

// an arbitration is valid on two conditions:
// 1. The output port OBUF has free entries
// 2. The IBUF is not already to busy with other ports
int IBVLArb::isValidArbitration(int portNum, int vl, int isFirstPacket, int numPacketCredits)
{
	cGate *p_gate = gate("out")->getPathEndGate();
	IBOutBuf *p_oBuf = dynamic_cast<IBOutBuf *>(p_gate->getOwnerModule());
	if ((p_oBuf == NULL) || strcmp(p_oBuf->getName(), "obuf"))
	{
		ev << "-E- " << getFullPath() << " fail to get OBUF from out port" << endl;
		ev.flush();
		exit(3);
	}

	// check the entire packet an fit in
	int obufFree = p_oBuf->getNumFreeCredits();
	if (isFirstPacket && (obufFree < numPacketCredits))
	{
		if (!ev.isDisabled())
			ev << "-I- " << getFullPath()
				<< " not enough free OBUF credits:" << obufFree << " requierd:"
				<< numPacketCredits <<" invalid arbitration." << endl;
		return 0;
	}

	// only for non HCA Arbiters and in case of new packet being sent
	if (!hcaArb && isFirstPacket)
	{
		cGate *p_remOutPort = gate("in", portNum)->getPathStartGate();
		IBInBuf *p_inBuf = dynamic_cast<IBInBuf *>(p_remOutPort->getOwnerModule());
		if ((p_inBuf == NULL) || strcmp(p_inBuf->getName(), "ibuf") )
		{
			ev << "-E- " << getFullPath() << " fail to get InBuf from in port:" << portNum << endl;
			ev.flush();
			exit(3);
		}

		if (!p_inBuf->incrBusyUsedPorts())
		{
			if (!ev.isDisabled())
				ev << "-I- " << getFullPath()
					<< " no free ports on IBUF - invalid arbitration." << endl;
			return 0;
		}
	}
	return 1;
}

// NOTE: If vlHighLimit was reached a single packet
// of the lower table is transmitted.

// Find the port and VL to be sent next from a High or Low entries.
// Given:
// * current index in the table
// * The table of entries (VL,Weight) pairs
// Return:
// * 1 if found a port to send data from or 0 if nothing to send
// * Update the provided entry index
// * Update the port number
// * update the VL
int
IBVLArb::findNextSend( int &curIdx, ArbTableEntry *Tbl, int &curPortNum, int &curVl )
{
	int idx;
	short int vl;
	int found = 0;
	int numCredits = 0;
	int portNum;
	IBDataMsg *p_credit;

	// we need to scan through all entries starting with last one used
	// WL - fixed the MaxVL > 1 not working in simulator
	for (int i = 0; i <= maxVL+1; i++)
	//for (int i = 0; i <= maxVL; i++)
	{
		idx = (curIdx + i) % (maxVL+1);
		// if we changed index we need to restat the weight counter
		if (i) Tbl[idx].used = 0;

		// we should skip the entry if it has zero credits (weights) not used
		if (!Tbl[idx].weight || (Tbl[idx].used > Tbl[idx].weight)) continue;

		vl = Tbl[idx].VL;

		// how many credits are available for this VL
		numCredits = FCCL[vl] - FCTBS[vl];

		// UNCOMMENT this if statement for running KS (randomizing) instead of RR (1st of 3 needed changes)
		// Always start with port shuffling
	//	if(alwaysShuffle) {
	//		nextArbitrationPortIndex = 0;
	//		shuffleArbitrationPorts();
	//	}
		// start with the next port to the last one we sent (depends on the alwaysShuffle variable above)
		for (unsigned pn = 1; pn <= numInPorts; pn++)
		//for (unsigned pn = 1; pn <= (numInPorts *2); pn++) // Change to this for loop for running KS (randomizing) instead of RR (2nd of 3 needed changes)
		{
			portNum = (curPortNum + pn) % numInPorts;
			//portNum = getNextArbitrationPortNumToCheck(); // Change to this porntNum assignment for running KS (randomizing) instead of RR (3rd of 3 needed changes)
			p_credit = inPktHoqPerVL[portNum][vl];
			// do we have anything to send?
			if (p_credit == NULL)  continue;

			// just make sure it is a first credit

			// we can have another messages leaving at the same time so
			// ignore that port/vl if in the middle of another transfer
			if (p_credit->getCreditSn())
			{
				if (! ev.isDisabled())
					ev << "-I- " << getFullPath() << " ignoring non first packet:"
						<< p_credit->getName() << " on port:" << portNum
						<< " vl:" << vl << endl;
			}
			else
			{
                if(cc && hcaArb && !ccmgr->canSend(p_credit))
                {
                    ev << "-I- " << getFullPath() << " [CC] ccmgr packet held back"
                        << numCredits << " < " << p_credit->getPacketLength()
                        << " required for sending:"
                        << p_credit->getName() << " on port:" << portNum
                        << " vl:" << vl
                        << " dstLid:" << p_credit->getDstLid() << endl;
                }
				// so can we send it?
                else if (p_credit->getPacketLength() <= numCredits)
				{
					found = 1;
					break;
				}
				else
				{
					if (! ev.isDisabled())
						ev << "-I- " << getFullPath() << " not enough credits available:"
							<< numCredits << " < " << p_credit->getPacketLength()
							<< " required for sending:"
							<< p_credit->getName() << " on port:" << portNum
							<< " vl:" << vl << endl;
				}
			}
		}

		if (found)
		{
			curIdx = idx;
			curPortNum = portNum;
			curVl = vl;
			break;
		}
	}

	return(found);
}

// Find the port on VL0 to send from
// Given:
// Return:
// * 1 if found a port to send data from or 0 if nothing to send
// * Update the port number
int
IBVLArb::findNextSendOnVL0( int &curPortNum )
{
	int found = 0;
	int numCredits = 0;
	int portNum;
	IBDataMsg *p_credit;

	// how many credits are available for this VL
	  const char* st = getParentModule()->getParentModule()->getFullName();
	numCredits = FCCL[0] - FCTBS[0];

	// UNCOMMENT this if statement for running KS (randomizing) instead of RR (1st of 3 needed changes)
	// Always start with port shuffling?
	//if(alwaysShuffle) {
	//	nextArbitrationPortIndex = 0;
	//	shuffleArbitrationPorts();
	//}
	// start with the next port to the last one we sent (depends on the alwaysShuffle variable above)
	for (unsigned pn = 1; pn <= numInPorts; pn++)
	//for (unsigned pn = 1; pn <= (numInPorts*2); pn++) // Change to this for loop for running KS (randomizing) instead of RR (2nd of 3 needed changes)
	{
		portNum = (curPortNum + pn) % numInPorts;
		//portNum = getNextArbitrationPortNumToCheck(); // Change to this porntNum assignment for running KS (randomizing) instead of RR (3rd of 3 needed changes)
		p_credit = inPktHoqPerVL[portNum][0];
		// do we have anything to send?
		if (p_credit == NULL)  continue;

		// just make sure it is a first credit

		// we can have another messages leaving at the same time so
		// ignore that port/vl if in the middle of another transfer
		if (p_credit->getCreditSn())
		{
			if (! ev.isDisabled())
				ev << "-I- " << getFullPath() << " ignoring non first packet:"
					<< p_credit->getName() << " on port:" << portNum
					<< " vl:" << 0 << endl;
		}
		else
		{
            // do we have congestion?
            if(cc && hcaArb && !ccmgr->canSend(p_credit))
            {
                ev << "-I- " << getFullPath() << " [CC] ccmgr packet held back "
                    << numCredits << " < " << p_credit->getPacketLength()
                    << " required for sending:"
                    << p_credit->getName() << " on port:" << portNum
                    << " vl:" << 0
                    << " DstLID:"<< p_credit->getDstLid() << endl;
            }
			// so can we send it?
            else if (p_credit->getPacketLength() <= numCredits)
			{
				found = 1;
				break;
			}
			else
			{
				if (! ev.isDisabled())
					ev << "-I- " << getFullPath() << " not enough credits available:"
						<< numCredits << " < " << p_credit->getPacketLength()
						<< " required for sending:"
						<< p_credit->getName() << " on port:" << portNum
						<< " vl:" << 0 << endl;
			}
		}
	}

	if (found)
		curPortNum = portNum;
	return(found);
}

int IBVLArb::getFreeCredits(int vl)
{
    return FCCL[vl] - FCTBS[vl];
}

void IBVLArb::displayState()
{
	if (!ev.isDisabled()) {
		ev << "-I- " << getFullPath() << " ARBITER STATE as VL/Used/Weight"
			<< endl;
		ev << "-I- High:";
		for (int e = 0; e < maxVL+1; e++)
		{
			if (LastSentWasHigh && HighIndex == e)
				ev << "*" << HighTbl[e].VL << " "
					<< setw(3) << HighTbl[e].used
					<< "/" << setw(3) << HighTbl[e].weight << "*";
			else
				ev << "|" << HighTbl[e].VL << " "
					<< setw(3) << HighTbl[e].used
					<< "/" << setw(3) << HighTbl[e].weight << " ";
		}
		if (LastSentWasHigh)
			ev << "<----" << SentHighCounter << endl;
		else
			ev << endl;

		ev << "-I- Low: ";
		for (int e = 0; e < maxVL+1; e++)
		{
			if (!LastSentWasHigh && LowIndex == e)
				ev << "*" << LowTbl[e].VL << " "
					<< setw(3) << LowTbl[e].used
					<< "/" << setw(3) << LowTbl[e].weight << "*";
			else
				ev << "|" << LowTbl[e].VL << " "
					<< setw(3) << LowTbl[e].used
					<< "/" << setw(3) << LowTbl[e].weight << " ";
		}
		ev << endl;
	}

   int vlsWithData = 0;
   for (int vl = 0; vl < maxVL+1; vl++)
   {
		int fctbs = FCTBS[vl];
		int freeCredits = FCCL[vl] - fctbs;
		if (!ev.isDisabled())
			ev << "-I- " << getFullPath() << " vl:" << vl
				<< " " << FCCL[vl] << "-" << fctbs << "="
				<< freeCredits << " Ports " ;
		int anyInput = 0;
		for (unsigned pn = 0; pn < numInPorts ; pn++)
		{
			if (inPktHoqPerVL[pn][vl]) {
				anyInput = 1;
				if (!ev.isDisabled()) ev << pn << ":Y ";
			} else {
				if (!ev.isDisabled()) ev << pn << ":n ";
			}
		}

		if (!ev.isDisabled()) ev << endl;
		if (anyInput)
			vlsWithData |= 1<<vl;

		if (vl == 0)
			vl0Credits.record(freeCredits);
		else if (vl == 1)
			vl1Credits.record(freeCredits);
   }
	if ( recordVectors ) {
		readyData.record(10*vlsWithData);
	}
}

// Arbitration:
//
// Decides which data to send and provide back a "sent" notification.
//
// Data Structure:
// HighIndex, LowIndex - points to the index in the VLArb tables.
// LastSentPort[VL] - points to the last port that have sent data on a VL
// SentHighCounter - counts how many credits still needs to be sent from the high
// LastSentWasHigh - let us know if we were previously sending from
//    low or high table
// inPktHoqPerVL[pn][vl] - an array per port and VL pointing to first
//    credit of the packet
//
// NOTE: As we decide to arbitrate on complete packets only we might need to
//       increase the credit count if what is left is smaller then the number
//       of credits the packet carries.
//
// Algorithm:
// NOTE: as we only need to decide on a packet boundary we keep track of the
// last port and vl used and simply use all credits from it.
// * If the SentHighCounter is 0 then we use LowTable otherwise HighTable
// * Find the first port, after the last one that was sent, that has data
//   ready to send on the current index vl.
// * If not found incr index and repeat until all entries
//   searched (ignore 0 weight entries).
// * If found a new index use it and update the credits sent vs the weight.
// * If not found and we are HighTable - use LowTable and do as above.
// * Dec the SentHighCounter if we are HighTable. Load it to vlHighLimit*4k/64
//   otherwise.
//
void IBVLArb::arbitrate()
{
	int found = 0;
	IBDataMsg *nextSendHoq;
	int isFirstPacket = 0;
	int isLastCredit;
	int portNum;
	int vl;

	if (!ev.isDisabled())
		recordVectors = par("recordVectors");

	// can not arbitrate if we are in a middle of send
	if (p_popMsg)
	{
		if (!ev.isDisabled())
			ev << "-I- " << getFullPath()
				<< " can not arbitrate while packet is being sent" << endl;
		return;
	}

	// display arbiter state:
	if (!ev.isDisabled() || recordVectors)
		displayState();

    if(!InsidePacket && cc && hcaArb && !ccmgr->CNPqEmpty())
    {
        isFirstPacket = 1;
        isLastCredit = 1;

		ev << "-I- " << getFullPath() << " [CC] found CNP to pop " << endl;
        cGate *p_gate = gate("out")->getPathEndGate();
        IBOutBuf *p_oBuf = dynamic_cast<IBOutBuf *>(p_gate->getOwnerModule());
        if ((p_oBuf == NULL) || strcmp(p_oBuf->getName(), "obuf"))
        {
            ev << "-E- " << getFullPath() << " fail to get OBUF from out port" << endl;
            ev.flush();
            exit(3);
        }

        IBDataMsg *d_msg = (IBDataMsg *) ccmgr->CNPqPop();

	    if( getFreeCredits(d_msg->getVL()) <= d_msg->getPacketLength() )
        {
            ev << "-I- " << getFullPath() << " [CC] vlarb no credits for CNP " << endl;
        }
        else if(isValidArbitration(0, d_msg->getVL(), 1, d_msg->getPacketLength()))
        {
            ev << "-I- " << getFullPath() << " [CC] vlarb really sending CNP "
                << getFreeCredits(d_msg->getVL()) << endl;
            sendOutMessage(d_msg);
            //sendSentMessage(0, d_msg->getVL());
            return;
        }

        ev << "-I- " << getFullPath() << " [CC] vlarb no room for CNP  " << endl;
        delete d_msg;
    }

	// if we did not reach the end of the current sent packet simply send this credit
	if (InsidePacket)
	{
		vl = LastSentVL;
		portNum = LastSentPort[vl];

		nextSendHoq = inPktHoqPerVL[portNum][vl];
		if (!nextSendHoq)
		{
			if (!ev.isDisabled())
				ev << "-I- " << getFullPath() << " HoQ empty for port:"
					<< portNum << " VL:" << vl << endl;
			return;
		}

		isLastCredit = (nextSendHoq->getCreditSn() + 1 == nextSendHoq->getPacketLength());

		if (isLastCredit)
		{
			if (!ev.isDisabled())
				ev << "-I- " << getFullPath() << " sending last credit packet:"
					<< nextSendHoq->getName() << " from port:" << portNum
					<< " vl:" <<  vl << endl;
			//	  LastSentWasLast = 1;
			InsidePacket = 0;
		}
		else
		{
			if (!ev.isDisabled())
				ev << "-I- " << getFullPath() << " sending continuation credit packet:"
					<< nextSendHoq->getName() << " from port:" << portNum
					<< " vl:" << vl << endl;
		}

		// need to decrement the SentHighCounter if we are sending high packets
		if (LastSentWasHigh)
		{
			SentHighCounter--;
		}
		else
		{
			// If we are sending the last credit of packet when the SentHighCounter
			// is zero we need to reload it as this was the last credit of forced low
			// packet
			if ( isLastCredit && (SentHighCounter == 0))
				SentHighCounter = vlHighLimit*4096/64;
		}
		found = 1;
	}
	else
	{
		isFirstPacket = 1;
		const char * n1 = getParentModule()->getParentModule()->getName();
		// IF WE ARE HERE WE NEED TO FIND FIRST PACKET TO SEND
		portNum = LastSentPort[LastSentVL];
		vl = LastSentVL;

		if (maxVL > 0) {
			// if we are in High Limit case try first from low
			if ( SentHighCounter <= 0 ) {
				found = 1;
				if (findNextSend(LowIndex, LowTbl, portNum, vl))
					LastSentWasHigh = 0;
				else if ( findNextSend(HighIndex, HighTbl, portNum, vl) )
					LastSentWasHigh = 1;
				else
					found = 0;
			} else {
				found = 1;
				if ( findNextSend(HighIndex, HighTbl, portNum, vl) )
					LastSentWasHigh = 1;
				else if (findNextSend(LowIndex, LowTbl, portNum, vl))
					LastSentWasHigh = 0;
				else
					found = 0;
			}
		} else {
			vl = 0;
			found = findNextSendOnVL0(portNum);
		}

        if (found)
        {
            if (!ev.isDisabled())
            {
                if (LastSentWasHigh)
                {
                    ev << "-I- " << getFullPath() << " Result High idx:" << HighIndex << " vl:" << vl
                        << " port:" << portNum << " used:" << HighTbl[HighIndex].used
                        << " weight:" << HighTbl[HighIndex].weight
                        << " high count:" << SentHighCounter << endl;
                }
                else
                {
                    ev << "-I- " << getFullPath() << " Result Low idx:" << LowIndex << " vl:" << vl
                        << " port:" << portNum << " used:" << LowTbl[LowIndex].used
                        << " weight:" << LowTbl[LowIndex].weight << endl;
                }
            }

            nextSendHoq = inPktHoqPerVL[portNum][vl];
            isLastCredit = (nextSendHoq->getCreditSn() + 1 == nextSendHoq->getPacketLength());

            if (!isLastCredit) {
                InsidePacket = 1;
                if (!ev.isDisabled())
                    ev << "-I- " << getFullPath() << " sending first credit packet:"
                        << nextSendHoq->getName() << " from port:" << portNum
                        << " vl:" << vl << endl;
            }
            else
            {
                InsidePacket = 0;
                if (!ev.isDisabled())
                    ev << "-I- " << getFullPath() << " sending single credit packet: "
                        << nextSendHoq->getName() << " " << nextSendHoq->getPacketId()
                        << " " << nextSendHoq->getSrcLid()
                        << " from port:" << portNum
                        << " vl:" << vl << endl;
            }

        }
        else
        {
            if (!ev.isDisabled())
                ev << "-I- " << getFullPath() << " nothing to send" <<endl;

            if ( recordVectors )
                arbDecision.record(-1);
            return;
        }
    } // first or not

	// we could arbitrate an invalid selection due to lack of output Q
	// or busy ports of the input port we want to arbitrate.
	if (isValidArbitration(portNum, vl, isFirstPacket,
								  nextSendHoq->getPacketLength()))
	{
		// do the actual send and record our successful arbitration
		if (nextSendHoq->getCreditSn() == 0)
		{
			LastSentVL = vl;
			LastSentPort[vl] = portNum;
		}

		inPktHoqPerVL[LastSentPort[LastSentVL]][LastSentVL] = NULL;

		if (LastSentWasHigh)
			HighTbl[HighIndex].used ++;
		else
			LowTbl[LowIndex].used ++;

		if ( recordVectors )
			arbDecision.record(10*(vl+1));

        if( cc && hcaArb && isFirstPacket ){
        	if(ccmgr->operateAtQPLevel())
                ccmgr->updatePacketTime(nextSendHoq->getDstLid(),simTime(),nextSendHoq->getPacketLengthBytes());
        	else
        		ccmgr->updatePacketTime(nextSendHoq->getSL(),simTime(),nextSendHoq->getPacketLengthBytes());
        }

		sendOutMessage(nextSendHoq);
		sendSentMessage(LastSentPort[LastSentVL],LastSentVL);
	}
	else
	{
		// if we are in the first data credit cleanup the InsidePacket flag
		if (nextSendHoq->getCreditSn() == 0)
			InsidePacket = 0;
		if ( recordVectors )
			arbDecision.record(-1);
	}
}

// Handle push message
// A new credit is being provided by some port
// We need to know which port we get the data on,
// which VL is it on
// if the HOQ is aleady taken assert
void IBVLArb::handlePush(IBDataMsg *p_msg)
{
	// what port did we get it from ?
	unsigned pn = p_msg->getArrivalGate()->getIndex();
	short int vl = p_msg->getVL();
	if ( pn >= numInPorts )
	{
		ev << "-E- " << getFullPath() << " got out of range port num:" << pn << endl;
		ev.flush();
		exit(1);
	}

	if ( (vl < 0) || (vl >= maxVL+1))
	{
		opp_error("VLA got out of range vl: %d by %s, arrived at port %d", vl, p_msg->getName(), pn);
	}


	if (inPktHoqPerVL[pn][vl] != NULL)
	{
		ev << "-E- " << getFullPath() << " Overwriting HoQ port:" << pn << " VL:" << vl
			<< " by msg:" << p_msg->getName() << " arrived at port:" << pn
			<< endl;
		ev.flush();
		exit(2);
	}

	if (hoqFreeProvided[pn][vl] == 0)
	{
		ev << "-E- " << getFullPath() << " No previous HoQ free port:" << pn << " VL:" << vl
			<< " by msg:" << p_msg->getName() << " arrived at port:" << pn
			<< endl;
		ev.flush();
		exit(2);
	}

	if (!ev.isDisabled())
		ev << "-I- " << getFullPath() << " filled HoQ for port:"
			<< pn << " vl:" << vl << " with:" << p_msg->getName() <<  endl;

	inPktHoqPerVL[pn][vl] = p_msg;
	hoqFreeProvided[pn][vl] = 0;
	arbitrate();
}

// Handle Pop Message
// clear the pop message and send the "sent message" then try to arbitrate
//
// NOTE: Only now when the packet was sent we can tell the IBUF
// to free its credits and busy ports.
void IBVLArb::handlePop()
{
	cancelAndDelete(p_popMsg);
	p_popMsg = NULL;
	arbitrate();
}

// Handle TxCred
void IBVLArb::handleTxCred(IBTxCredMsg *p_msg)
{
	int vl = p_msg->getVL();
	// update FCCL...
	FCCL[vl] = p_msg->getFCCL();

	if (!ev.isDisabled())
		ev << "-I- " << getFullPath() << " updated vl:" << vl
			<< " fccl:" << p_msg->getFCCL()
			<< " can send :" << FCCL[vl] - FCTBS[vl] << endl;

	delete p_msg;
	arbitrate();
}

void IBVLArb::handleMessage(cMessage *p_msg)
{
	int msgType = p_msg->getKind();
	if ( msgType == IB_POP_MSG )
	{
		handlePop();
	}
	else if ( msgType == IB_DATA_MSG )
	{
		handlePush((IBDataMsg*)p_msg);
	}
	else if ( msgType == IB_TXCRED_MSG )
	{
		handleTxCred((IBTxCredMsg*)p_msg);
	}
	else if ( (msgType == IB_DONE_MSG) || (msgType == IB_FREE_MSG) )
	{
		delete p_msg;
		arbitrate();
	}
	else
	{
		ev << "-E- " << getFullPath() << " does not know how to handle message:" << msgType << endl;
		delete p_msg;
	}
}

void IBVLArb::finish()
{
	/*ev << "STAT: " << getFullPath() << " Wait for credits num/avg/max/std "
     << portXmitWaitHist.getCount()
     << " / " << portXmitWaitHist.getMean()
     << " / " << portXmitWaitHist.getMax()
     << " / " << portXmitWaitHist.getStddev() << endl;
	*/
}
