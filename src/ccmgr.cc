//////////////////////////////////////////////////////////////////////////////
//
// OMNET InfiniBand Congestion Control Model
//
// Copyright (C) 2009 Simula Research Laboratory
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//////////////////////////////////////////////////////////////////////////////

#include <omnetpp.h>
#include <assert.h>
#include <math.h>

#include <sstream>

#include "ccmgr.h"
#include "vlarb.h"
#include "ib_m.h"
#include "obuf.h"

using std::string;
using std::stringstream;

Define_Module ( CCMgr );

void CCMgr::initialize()
{
    if(!par("cc_enabled"))
        return;

    CNPqSize = CNP_QUEUE_SIZE; // Size of the IBCNPMsg (BECN) queue. Should be able to
    CNPsThrownAwayAtHCA = 0;
    CNPsEnqueuedAtHCA = 0;

    cModule *calleeModule = getParentModule()->getSubmodule("vlarb");
    parent = check_and_cast<IBVLArb *> (calleeModule);

    creditSize = par("creditSize");
    speed = par("speed"); //XXX what if different ports have different speeds
    width = par("width"); //XXX what if different ports have different speeds
    maxVL = par("maxVL");
    qp_level = par("qp_level"); // true -> CC works on dst lid lvl, else vl lvl (default)
    singleThrPerOP = par("cc_single_thr_per_outport");  // true: single threshold compared to the fill ratio of all VoQs together
    singleThrDevide = par("cc_single_thr_devide"); // true: devide sum of VoQ fill rates before comparing to the threshold
    maxDstLID = 710; // enough for M9 :P XXX should be changed to read the value from the generator !!!+1!!!(or something :))
    numPorts = par("numPorts");
    VLcount = maxVL + 1;

    fecnCount = 0;
    becnCount = 0;

    hcaParent = par("hcaCC");

    congested = 0;

    // Interval logging
    logInterval = par("logInterval");
    lastIntLogFECN = 0;
    lastIntLogBECN = 0;
    lastIntLogTime = 0;
    // Start interval logging
    scheduleAt(simTime() + logInterval, new cMessage(NULL, IB_LOG_TIMER_MSG));

    if(qp_level)
    { // Running CC at the QP/SrcLid level
    	cctiPos = new cOutVector[maxDstLID];
    	ccti = new int[maxDstLID];
    	timerEvent = new timerMessage[maxDstLID];
    	packetTime = new ptime_t[maxDstLID];
    }
    else
    { // Running CC at the VL level (default)
    	cctiPos = new cOutVector[SL_MAX];
    	ccti = new int[SL_MAX];
    	timerEvent = new timerMessage[SL_MAX];
    	packetTime = new ptime_t[SL_MAX];
    }

    if(hcaParent)
        initializeHCA(parent);
    else
        initializeSW(parent);
}

void CCMgr::initializeSW(cModule *parent_)
{
    parent = parent_;
    sw = getParentModule();

    assert(parent);
    assert(sw);
    assert(numPorts);

	interval_CC_FECN_Stats.setName("[CC] Interval FECN count (SW) "); // Record the interval logging
	interval_CC_BECN_Stats.setName("[CC] Interval BECN count (SW) "); // Record the interval logging

    Threshold       = par("Threshold");
    Marking_Rate    = par("Marking_Rate");
    Packet_Size     = par("Packet_Size");
    Victim_Mask     = par("Victim_Mask");
    Reset_Threshold = par("Reset_Threshold");
    cc_extended_ibuf_log = par("cc_extended_ibuf_log");

    if(Reset_Threshold == -1)
        Reset_Threshold = Threshold;

    inputPortsFillStatus = new float*[VLcount];
    congestedInputPorts = new bool*[VLcount];
    congested = new unsigned[VLcount];
    congStatus = new cOutVector[VLcount];
    obufSrcLidOfFECNmarkedPacket = new cOutVector[VLcount]; // SrcLIDs of FECNmarked packets
    obufStatusCCOn = new cOutVector[VLcount]; // obuf fill ratio when CC is turned on (per vl)
    obufStatusCCOff = new cOutVector[VLcount]; // obuf fill ratio when CC is turned off (per vl)
    obufQHcounterCCOn = new cOutVector[VLcount]; // obuf header logger when CC is turned on (per vl)
    obufQHcounterCCOff = new cOutVector[VLcount]; // obuf header logger when CC is turned off (per vl)
    bufStatusRecord = new cOutVector*[VLcount];

    for(int vl=0;vl<VLcount;vl++)
    {
        string name;

        inputPortsFillStatus[vl] = new float[numPorts];
    	congestedInputPorts[vl] = new bool[numPorts];
        bufStatusRecord[vl] = new cOutVector[numPorts];

        for(unsigned port=0;port<numPorts;port++)
        {
            congestedInputPorts[vl][port] = false;
            inputPortsFillStatus[vl][port] = 0.0;

            name = "Buffer port state for VL ";
            name += convertIntToString(vl);
            name += ", input port ";
            name += convertIntToString(port);
            name += ", headed for output port ";
            name += convertIntToString(getParentModule()->getIndex());
            bufStatusRecord[vl][port].setName(name.c_str());
        }

        name = "Congestion port state for VL ";
        name += convertIntToString(vl);
        congested[vl] = 0;
        congStatus[vl].setName(name.c_str());

        name = "Src LID of FECN marked packets for VL ";
        name += convertIntToString(vl);
        obufSrcLidOfFECNmarkedPacket[vl].setName(name.c_str());

        name = "Fill ratio of obuf when moving into CongState for VL ";
        name += convertIntToString(vl);
        obufStatusCCOn[vl].setName(name.c_str());
        name = "Fill ratio of obuf when moving out of CongState for VL ";
        name += convertIntToString(vl);
        obufStatusCCOff[vl].setName(name.c_str());
        name = "Headers in obuf when moving into CongState for VL ";
        name += convertIntToString(vl);
        obufQHcounterCCOn[vl].setName(name.c_str());
        name = "Headers in obuf when moving out of CongState for VL ";
        name += convertIntToString(vl);
        obufQHcounterCCOff[vl].setName(name.c_str());

    }

    ev << "-I- " << getFullPath() << " [CC] SW init completed" << endl;
}

void CCMgr::initializeHCA(cModule *parent_)
{
    parent = parent_;
    hca = getParentModule();

    assert(parent);
    assert(hca);

	interval_CC_FECN_Stats.setName("[CC] Interval FECN count (HCA) "); // Record the interval logging
	interval_CC_BECN_Stats.setName("[CC] Interval BECN count (HCA) "); // Record the interval logging

    CNPSn = 0; // CNP serial number
    CNPvl = 0;

    CCTI_Increase    = par("CCTI_Increase");
    CCTI_Limit       = par("CCTI_Limit");
    CCTI_Timer       = par("CCTI_Timer");
    CCTI_Min         = par("CCTI_Min");
    CCT_Algorithm    = par("CCT_Algorithm");
    CCT_Div          = par("CCT_Div");
    AddIrdFlightTime = par("CC_AddIrdFlightTime");
    num_nodes        = par("num_nodes"); /* XXX parameter should be somewhere else,
        or we should just figure it out */

    if(qp_level)
    {
        for(int i=0;i<maxDstLID;i++)
        {
            std::ostringstream s;
            s << "CCTI position for SrcLid(QP) " << i;
            cctiPos[i].setName(s.str().c_str());
        }
    }
    else
    {
        for(int i=0;i<SL_MAX;i++)
        {
            std::ostringstream s;
            s << "CCTI position for SL " << i;
            cctiPos[i].setName(s.str().c_str());
        }
    }

    populateCCT();

    cctiInit();
    packetTimeInit();

    CNPQueue = new cQueue[VLcount];

    ev << "-I- " << getFullPath() << " [CC] HCA init completed" << endl;
}

void CCMgr::populateCCT()
{
    int i;

    if(num_nodes <= 0)
    {
        ev << "-I- " << getFullPath() << " [CC] cc enabled but num_nodes is "
            << num_nodes << "(so you might need to set the parameter)"
            << endl;
        ev.flush();
        exit(EXIT_FAILURE);
    }

    switch(CCT_Algorithm)
    {
        case 1:
            for(i=0;i<CCT_MAX;i++)
            {
                cct[i] = 0x3FFF;
            }
            break;
        case 2:
            unsigned a;
            double afact;
            double k;
            double val;

            a = 2; //XXX parameterize ? FIXME
            k = num_nodes/pow(CCT_Div,2);

            afact = 1 - (a + k)/(pow(CCT_MAX-1,2) * k);

            for(i=0;i<CCT_MAX;i++)
            {
                unsigned divider;
                unsigned mantice;

                const unsigned maxMantice = 0x3FFF;

                divider = 3;

                val = a + pow(i,2) * k * afact;

                if(val > maxMantice)
                    opp_error("CCT value %g larger than max %u",val,maxMantice);

                while(val > (maxMantice >> divider))
                    divider--;

                mantice = val * pow(2, divider);

                cct[i] = (divider << 14) | mantice;
            }
            break;
        default:
            for(i=0;i<CCT_MAX;i++)
            {
                double k;
                double val;

                unsigned divider;
                unsigned mantice;

                const unsigned maxMantice = 0x3FFF;

                divider = 3;
                k = num_nodes/pow(CCT_Div,2);
                val = pow(i,2) * k;

                if(val > maxMantice)
                    opp_error("CCT value %u larger than max %u",val,maxMantice);

                while(val > (maxMantice >> divider))
                    divider--;

                mantice = val * pow(2, divider);

                cct[i] = (divider << 14) | mantice;
            }
    }
}

void CCMgr::cctiInit()
{
    int i;
    int limit;

    if(qp_level)
    	limit = maxDstLID;
    else
    	limit = SL_MAX;

    for(i=0;i<limit;i++)
    {
    	timerEvent[i].setCCID(i);
    	timerEvent[i].setKind(IB_CC_TIMER_MSG);
    	ccti[i] = CCTI_Min;
    	cctiPos[i].record(ccti[i]);
	}
}

void CCMgr::packetTimeInit()
{
    int i;
    int limit;

    if(qp_level)
    	limit = maxDstLID;
    else
    	limit = SL_MAX;

    for(i=0;i<limit;i++)
    {
        packetTime[i].time = 0;
        packetTime[i].plen = 0;
    }
}

void CCMgr::cctiTimerReset(timerMessage *msg)
{
    Enter_Method_Silent();

    assert(msg);
    if(qp_level)
    	assert(msg->getCCID() >= 0 && msg->getCCID() < maxDstLID);
    else
    	assert(msg->getCCID() >= 0 && msg->getCCID() < SL_MAX);
    assert(msg->isSelfMessage());

    if(msg->isScheduled())
    {
        ev << "-I- " << getFullPath() << " [CC] replacing scheduled message" << endl;
        cancelEvent(msg);
    }

    scheduleAt(simTime() + CCTI_Timer, msg);

    ev << "-I- " << getFullPath() << " [CC] resetting timer" << endl;
}

void CCMgr::cctiTimer(timerMessage *msg)
{
    assert(msg);
    if(qp_level)
    	assert(msg->getCCID() >= 0 && msg->getCCID() < maxDstLID);
    else
    	assert(msg->getCCID() >= 0 && msg->getCCID() < SL_MAX);

    if(msg->isScheduled())
    {
        ev << "-I- " << getFullPath() << " [CC] replacing scheduled message" << endl;
        cancelEvent(msg);
    }

    ccti[msg->getCCID()] = std::max(CCTI_Min,ccti[msg->getCCID()]-1);

    if(ccti[msg->getCCID()] == CCTI_Min)
    {
        ev << "-I- " << getFullPath() << " [CC] CCTI reached CCTI_Min (removing timer)" << endl;
        return;
    }

    cctiPos[msg->getCCID()].record(ccti[msg->getCCID()]);
    ev << "-I- " << getFullPath() << " [CC] updating timer, CCTI now at " << ccti[msg->getCCID()] << endl;
    scheduleAt(simTime() + CCTI_Timer, msg);
}

void CCMgr::handleIntervalLogging(cMessage *msg)
{
	simtime_t now = simTime();

	if(lastIntLogTime >= now)
		return; // Nothing new to log/something is rotten

	// The FECN/BECN count for the last interval
	int dFECN = fecnCount - lastIntLogFECN;
	int dBECN = becnCount - lastIntLogBECN;

	// update bookkeeping variables
	lastIntLogFECN = fecnCount;
	lastIntLogBECN = becnCount;
    lastIntLogTime = now;  // lastIntLogTime not of much use right now...

    // Do the actual recording of the last interval
	interval_CC_FECN_Stats.record((double) dFECN);
	interval_CC_BECN_Stats.record((double) dBECN);

	ev << "STAT - INTERVAL (" << logInterval << ") : " << getFullPath() << " #FECN last interval :  " << dFECN << endl;
	ev << "STAT - INTERVAL (" << logInterval << ") : " << getFullPath() << " #BECN last interval :  " << dBECN << endl;

    // Rescheduling interval logging
    scheduleAt(now + logInterval, msg);
}


void CCMgr::handleMessage(cMessage *msg)
{
    int msgType = msg->getKind();

    if(msgType == IB_CC_TIMER_MSG)
    {
        cctiTimer((timerMessage *) msg);
    }
    else if (msgType == IB_LOG_TIMER_MSG)
    {
    	handleIntervalLogging(msg);
    }

    ev << "-I- " << getFullPath() << " [CC] handleMessage " << endl;
}

// Checking (and acting upon) FECN reception at an ibuf (at HCA):
void CCMgr::checkFECN(cMessage *p_msg)
{
    ev << "-I- " << getFullPath() << " [CC] checking for FECN" << endl;

    IBDataMsg *i_msg = (IBDataMsg *) p_msg;

    assert(i_msg);

    if(i_msg->getFECN())
    {
        ev << "-I- " << getFullPath() << " [CC] FECN bit is set" << endl;

        fecnCount++;

        // We want to create a message (carrying a BECN) to send
    	// back to the contributor to congestion.
        ev << "-I- " << getFullPath() << " [CC] generating CNP" << endl;
        IBDataMsg *cnp = new IBDataMsg("cnp",IB_DATA_MSG);

        cnp->setFECN(0);
        cnp->setBECN(1);
        cnp->setSrcLid(i_msg->getDstLid());
        cnp->setDstLid(i_msg->getSrcLid());
        cnp->setSL(i_msg->getSL());
        cnp->setVL(i_msg->getVL());
        cnp->setPacketId(CNPSn++);  // "private" PacketId
        cnp->setPacketLength(1);
        cnp->setCreditSn(0);
        cnp->setPacketLengthBytes(creditSize*cnp->getPacketLength());
        cnp->setByteLength(creditSize*cnp->getPacketLength());
        /* XXX should packetId be included?
            packetId is normally generated by gen
            and should be globally unique.
            the internal messages usually
            do not need a packetId
         */

        /* Put the cnp into the CNP queue */
        if(CNPqInsert(cnp)) {
            // Successfully enqueued cnp
            CNPsEnqueuedAtHCA++;
        } else {
            // Not able to enqueue cnp
            CNPsThrownAwayAtHCA++;
            delete cnp;
        }
    }
}

// Checking (and acting upon) BECN reception at an ibuf:
void CCMgr::checkBECN(cMessage *p_msg)
{
    ev << "-I- " << getFullPath() << " [CC] checking for BECN" << endl;

    IBDataMsg *i_msg = (IBDataMsg *) p_msg;

    assert(i_msg);

    if(i_msg->getBECN())
    {
        ev << "-I- " << getFullPath() << " [CC] BECN bit is set" << endl;

        becnCount++;

        ev << "-I- " << getFullPath() << " [CC] BECN received, updating CC" << endl;

        if(qp_level)
        {   // The source(!) of the BECN is the dst we send to...
        	assert(i_msg->getSrcLid() >= 0 && i_msg->getSrcLid() < maxDstLID);

        	ccti[i_msg->getSrcLid()] = std::min(std::min(CCT_MAX,CCTI_Limit),ccti[i_msg->getSrcLid()]+CCTI_Increase);
        	cctiTimerReset(&(timerEvent[i_msg->getSrcLid()]));

        	cctiPos[i_msg->getSrcLid()].record(ccti[i_msg->getSrcLid()]);
        	ev << "-I- " << getFullPath() << " [CC] CCTI for SrcLid(QP) " << i_msg->getSrcLid() << " now at " << ccti[i_msg->getSrcLid()] << endl;
        }
        else
        {
        	assert(i_msg->getSL() >= 0 && i_msg->getSL() < SL_MAX);

        	ccti[i_msg->getSL()] = std::min(std::min(CCT_MAX,CCTI_Limit),ccti[i_msg->getSL()]+CCTI_Increase);
        	cctiTimerReset(&(timerEvent[i_msg->getSL()]));

        	cctiPos[i_msg->getSL()].record(ccti[i_msg->getSL()]);
        	ev << "-I- " << getFullPath() << " [CC] CCTI for sl " << i_msg->getSL() << " now at " << ccti[i_msg->getSL()] << endl;
        }
    }
}

// Possibly mark PORT with congested state (only called for a switch)
void CCMgr::updateCONG(int vl, unsigned port, size_t quelen, size_t maxlen)
{
	// 'port': the port number of the input port updating CONG status on the output port this CCMgr relates to...
    // let's call it the source port
    assert(congestedInputPorts);
    assert(sw);

    if(cc_extended_ibuf_log)
		bufStatusRecord[vl][port].record((double) quelen/maxlen);

    if(!singleThrPerOP)
    { // One threshold per input port
    	if((double) quelen/maxlen > 1 - (double) Threshold/THRESHOLD_MAX)
    	{
    		if(!congestedInputPorts[vl][port])
    		{
    			congestedInputPorts[vl][port] = true; // the in port queue "port/vl" (headed for this out port) is congested.
    			congested[vl]++;  // tracks the total number of congested in ports (per vl) headed for _this_ out port in the switch.
    			congStatus[vl].record(congested[vl]);

    			// Record headers in out queue when CC state of out port changes (1 -> just turned on):
    			if(congested[vl] == 1) recordCongStateChange(vl, port, true);
    		}
    	}
    	else if((double) quelen/maxlen <= 1 - (double) Reset_Threshold/THRESHOLD_MAX)
    	{
    		if(congestedInputPorts[vl][port])
    		{
    			congestedInputPorts[vl][port] = false; // the in port queue "port/vl" (headed for this out port) is no longer congested.
    			congested[vl]--;  // tracks the total number of congested in ports (per vl) headed for _this_ out port in the switch.
    			congStatus[vl].record(congested[vl]);

    			// Record headers in out queue when CC state of out port changes (0 -> just turned off):
    			if(congested[vl] == 0) recordCongStateChange(vl, port, false);
    		}
    	}
	}
    else
    {  // One threshold for all the input ports:
    	inputPortsFillStatus[vl][port] = (float) quelen/maxlen;

    	int nrActive = 0; // Number of input ports (for the current VL) headed for this output port
    	float sumOfFill = 0.0;

    	for(unsigned prNr=0;prNr<numPorts;prNr++)
        	if(inputPortsFillStatus[vl][prNr] > 0.0) {
        		nrActive++;
        		sumOfFill += inputPortsFillStatus[vl][prNr];
        	}

        // Now we know the number of possible (current) contributors, as well as the updated of the current fill ratios

    	if(nrActive == 0)
    	{ // Not in congested state
			// Record headers in out queue when CC state of out port changes:
			if(congested[vl] > 0) recordCongStateChange(vl, port, false);
    		congested[vl] = 0;
    	}
    	else
    	{
        	//float calcFill = (sumOfFill)/(sqrt(nrActive));
        	float calcFill;

        	if(singleThrDevide)
        	{
        		calcFill = (sumOfFill)/(nrActive);
        	} else
        	{
        		calcFill = sumOfFill;
        	}

        	if(calcFill > 1 - (double) Threshold/THRESHOLD_MAX)
        	{
        		if(congested[vl] == 0) recordCongStateChange(vl, port, true);
        		// Move port into congested state
        		congested[vl] = 1;
        	}
        	else if(calcFill <= 1 - (double) Reset_Threshold/THRESHOLD_MAX)
        	{
    			if(congested[vl] > 0) recordCongStateChange(vl, port, false);
    			// Move port out of congested state
    			congested[vl] = 0;
        	}

    	}

    }
}

// Record information about out port status when congestion state changes (in switch)
void CCMgr::recordCongStateChange(int vl, unsigned inport, bool turnedOn) {
    cModule *outBufTemp = getParentModule()->getSubmodule("obuf");
    IBOutBuf *outBuf = check_and_cast<IBOutBuf *> (outBufTemp);
    IBWireMsg *flit;

    outBuf->initIterator();
    while(!outBuf->iterationDone())
    {
    	flit = (IBWireMsg *) outBuf->nextIterationElement();

		// if this flit is the header of a IBDataMsg we record the SrcLid(!) of it
    	if(flit->getKind() == IB_DATA_MSG && flit->getCreditSn() == 0)
    	{
    		if(turnedOn)
    			obufQHcounterCCOn[vl].record(flit->getSrcLid());
    		else
    			obufQHcounterCCOff[vl].record(flit->getSrcLid());
    	}
    }

	if(turnedOn)
	{
		// record fill ration of out buffer when entering congested state
	    obufStatusCCOn[vl].record(((double) outBuf->getQueueLength())/((double) outBuf->getQSize()));
	}
	else
	{
		// record fill ration of out buffer when leaving congested state
	    obufStatusCCOff[vl].record(((double) outBuf->getQueueLength())/((double) outBuf->getQSize()));
	}

} // END recordCongStateChange(...)


// Possible FECN marking at a switch:
void CCMgr::markFECN(cMessage *p_msg)
{
    IBVLArb *vlarb = (IBVLArb *) parent;
    IBDataMsg *i_msg = (IBDataMsg *) p_msg;

    assert(vlarb);
    assert(i_msg);

    ev << "-I- " << getFullPath() << " [CC] markFECN " <<
        vlarb->getFreeCredits(i_msg->getVL()) <<
        " congested: " << congested[i_msg->getVL()] <<
        endl;

    if(i_msg->getFECN())
    {
        ev << "-I- " << getFullPath() << " [CC] no FECN (Already tagged) " << endl;
        return;
    }

    /* mark packet if need*/
    if(i_msg->getPacketLength() < Packet_Size)
    {
        ev << "-I- " << getFullPath() << " [CC] no FECN (Packet_Size) " << endl;
        return;
    }

    if(!Victim_Mask && vlarb->getFreeCredits(i_msg->getVL()) - i_msg->getPacketLength() <= 0)
    {
        ev << "-I- " << getFullPath() << " [CC] no FECN (Victim SW (Mask not set)) " << endl;
        return;
    }

    if(congested[i_msg->getVL()])
    {
        if(intrand((Marking_Rate+1)) == 0)
        {
            ev << "-I- " << getFullPath() << " [CC] marking with FECN" << endl;

            i_msg->setFECN(1);

            obufSrcLidOfFECNmarkedPacket[i_msg->getVL()].record(i_msg->getSrcLid());

            fecnCount++;
        }
        else
        {
            ev << "-I- " << getFullPath() << " [CC] no FECN (Marking_Rate) " << endl;
        }
    }
    else
    {
        ev << "-I- " << getFullPath() << " [CC] no FECN (Threshold) " << endl;
    }
}

int CCMgr::canSend(IBDataMsg *msg)
{
    int ccid;

    assert(hcaParent);

    if(qp_level)
    {
        ccid = msg->getDstLid();
        assert(ccid >= 0 && ccid<maxDstLID);
    }
    else
    {
        ccid = msg->getSL();
        assert(ccid >= 0 && ccid<SL_MAX);
    }

    if(packetTime[ccid].time +
            getIRD(ccid) >
            simTime())
    {
        return 0;
    }

    return 1;
}

int CCMgr::canSend(int dstLid, int dstVl)
{
    int ccid;

    assert(hcaParent);

    if(qp_level)
    {
        ccid = dstLid;
        assert(ccid >= 0 && ccid<maxDstLID);
    }
    else
    {
        ccid = dstVl;
        assert(ccid >= 0 && ccid<SL_MAX);
    }

    if(packetTime[ccid].time +
            getIRD(ccid) >
            simTime())
    {
        return 0;
    }

    return 1;
}

simtime_t CCMgr::getIRD(int ccid)
{
    simtime_t TpacketTime;
    uint16_t Shift_field;
    uint16_t Mult_Factor;
    simtime_t Ts;

    assert(hcaParent);

    TpacketTime = (packetTime[ccid].plen*8.0)/(speed*width*10e9); //flight time
    Mult_Factor = cct[ccti[ccid]] & 0x3FFF;
    Shift_field = cct[ccti[ccid]] >> 14;

    Ts = (TpacketTime / pow(2,Shift_field)) * Mult_Factor;

    ev << "-E- " << getFullPath() << " [CC] ird send ok at " << packetTime[ccid].time + TpacketTime*AddIrdFlightTime + Ts << " now is " << simTime() << endl;

    return Ts;
}

void CCMgr::updatePacketTime(int ccid, simtime_t t, size_t len)
{
	if(qp_level)
		assert(ccid < maxDstLID);
	else
		assert(ccid < SL_MAX);
    assert(hcaParent);

    ev << "-E- " << getFullPath() << " [CC] updating stored packet time " << t << endl;

    packetTime[ccid].time = t;
    packetTime[ccid].plen = len;
}

bool CCMgr::operateAtQPLevel()
{
	return qp_level;
}

/* Returns 'true' if the traffic headed for the destination 'dstLid' (the parameter)
 *  is considered to be a contributor to congestion. The method requires the CC
 *  to operate in the ad hoc QP mode.  */
bool CCMgr::congDstLid(int dstLid)
{
	if(operateAtQPLevel()) {
		assert(dstLid >= 0 && dstLid<maxDstLID); // sanity check

		// Operating at the QP lvl - checking if the dstLid has a corresponding
		// CCT index above the min value...
		if(ccti[dstLid] > CCTI_Min)
			return true;
		else
			return false;
	} else
		throw cRuntimeError("Testing for congested destination using LID while running in CC VL mode - this is considered to be an error, (CCMfr::congDstLid)");

	return false;
} // END congDstLid(int dstLid)


/* (Private) Methods to handle the queue of IBCNPMsg messages waiting to be sent */
bool CCMgr::CNPqEmpty()
{
    int i;
    IBVLArb *vlarb = (IBVLArb *) parent;

    for(i=0;i<VLcount;i++)
        if(!CNPQueue[i].isEmpty() && vlarb->getFreeCredits(i) > 0)
            return false;

    return true;
}

bool CCMgr::CNPqFull()
{
    int i;
    size_t c;

    for(i=0,c=0;i<VLcount;i++)
        c += CNPQueue[i].getLength();

    return (c >= CNPqSize);
}

bool CCMgr::CNPqInsert(cMessage *msg)
{
    if(!msg)
    {
        ev << "-E- " << getFullPath() << " [CC] CNPqInsert with empty message, aborting" << endl;
        ev.flush();
        exit(1);
    }

    IBDataMsg *d_msg = (IBDataMsg *) msg;

    assert(d_msg->getBECN());

    if(!CNPqFull())
    {
        CNPQueue[d_msg->getVL()].insert(d_msg);
        return true;
    }
    else
        return false;
}

cMessage* CCMgr::CNPqPop()
{
    int i;
    int vl;
    IBVLArb *vlarb = (IBVLArb *) parent;

    for(i=0;i<VLcount;i++)
    {
        vl = (i+CNPvl) % VLcount;
        if(vlarb->getFreeCredits(vl) == 0)
            continue;

        if(!CNPQueue[vl].isEmpty())
            break;
    }

    if(i == VLcount)
        return NULL;

    CNPvl = (vl + 1) % VLcount;
    return (cMessage *) CNPQueue[vl].pop();
}

void CCMgr::finish()
{
    int i;
    int limit;

    if(!par("cc_enabled"))
        return;

    if(qp_level)
    	limit = maxDstLID;
    else
    	limit = SL_MAX;

    for(i=0;i<limit;i++)
    {
        if(timerEvent[i].isScheduled())
            cancelEvent(&timerEvent[i]);
    }

    ev << "-I- " << getFullPath() << " [CC] CNP thrown: " <<
        CNPsThrownAwayAtHCA  << " CNP sent " << CNPsEnqueuedAtHCA << endl;
    ev << "-I- " << getFullPath() << " [CC] FECN: " << fecnCount << " BECN: "
        << becnCount << endl;
}

string CCMgr::convertIntToString(int number)
{
   stringstream ss;
   ss << number;
   return ss.str();
}
