//////////////////////////////////////////////////////////////////////////////
//
// OMNET InfiniBand Congestion Control Model
//
// Copyright (C) 2009 Simula Research Laboratory
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//////////////////////////////////////////////////////////////////////////////

#include <omnetpp.h>
#include <vector>

#include "ib_m.h"

#ifndef CCMGR_H
#define CCMGR_H

#define THRESHOLD_MAX 0xF
#define CCT_MAX 128
#define SL_MAX 16 //XXX maybe just 4?
#define CNP_QUEUE_SIZE 32

using std::string;

enum
{
    IB_FECN         = 1 << 0,
    IB_BECN         = 1 << 1,
};

typedef struct
{
    simtime_t time;
    size_t plen;
} ptime_t;

class timerMessage : public cMessage
{
private:
    int ccid; // The "id" of a cc entry (VL or SrcLID depending on level of operation)

public:
    int getCCID()
    {
        return ccid;
    }

    void setCCID(int ccid_)
    {
        ccid = ccid_;
    }
};

class CCMgr : public cSimpleModule
{
private:
    cModule *sw;
    cModule *hca;
    cModule *parent;

    int duringInit;

    /* parameters */
    int creditSize;
    int maxVL;              // Maximum value of VL
    int VLcount;
    int speed;
    int width;
    int hcaParent;
    unsigned numPorts;

    /* switch parameters */
    int Threshold;
    int Marking_Rate;
    int Packet_Size;
    int Victim_Mask;
    int Reset_Threshold;

    /* hca parameteres */
    int CCTI_Increase;
    int CCTI_Limit;
    simtime_t CCTI_Timer;
    int CCTI_Min;
    int CCT_Algorithm;
    int CCT_Div;
    int AddIrdFlightTime;

    // data structure to hold BECN (IBCNPMsg) messages waiting to be sent
    cQueue *CNPQueue;	// queue of IBCNPMsg messages (that is, BECN messages)
    size_t CNPqSize;		// Size of the IBCNPMsg queue. Should be able to

    int CNPSn;			// Serial number for the CN packets
    int CNPvl;          // last cnp vl

    // structures for cct
    uint16_t cct[CCT_MAX];
    int *ccti;
    timerMessage *timerEvent;
    ptime_t *packetTime;

    bool qp_level;
    int maxDstLID; // used if running CC at the QP (dstLid) level
    bool singleThrPerOP; // true -> single threshold for all VoQs to given output port
    bool singleThrDevide; // true -> (sum(all fill ratios))/(#contributors) compared to threshold
						  // false -> (sum(all fill ratios)) compared to threshold

    bool **congestedInputPorts; //congested port state (index VL, port)
    unsigned *congested; //number of congested ports (index VL)
    // Structure to keep track of fill ratio of input buffers headed for a certain output port
    float **inputPortsFillStatus; // (index VL, port)

    int num_nodes;

    // bookkeeping
    unsigned CNPsThrownAwayAtHCA;	// #times the HCA is not able to enqueue a CNP
    unsigned CNPsEnqueuedAtHCA;		// #CNPs enqueued at HCA
    unsigned fecnCount;
    unsigned becnCount;

    cOutVector *cctiPos;
    cOutVector *congStatus;
    cOutVector **bufStatusRecord;
    bool cc_extended_ibuf_log; // true -> enable extended logging of ibuf fill ratio (bufStatus vector)
    cOutVector *obufSrcLidOfFECNmarkedPacket; // records the src lid of packets marked with FECN
    cOutVector *obufStatusCCOn;	// obuf fill ratio when CC is turned on (per vl)
    cOutVector *obufStatusCCOff;// obuf fill ratio when CC is turned off (per vl)
    cOutVector *obufQHcounterCCOn; // obuf header logger when CC is turned on (per vl)
    cOutVector *obufQHcounterCCOff; // obuf header logger when CC is turned off (per vl)

    // Interval logging
    simtime_t logInterval;  // Time between interval result logging
    simtime_t lastIntLogTime;   // Time the last interval logging took place
    int lastIntLogFECN; // Bookkeeping for interval logging
    int lastIntLogBECN; // Bookkeeping for interval logging
    cOutVector interval_CC_FECN_Stats; // Record the interval logging
    cOutVector interval_CC_BECN_Stats; // Record the interval logging


    void initialize();
    void handleMessage(cMessage *msg);
	void handleIntervalLogging(cMessage *msg);
    void populateCCT();
    void finish();
    bool CNPqInsert(cMessage *msg);
    void cctiTimerReset(timerMessage *msg);
    void cctiInit();
    void packetTimeInit();
    void recordCongStateChange(int vl, unsigned inport, bool turnedOn);

protected:

public:
	bool congDstLid(int dstLid); // true if packets headed for dstLid is contributing to congestion (req: ad hoc QP mode)
    bool CNPqEmpty();
    bool CNPqFull();
    cMessage* CNPqPop();
    void initializeSW(cModule *parent_);
    void initializeHCA(cModule *parent_);
    void updateCONG(int vl, unsigned port, size_t quelen, size_t maxlen);
    void markFECN(cMessage *msg);
    simtime_t getIRD(int vl);
    int canSend(IBDataMsg *msg);
    int canSend(int dstLid, int dstVl);
    void updatePacketTime(int sl, simtime_t t, size_t len);
    void cctiTimer(timerMessage *msg);
    void checkFECN(cMessage *msg);
    void checkBECN(cMessage *msg);
    string convertIntToString(int number);
    bool operateAtQPLevel();
};

#endif
