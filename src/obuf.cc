//////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2004 Mellanox Technologies LTD. All rights reserved.
//
// This software is available to you under a choice of one of two
// licenses.  You may choose to be licensed under the terms of the GNU
// General Public License (GPL) Version 2, available from the file
// COPYING in the main directory of this source tree, or the
// OpenIB.org BSD license below:
//
//     Redistribution and use in source and binary forms, with or
//     without modification, are permitted provided that the following
//     conditions are met:
//
//      - Redistributions of source code must retain the above
//        copyright notice, this list of conditions and the following
//        disclaimer.
//
//      - Redistributions in binary form must reproduce the above
//        copyright notice, this list of conditions and the following
//        disclaimer in the documentation and/or other materials
//        provided with the distribution.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////////
//
// The IBOutBuf implements an IB port FIFO
// See functional description in the header file.
//
// Contains modifications fixing the unconnected ports issues.
#include "ib_m.h"
#include "obuf.h"

Define_Module( IBOutBuf );

void IBOutBuf::initialize()
{
	// read parameters
	speed = par("speed");
	width = par("width");
	qSize = par("size");
	maxVL = par("maxVL");
    isHcaObuf = par("isHcaObuf");
	recordVectors = par("recordVectors");

    if (width == 1 || width % 4 == 0)
        Enabled = true;
    else if (width == 0)
    {
        Enabled = false;
    }
    else
        opp_error("invalid width %d (should be factor of 4, or 0 to disable)",width);

	// MinTime is a function of width
	credMinTime_us = par("credMinTime");
	credMinTime_us = credMinTime_us*4/width;

	// Initiazlize the statistical collection elements
	qDepthHist.setName("Queue Usage");
	qDepthHist.setRangeAutoUpper(0, 10, 1);
	packetStoreHist.setName("Packet Storage Time");
	packetStoreHist.setRangeAutoUpper(0, 10, 1.5);
	flowControlDelay.setName("Time between VL0 FC");
	flowControlDelay.setRangeAutoUpper(0,10,1.2);
	qDepth.setName("Queue Depth");
	// 4x 2.5Gbps = 1Byte/nsec ; but we need the 10/2.5/4.0 ...
	popDelayPerByte_us = 1.0e-3 * 10 / width / speed;
	WATCH(popDelayPerByte_us);
	WATCH(credMinTime_us);

	// we will allocate a pop messgae only when the first credit is pushed in
	p_popMsg = NULL;

	// queue is empty
	prevPopWasDataCredit = 0;
	insidePacket = 0;
	isMinTimeUpdate = 0;

	// Init the iterator for this queue
	// This one is only used when searching through the Q to do bookkeeping
	queueIterator = new cQueue::Iterator(queue);

    // no packets in flight
    packetsInFlight = 0;

    //qSizePerVl
    qSizePerVL = new unsigned[maxVL+1];

	for ( int i = 0; i < maxVL+1; i++ )
	{
		prevSentFCTBS.push_back(-9999);
		prevSentFCCL.push_back(-9999);
        prevFCTime.push_back(0);
		FCCL.push_back(0);
		FCTBS.push_back(0);
        qSizePerVL[i] = 0;
	}

    cc = par("cc_enabled");
    if(cc)
    {
        cModule *calleeModule = getParentModule()->getSubmodule("ccmgr");
        ccmgr = check_and_cast<CCMgr *> (calleeModule);
    }

	WATCH_VECTOR(prevSentFCTBS);
	WATCH_VECTOR(prevSentFCCL);
	WATCH_VECTOR(FCTBS);
	WATCH_VECTOR(FCCL);

	if (Enabled) {
      // but we do want to have a continous flow of MinTime
      p_minTimeMsg = new cMessage("minTime", IB_MINTIME_MSG);
      // Send the first mintime immediately so that all is initialised when we get first packets
      //scheduleAt(simTime() + credMinTime_us*1e-6, p_minTimeMsg);
      scheduleAt(simTime() , p_minTimeMsg);
	}

	ev << "-I- " << getFullPath() << " init completed" << endl;
}

// send the message out
// Init a new pop message and schedule it after delay
// Note that at this stage the Q might be empty but a
// data packet will be streamed out
void IBOutBuf::sendOutMessage(IBWireMsg *p_msg) {
	simtime_t delay = p_msg->getBitLength() / 8.0 * popDelayPerByte_us;
	if ( ! p_popMsg )
	{
		p_popMsg = new cMessage("pop", IB_POP_MSG);
		scheduleAt(simTime() + delay*1e-6, p_popMsg);
	}
	if (!ev.isDisabled())
	   ev << "-I- " << getFullPath() << " sending msg:" << p_msg->getName() << " at time " << simTime() <<endl;
	// track out going packets
	if ( p_msg->getKind() == IB_DATA_MSG )
	{
		IBDataMsg *p_dataMsg = (IBDataMsg *)p_msg;

        qSizePerVL[p_msg->getVL()]--;

        if(cc && !isHcaObuf && p_dataMsg->getCreditSn() == 0)
            ccmgr->markFECN(p_dataMsg);

		// track if we are in the middle of packet
		if (!p_dataMsg->getCreditSn() && (p_dataMsg->getPacketLength() > 1))
			insidePacket = 1;
		else if (p_dataMsg->getCreditSn() + 1 == p_dataMsg->getPacketLength())
			insidePacket = 0;

		FCTBS[p_msg->getVL()]++;
	    // time stamp message to measure how much time the message spent in the network
	    if(isHcaObuf) {
	        simtime_t time_stamp = simTime();
/*	        double tts = time_stamp.dbl();
	        p_dataMsg->setTimestamp(time_stamp);
	        int csn = p_dataMsg->getCreditSn();
	        int msgRemaining = p_dataMsg->getMsgRemaining();
	        int updatedremaining = p_dataMsg->getMsgSize() - 2*p_msg->getPacketLengthBytes();
	        int msgSize =  p_dataMsg->getMsgSize();
*/
	        if ( p_dataMsg->getCreditSn() == 0) {
//	            double tt = simTime().dbl();
	            msg_time = simTime();
	        }
//	        double tt = msg_time.dbl();
	        p_dataMsg->setMsgTimestamp(msg_time);
	    }
	}
	ev << "-I- " << getFullPath() << "current obuf qsize " << getQSize() << endl;
	send(p_msg, "out");
}

// Q a message to be sent out.
// If there is no pop message pending can directly send...
void
IBOutBuf::qMessage(IBDataMsg *p_msg) {
	// we stamp it to know how much time it stayed with us
	//p_msg->setTimestamp(simTime());

    decPacketsInFlight();
    qSizePerVL[p_msg->getVL()]++;

	if ( p_popMsg )
	{
		if ( qSize <= queue.length() )
		{
                        ev << "-E- " << getFullPath() << " need to insert " <<
                            p_msg->getName() << " " << p_msg->getPacketId() <<
                            " " << p_msg->getSrcLid() <<
                            " into a full Q " << endl;
                        ev.flush();
			exit(1);
		}

		if (!ev.isDisabled()) {
			ev << "-I- " << getFullPath() << " queued data msg:" << p_msg->getName() << " Qdepth " << queue.length() << endl;
			recordVectors = par("recordVectors");
		}

		queue.insert(p_msg);
		if ( recordVectors )
			qDepth.record(queue.length());
	}
	else
	{
		// track the time this PACKET (all credits) spent in the Q
		// the last credit of a packet always
		if ( p_msg->getCreditSn() + 1 == p_msg->getPacketLength() )
		{
			//packetStoreHist.collect( simTime() - packetHeadTimeStamp );
		}
		else if ( p_msg->getCreditSn() == 0 )
		{
			packetHeadTimeStamp = p_msg->getTimestamp();
		}
		sendOutMessage(p_msg);
	}
}

// check if need to send a flow control and send it if required.
// return 1 if sent or 0 if not
// this function should be called by the pop event to check if flow control
// is required to be sent
// The minTime event only zeros out the curFlowCtrVL such that the operation
// restarts.
// New Hermon mode provides extra cases where a flow control might be sent:
// 1. If there are no other messages in the Q
//
// Also if there are messages in the Q we might not send FC unless the difference
// is large enough
int IBOutBuf::sendFlowControl()
{
   static long flowCtrlId = 0;
   int sentUpdate = 0;

   // checks whether it is a HCA or a Switch
   //if it's a HCA, it ommits the condition
   if(!isHcaObuf){
	   // sanity check for an "out" gate
	   // if a module (Switch) does not contain an out interface at the top level, the fix won't work
	   if((getParentModule() -> hasGate("out"))){
		   // sanity check whether the existing "out" interface is further connected
		   // if it isn't, no packets can be sent
		   if((getParentModule() -> gate("out") -> getNextGate()!=NULL)){
			   // creates a Switch module
			   cModule* switchModule = getParentModule() -> getParentModule();
			   // checks whether the "out[getIndex()] port of the switch is further connected
			   // if the port is not connected, no packets can be sent through it
			   // getIndex returns the port number currently in use in the switch (parent of the current obuf)
			   if(switchModule->gate("out", getParentModule() -> getIndex())->getNextGate()==NULL)
				   return(0);
		   }
		   else
			   return(0);
	   }
   }

   // we should not continue if the Q is not empty if we aren't in mintime mode
   if (! isMinTimeUpdate && ! queue.empty())
		return(0);

   if (curFlowCtrVL >= maxVL+1)
   {
	  return(0);
   }

   for (; (sentUpdate == 0) && (curFlowCtrVL < maxVL+1); curFlowCtrVL++ )
   {
		int i = curFlowCtrVL;

		// dont send flow control message if there is no change in FCTBS or FCCL
        // or the timeout has passed (time to send 65536 symbols)
		if ( prevSentFCTBS[i] != FCTBS[i] || prevSentFCCL[i] != FCCL[i]
                || simTime() > prevFCTime[i] + (4e-9)/(speed/2.5))
		{
			// create a new message and place in the Q
			char name[128];
			sprintf(name, "fc-%d-%ld", i, flowCtrlId++);
			IBFlowControl *p_msg = new IBFlowControl(name, IB_FLOWCTRL_MSG);

			p_msg->setBitLength(8*8);
			p_msg->setVL(i);
			p_msg->setFCCL(FCCL[i]);
			p_msg->setFCTBS(FCTBS[i]);
			prevSentFCCL[i] = FCCL[i];
			prevSentFCTBS[i] = FCTBS[i];
            prevFCTime[i] = simTime();
			if (!ev.isDisabled())
				ev << "-I- " << getFullPath() << " generated:" << p_msg->getName()
					<< " vl:" << p_msg->getVL() << " FCTBS:"
					<< p_msg->getFCTBS() << " FCCL:" << p_msg->getFCCL() << endl;

			// we do not need to Q as we are only called in pop
			sendOutMessage(p_msg);

			sentUpdate = 1;
		}
		// last VL zeros the min time update flag
		if (curFlowCtrVL == 7)
			isMinTimeUpdate = 0;
   }

   return(sentUpdate);
}

// Handle Pop Message
// Should not be called if the Q is empty.
// Simply pop and schedule next pop.
// Also schedule a "free" message to be sent out later
void IBOutBuf::handlePop()
{
	simtime_t delay;

	cancelAndDelete( p_popMsg );
	p_popMsg = NULL;

	// if we got a pop - it means the previous message just left the
	// OBUF. In that case if it was a data credit packet we have now a
	// new space for it. tell the VLA.
	if (prevPopWasDataCredit)
	{
		cMessage *p_msg = new cMessage("free", IB_FREE_MSG);
		if (!ev.isDisabled())
			ev << "-I- " << getFullPath() << " sending 'free' to VLA as last packet just completed." << endl;
		send(p_msg, "free");
	}

	// try sending a flow control if required:
	if (!insidePacket && sendFlowControl())
	{
		prevPopWasDataCredit = 0;
		return;
	}

	// got to pop from the queues if anything there
    if(!queue.empty())
    {
        IBWireMsg *p_msg = (IBWireMsg *)queue.pop();

        if ( p_msg && p_msg->getKind() == IB_DATA_MSG )
        {
            IBDataMsg *p_cred = (IBDataMsg *) p_msg;
            if (!ev.isDisabled())
                ev << "-I- " << getFullPath() << " popped data message:" << p_cred->getName() << endl;
            sendOutMessage(p_msg);

            // track the time this PACKET (all credits) spent in the Q
            // the last credit of a packet always
            if ( p_cred->getCreditSn() + 1 == p_cred->getPacketLength() )
            {
                //packetStoreHist.collect( simTime() - packetHeadTimeStamp );
            }
            else if ( p_cred->getCreditSn() == 0 )
            {
                packetHeadTimeStamp = p_msg->getTimestamp();
            }

            // we just popped a real credit
            prevPopWasDataCredit = 1;
        }
        else
        {
            ev << "-E- " << getFullPath() << " unknown message type to pop:"
                << p_msg->getKind() << endl;
        }
    }
	else
	{
		// The queue is empty. Next message needs to immediatly pop
		// so we clean this event
		if (!ev.isDisabled())
			ev << "-I- " << getFullPath() << " nothing to POP" << endl;
		prevPopWasDataCredit = 0;
	}

	if (!ev.isDisabled())
		recordVectors = par("recordVectors");

	if ( recordVectors )
		qDepth.record(queue.length());
}

// Handle MinTime:
// If the prev sent VL Credits are no longer valid send push an update
void IBOutBuf::handleMinTime()
{
   if (!ev.isDisabled())
		ev << "-I- " << getFullPath() << " handling MinTime event" << endl;

	curFlowCtrVL = 0;
	isMinTimeUpdate = 1;
	// if we do not have any pop message - we need to create one immediatly
	if (! p_popMsg )
	{
		p_popMsg = new cMessage("pop", IB_POP_MSG);
		scheduleAt(simTime() + 1e-9, p_popMsg);
	}

	// we use the min time to collect Queue depth stats:
	//qDepthHist.collect( queue.length() );

	scheduleAt(simTime() + credMinTime_us*1e-6, p_minTimeMsg);
}

// Handle rxCred
void IBOutBuf::handleRxCred(IBRxCredMsg *p_msg)
{
	// update FCCL...
	FCCL[p_msg->getVL()] = p_msg->getFCCL();
	delete p_msg;
}

void IBOutBuf::handleMessage(cMessage *p_msg)
{
	int msgType = p_msg->getKind();
	if ( msgType == IB_POP_MSG )
	{
		handlePop();
	}
	else if ( msgType == IB_MINTIME_MSG )
	{
		handleMinTime();
	}
	else if ( msgType == IB_DATA_MSG )
	{
		qMessage((IBDataMsg*)p_msg);
	}
	else if ( msgType == IB_RXCRED_MSG )
	{
		handleRxCred((IBRxCredMsg*)p_msg);
	}
	else
	{
		ev << "-E- " << getFullPath() << " do no know how to handle message:" << msgType << endl;
		delete p_msg;
	}
}

void IBOutBuf::finish()
{
	/* ev << "STAT: " << getFullPath() << " Data Packet Q time num/avg/max/std:"
		<< packetStoreHist.getCount() << " / "
		<< packetStoreHist.getMean() << " / "
		<< packetStoreHist.getMax() << " / "
		<< packetStoreHist.getStddev() << endl;
		ev << "STAT: " << getFullPath() << " Q depth num/avg/max/std:"
		<< qDepthHist.getCount() << " / "
		<< qDepthHist.getMean() << " / "
		<< qDepthHist.getMax() << " / "
		<< qDepthHist.getStddev() << endl;
	 ev << "STAT: " << getFullPath() << " FlowControl Delay num/avg/max/std:"
		<< flowControlDelay.getCount() << " / "
		<< flowControlDelay.getMean() << " / "
		<< flowControlDelay.getMax() << " / "
		<< flowControlDelay.getStddev() << endl;
	*/
}


// Methods used to iterate the queue. Note: used for bookkeeping only
void IBOutBuf::initIterator() {
	queueIterator->init(queue);
}
bool IBOutBuf::iterationDone() {
	return queueIterator->end();
}
cObject *IBOutBuf::nextIterationElement(){
	return queueIterator->operator ++(0);
}
