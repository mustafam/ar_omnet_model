/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2004 Mellanox Technologies LTD. All rights reserved.
//
// This software is available to you under a choice of one of two
// licenses.  You may choose to be licensed under the terms of the GNU
// General Public License (GPL) Version 2, available from the file
// COPYING in the main directory of this source tree, or the
// OpenIB.org BSD license below:
//
//     Redistribution and use in source and binary forms, with or
//     without modification, are permitted provided that the following
//     conditions are met:
//
//      - Redistributions of source code must retain the above
//        copyright notice, this list of conditions and the following
//        disclaimer.
//
//      - Redistributions in binary form must reproduce the above
//        copyright notice, this list of conditions and the following
//        disclaimer in the documentation and/or other materials
//        provided with the distribution.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////////
//
// IB Packets Generator:
// Send IB credits one at a time.
//
// Internal Messages:
// push - inject a new data credit into the Queues
//
// External Messages:
// sent - a vHoQ was sent by the VLA
//
// MODES:
// Normal mode - every push a new data credit will be inejcted
// Flood mode - on init and "sent" event the Q depth will be checked and filled.
//

#include "ib_m.h"
#include "gen.h"
#include "vlarb.h"
#include <vec_file.h>
#include <cassert>

using std::max;

Define_Module(IBGenerator);

// use static to get a new packet id which is globaly unique
int IBGenerator::getPacketId()
{
    if (!ev.isDisabled())
    	ev << "FUNC:  getPacketId " << endl;
    static int id = 0;

    return(++id);
}

// use static to get a new packet id which is globaly unique
int IBGenerator::getMsgId()
{
    if (!ev.isDisabled())
        ev << "FUNC:  getMsgId " << endl;
    static int msgId = 0;

    return(++msgId);
}

// returns the integers associated with parName as a vector out
void IBGenerator::parseIntListParam(const char *parName, std::vector<int> &out)
{
    if (!ev.isDisabled())
    	ev << "FUNC:  parseIntListParam " << endl;

    const char *str = par(parName);
    char *tmpBuf = new char[strlen(str)+1];
    strcpy(tmpBuf, str);
    char *entStr = strtok(tmpBuf, " ,");
    if (out.size()) out.clear();
    while (entStr) {
        out.push_back(atoi(entStr));
        entStr = strtok(NULL, " ,");
    }
}
// allocate and init new ACK data credit ("the next flit of a packet")
IBDataMsg *IBGenerator::getNewAckMsg(IBDataMsg *p_msg)
{
    IBDataMsg *p_cred;
    if (!ev.isDisabled())
    	ev << "FUNC:  getNewAckMsg " << endl;

    char name[128];
    sprintf(name, "ack-%d-%d", p_msg->getPacketId(), creditCounter);
    p_cred = new IBDataMsg(name, IB_DATA_MSG);
    //p_cred->setSL(p_msg->getSL());

    p_cred->setSrcLid(srcLid);
    p_cred->setDstLid(p_msg->getSrcLid());

    if(virtualLaneFile) {
    	p_cred->setVL(vlSeq->at(p_cred->getDstLid()));
    	p_cred->setSL(p_cred->getVL());
    }
    else {
    	p_cred->setVL(p_msg->getVL());
		p_cred->setSL(p_msg->getSL());
    }

    p_cred->setCreditSn(creditCounter);
    p_cred->setPacketLength(packetLength);
    p_cred->setPacketLengthBytes(packetLengthBytes);
    p_cred->setMsgSize(MsgLength);
    p_cred->setMsgRemaining(MsgLength);
    p_cred->setBitLength(creditSize*8);
    p_cred->setPpResponse(p_msg->getPpResponse());
    if (!ev.isDisabled()) {
    	ev << "-I- " << " Packet Length : " << p_cred->getPacketLength() << " creditSn : " << p_cred->getCreditSn() << " Bit Length : " << p_cred->getBitLength() << endl;
    }
    return p_cred;
}


// allocate and init a new data credit ("the next flit of a packet")
IBDataMsg *IBGenerator::getNewDataMsg()
{
    IBDataMsg *p_cred;
    if (!ev.isDisabled())
    	ev << "FUNC:  getNewDataMsg " << endl;
    char name[128];
    sprintf(name, "data-%d-%d", packetId, creditCounter);
    p_cred = new IBDataMsg(name, IB_DATA_MSG);
    //p_cred->setSL(SL);


    if (RemainPktLength>=creditSize) {
        p_cred->setBitLength(creditSize*8); // length is in bits
        RemainPktLength -= creditSize;
    } else {
        p_cred->setBitLength(RemainPktLength*8); // length is in bits
        RemainPktLength = 0;
    }
    p_cred->setCreditSn(creditCounter);
    p_cred->setPacketId(packetId);
    p_cred->setMsgId(msgId);
    p_cred->setSrcLid(srcLid);
    p_cred->setDstLid(dstLid);
    p_cred->setMsgSize(MsgLength);
    p_cred->setPacketLength(packetLength);
    p_cred->setPacketLengthBytes(packetLengthBytes);
    p_cred->setMsgRemaining(MsgLength);
    p_cred->setFECN(0);
    p_cred->setBECN(0);

    if(virtualLaneFile) {
    	p_cred->setVL(vlSeq->at(p_cred->getDstLid()));
    	p_cred->setSL(p_cred->getVL());
    }
    else {
    	p_cred->setVL(packetVL);
    	p_cred->setSL(SL);
    }

    if (!ev.isDisabled()) {
    	ev << "-I- " << " Packet Length : " << p_cred->getPacketLength()
					 << " creditSn : " << p_cred->getCreditSn()
					 << " Bit Length : " << p_cred->getBitLength()
					 << " VL: " << p_cred->getVL()
					 << " dstLID: " << p_cred->getDstLid()
					 << endl;
    }

    return p_cred;
}

// initialize the parameters for a new data packet.
// optionally being given a VL to use.
bool IBGenerator::initIBPacketParams(int vl)
{
	bool passedVL = (vl==-1) ? false : true;
    ev << "FUNC:  initIBPacketParams " << endl;
    int rand = -1;
    // Current packet parameters
    packetId = getPacketId();

    // In case we are in flood mode, generate packet as part of message.
    // Else Generate each packet independantly with length packetLength

    if (firstPacket == 1) { // First packet to send
		MsgLength = par("msgLengthInMTUs");
		MsgLength = MsgLength*mtu*creditSize;
		RemainMsgLength = MsgLength;
    }

    // Generate packet Length
    if (RemainMsgLength == 0 ) {
        opp_error(" pktid =%d", packetId);
    }
    packetLengthBytes = getPacketLen();


    // Make sure the minimal size of packet is always 8
    // Packet is at least 20B: LRH(8B) + BTH(12B)
    if (packetLengthBytes%creditSize<8 && packetLengthBytes%creditSize!=0) {
        packetLengthBytes += (8-packetLengthBytes%creditSize) ;
    }
    if (packetLengthBytes%creditSize == 0 ) {
        packetLength = packetLengthBytes/creditSize ;
    } else {
        packetLength = packetLengthBytes/creditSize +1;
    }

    creditCounter = 0; // count from 0 up.

    // CHOSING DSTLID
    if (dstSeqMode != DST_BY_PARAM) {

        // only change dstLid if we crossed a message
        if (!ev.isDisabled()) {
        	ev << "-I- " << " mtuInMsgIdx :" << mtuInMsgIdx << " MsgLength :" << MsgLength <<" mtu :" << mtu <<" GenModel :" << GenModel << " firstPacket :" << firstPacket << endl;
        }
        if (firstPacket ||
            (GenModel == 1 && (RemainMsgLength == MsgLength || (trafficDist != TRF_FLOOD && trafficDist!=TRF_APP))) ||
            (GenModel == 0 && (mtuInMsgIdx + 1 == MsgLength/(creditSize*mtu)))) {

        	// Alternate between HotSpot and SeqLoop modes if running in BurstHotSpot destination mode
            if (dstBHS) {
                if (dstSeqMode == DST_HOT_SPOT && ((simTime() - dstBHS_start) > dstBurstHSDel)) {
                	// Time to change from DST_HOT_SPOT to DST_SEQ_LOOP destination distribution
                    if (!ev.isDisabled()) {
                        ev << "-I- " << getFullPath() << " HS->SL simTime():"<< simTime()<< " dstBHS_start:" << dstBHS_start<< " diff:" << simTime() - dstBHS_start << " dstBurstHSDel:" << dstBurstHSDel << endl;
                    }
                    dstSeqMode = DST_SEQ_LOOP;
                    dstBHS_start = simTime();
                } else if (dstSeqMode == DST_SEQ_LOOP && ((simTime() - dstBHS_start) > dstBurstHSInterDel)) {
                	// Time to change from DST_SEQ_LOOP to DST_HOT_SPOT destination distribution
                    if (!ev.isDisabled()) {
                        ev << "-I- " << getFullPath() << " SL->HS simTime():"<< simTime()<< " dstBHS_start:" << dstBHS_start<< " diff:" << simTime() - dstBHS_start << " dstBurstHSDel:" << dstBurstHSDel << endl;
                    }
                    dstSeqMode = DST_HOT_SPOT;
                    dstBHS_start = simTime();
                }
            }

            // If in hot spot mode we draw to check if the next msg should be sent to the hot spot or elsewhere
            if (dstSeqMode == DST_HOT_SPOT) {
                rand = bernoulli(dstHotSpotPerc,0);
            } else {
                rand = 0;
            }

            if (dstSeqMode == DST_HOT_SPOT && rand ==1) { // We want to send to the hot spot
                dstLid = parDstLid;
            } else if (dstSeqMode == DST_RANDOM || (dstSeqMode == DST_HOT_SPOT && rand == 0 && HotSpotR)) {
                dstLid = (*dstSeq)[intuniform(0,dstSeq->size()-1)];
            } else {
                dstLid = (*dstSeq)[dstSeqIdx];
                dstSeqIdx = ++dstSeqIdx;
                if (dstSeqIdx == dstSeq->size()) {
                    dstSeqIdx = 0;
                }
                // In case we are in bursty HS, we do not want to get the
                // parameter Lid outside the window .
                if (dstSeqMode == DST_SEQ_LOOP && dstBHS && dstLid == parDstLid) {
                    dstLid = (*dstSeq)[dstSeqIdx];
                    dstSeqIdx = ++dstSeqIdx;
                    if (dstSeqIdx == dstSeq->size()) {
                        dstSeqIdx = 0;
                    }
                }
                if ((dstSeqMode == DST_SEQ_ONCE) && (dstSeqIdx == 0)) {
                    dstSeqDone = 1;
                }
            }

            // HACK TO AVOID INTERNAL HOL BLOCKING BY HS TRAFFIC IN THE GEN WHILE USING
            // CC IN FLOOD MODE && DST_HOT_SPOT && RANDOM ALTERNATIV DST :-P
            //
            // Start by finding the vlarb to check if CC is enabled:
            cGate *p_gate = gate("out")->getPathEndGate();
            IBVLArb *p_vla = dynamic_cast<IBVLArb *>(p_gate->getOwnerModule());
            if ((p_vla == NULL) || strcmp(p_vla->getName(), "vlarb")) {
                ev << "-E- " << getFullPath() << " cannot get VLA for out port" << endl;
                ev.flush();
                exit(3);
            }
            if (p_vla->ccEnabled() &&
            		simTime() > 0 &&
            		trafficDist == TRF_FLOOD &&
            		dstSeqMode == DST_HOT_SPOT &&
            		HotSpotR){
            	// At this point we know that CC is enabled in the network
            	// and that we're running in HotSpot mode with random
            	// alternative dst. To avoid (or actually minimize the
            	// chance of) the head-of-line-blocking internally in the
            	// generator, while still not exceeding the percentage
            	// of traffic going to the hotspot and the random nodes
            	// in the network, we need to do quite a lot of testing
            	// and corresponding actions... :-)

            	// First of all we need to find how much we could have sent this far
            	// to avoid exceeding the % of traffic not supposed to be sent to nonHSes.
            	// ToDo:
                int PktLen = 0; // in number of credits
            	simtime_t timeAlive, now;
            	double timeToSendPkt = 0.0;
            	double timeToSendByte = 0.0;
            	double maxBytesSent = 0.0;

            	now = simTime();
            	if(now <= genStartTime) { return false; /* We give up as we shouldn't actually have started */ }
            	timeAlive = now - genStartTime;  // timeAlive > zero  (in sec)

            	// Calculate the time to send a single packet (in ns), _including_ delay...
            	PktLen = par("packetlength");
            	timeToSendPkt = 1.0 * (10.0 * PktLen * creditSize)/ (width * speed);
            	// ...adding the intraBurstDelay (using the formula from the
            	// sendDataOut(...) method):
            	timeToSendPkt += (((getIntraBurstDelay()) * PktLen)/3.0);   // in nsec

            	// Now we have the total time to send a single packet, including delay.
            	// Calculate the average time it takes to send a byte. That is
            	// time_to_send_a_packet_in_nsec/size_of_packet_in_byte
            	// => result in nsec/byte:
            	timeToSendByte = timeToSendPkt/(PktLen * creditSize);

            	// The maximum amount of traffic we could have sent this far:
            	maxBytesSent = ((1e+9 * timeAlive.dbl()) / timeToSendByte);  //  ( nsec / (nsec/byte) ) = byte

            	if(dstLid != parDstLid) {
            		// We would like to send traffic to a random node.
            		// 1. Check if we haven't already sent more random
            		// traffic than we should:
            		if((((double)NoneHSBytesSent)/maxBytesSent) > (1.0-dstHotSpotPerc)) {
            			// We've sent too much random traffic (and by that too little
            			// hotspot traffic). Check to see if we could queue a hotspot
            			// packet instead:
            			if(!p_vla->ccmgr->canSend(parDstLid,vl)) {
            				// We can's send hotspot traffic either.
            				// Give up:
            				return false;
            			} else {
            				// We're allowed to sent to the hotspot.
            				// Changing the lid:
            				dstLid = parDstLid;
            			}
            		} else {
						// We're allowed to send random traffic. Just keep the
            			// lid (that is, do nothing :))
            		}
            	} else {
            		// We would like to send traffic to the hotspot.
            		// Check if traffic to the hotspot is
            		// held back by the CC:
            		if(!p_vla->ccmgr->canSend(parDstLid,vl)) {
            			// Traffic to the hotspot is held back
            			// by the CC. Send random traffic instead
            			// unless we've sent too much already.
            			if((((double)NoneHSBytesSent)/maxBytesSent) > (1.0-dstHotSpotPerc)) {
                			// We've sent too much random traffic.
            				// Giving up:
            				return false;
            			} else {
            				// We're allowed to send random traffic.
            				// Find a random destination, avoiding the hotspot:
                    		while(dstLid == parDstLid) {
                    			dstLid = (*dstSeq)[intuniform(0,dstSeq->size()-1)];
                    		}
                    		// The dstLid we now have _could_ also be held back by the CC, but
                    		// we consider that to be reasonable...at least for know :-)
            			}
            		} else {
            			// Hotspot traffic is NOT held back by CC
            			// Have we sent too much hotspot traffic already?
            			if ((((double)(AccBytesSent - NoneHSBytesSent))/maxBytesSent) > dstHotSpotPerc) {
            				// Yes, too much HS traffic. Note that this implies
            				// that we can (and should!) send more random traffic
            				// so let's do that:
                    		while(dstLid == parDstLid) {
                    			dstLid = (*dstSeq)[intuniform(0,dstSeq->size()-1)];
                    		}
            			} else {
            				// At this point we know that i) hotspot traffic
            				// is not held back by CC, and ii) we can send
            				// hotspot traffic without exceeding the amount
            				// of traffic supposed to go to the hotspot.
            				// That is: just keep the lid; do nothing :)
            			}
            		}
            	}

            	// Data for the destination dstLid (vl) will be
            	// queued. We're happy...
            } // END CC INTERNAL GENERATOR HOL HACK :-P


            if (GenModel == 0) {
                mtuInMsgIdx = 0;
            }
        } else {
            if ((GenModel == 0) && (mtuInMsgIdx  < MsgLength/(creditSize*mtu))) { // is this redundant?
                mtuInMsgIdx++;
            }
        }
    } else {
        dstLid = parDstLid; // destination given by the dstLid parameter
    }

    if (dstLid == 0) {
        opp_error("Error: Packet %d has destination Lid = 0", packetId);
    }

    // Update the length of the remaining msg:
    if (RemainMsgLength <= mtu*creditSize) {
        if (GenModel == 0) {
            MsgLength = par("msgLengthInMTUs");
            MsgLength = MsgLength*mtu*creditSize;
        } else {
            if (trafficDist != TRF_APP) {
                MsgLength = par("msgLength");
            } else {
                MsgLength = (*sizeSeq)[dstSeqIdx];
            }
        }
        RemainMsgLength = MsgLength;

    } else {
        RemainMsgLength = RemainMsgLength - packetLengthBytes;
    }
    // Update the length of the remaining pkt:
    RemainPktLength = packetLengthBytes;
    firstPacket = 0;

    // set the VL:
    if (!passedVL && !virtualLaneFile)
        packetVL = par("VL");
    else if(passedVL)
    	packetVL = vl;
    else if(!passedVL && virtualLaneFile)
    	packetVL = vlSeq->at(dstLid);

    if (!ev.isDisabled())
    	ev << "packetVL in initIBPacketParams:" << packetVL << endl;

    return true;
}


int IBGenerator::getPacketLen(){
    if (!ev.isDisabled())
    	ev << "FUNC:  getPacketLen " << endl;

    int PktLen = 0;
    if (GenModel == 0) {
        PktLen = par("packetlength");
        PktLen *= creditSize;
    } else {
        if (trafficDist== TRF_MIXLEN) {
            int TotProb = PktLenTotProb;
            int res;
            for (unsigned i = 0; i<PktLenDist.size();i++) {
                float k = (float) PktLenProb[i]/TotProb;
                res = bernoulli(k,0) ;
                if (res == 1) {
                    PktLen = PktLenDist[i];
                    return PktLen;
                } else {
                    TotProb -= PktLenProb[i];
                }
            }
        }
        else if (trafficDist == TRF_FLOOD)
        {
            PktLen = par("packetlength");
            PktLen *= creditSize;
        }
        else if (trafficDist == TRF_UNI || trafficDist == TRF_APP) {
            if (RemainMsgLength >= mtu*creditSize) {
                PktLen = mtu*creditSize;
            } else {
                PktLen = RemainMsgLength;
            }
        }
    }
    if (PktLen == 0) {
        opp_error(" PktLen is generated to 0" );
    }
    return PktLen;
}

void IBGenerator::initialize(){
    if (!ev.isDisabled())
    	ev << "FUNC:  initialize " << endl;
    // init parameters and state variables
    // In non flood mode, parameters:
    burstLength = par("burstLength");
    burstNumber = par("burstNumber");
    burstCounter = burstLength;
    // General parameters
    width = par("width");
    speed = par("speed");
    SL = par("SL");
    srcLid = par("srcLid");
    creditSize = par("creditSize");
    dstHotSpotPerc = par("dstHotSpotPerc");
    mtu = par("mtu");
    parDstLid = par("dstLid");
	msgLengthInMTUs = par("msgLengthInMTUs");
    genStartTime = par("genStartTime");
    genFullTime = par("genFullTime");
    genFullTime += genStartTime;
    logInterval = par("logInterval");
	// work with packets made of 64B credits. Bytes = 64*numCredits
	GenModel = 0;
	msgLengthInAckMTUs = 3; //TODO: move to param
    firstPacket = 1;
    firstPacketSent = 1;
    mtuInMsgIdx = 0;
    dstSeqIdx = 0;
    dstSeqDone = 0;
    packetId = 0;
    msgId    = 0;
    dstLid = 0;
    TimeLastSent = 0;
    AccBytesSent = 0;
    NoneHSBytesSent = 0;
    // Interval logging
    lastIntLogAccBytesSent = 0;
    lastIntLogTime = 0;
    // ppResponse to indicate whether the message is a send or ACK
    ppResponse = 0;
    // if msgLengthInMTUs > 1, mtuAckId to indicate which number of msg
    mtuAckId = 0;
    // sendComplete use to indicate ACK sending message done for ACK_DONE
    sendComplete = 1;
    // dataSendComplete to indicate the message transmission is done before ACK
    dataSendComplete = 0;
    // Destination Sequence Mode
    dstBHS = false;
    // None HotSpot destination pattern mode (when DST_HOT_SPOT)
	HotSpotR = false; // using DST_SEQ*
    //this parameter controls the usage of virtual lane file (src_dst vl mapping)
    virtualLaneFile = par("virtualLaneFile");
    // This parameter controls the usage of dynamic hotspot traffic (true => read file)
    dynamicHSFile = par("dynamicHSFile");

    //bw stats
    curBytesSent = 0;
//    bwStats.setName("Generated Bandwidth");
//    bwVecStats.setName("Generated Bandwidth");

    //intraburst
    slowStart = par("slowStart");
    targetIntraBurstDelay = par("intraBurstDelay");

    if(!slowStart)
        intraBurstDelay = targetIntraBurstDelay;
    else
    {
        intraBurstDelay = slowStart;

        int steps;
        runtime = genFullTime - genStartTime;
        steps = targetIntraBurstDelay - slowStart;
        timeperstep = runtime/steps;
    }

  //  intervalBwVecStats.setName("Interval Generated Bandwidth");

    const char *dstSeqModePar = par("dstSeqMode");
    if (!strcmp(dstSeqModePar, "dstLid")) {
        dstSeqMode = DST_BY_PARAM;
    } else if (!strcmp(dstSeqModePar, "dstSeqOnce")) {
        dstSeqMode = DST_SEQ_ONCE;
    } else if (!strcmp(dstSeqModePar, "dstSeqLoop")) {
        dstSeqMode = DST_SEQ_LOOP;
    } else if (!strcmp(dstSeqModePar, "dstRandom")) {
        dstSeqMode = DST_RANDOM;
    } else if (!strcmp(dstSeqModePar, "dstHotSpot")) {
        dstSeqMode = DST_HOT_SPOT;
    } else if (!strcmp(dstSeqModePar, "dstHotSpotR")) {
        dstSeqMode = DST_HOT_SPOT;
    	HotSpotR = true;
    } else if (!strcmp(dstSeqModePar, "dstBurstHotSpot")) {
        dstSeqMode = DST_HOT_SPOT;
        dstBHS = true;
        dstBHS_start = simTime();
    } else {
        opp_error("Invalid sequence %s\n",dstSeqMode);
    }
    // Traffic Distribution Mode
    const char *trafficDistPar = par("trafficDist");
    if (!strcmp(trafficDistPar,"trfFlood")) {
        trafficDist = TRF_FLOOD;
    } else if (!strcmp(trafficDistPar,"trfUniform")) {
        trafficDist = TRF_UNI;
    } else if (!strcmp(trafficDistPar,"trfLengthMix")) {
        trafficDist = TRF_MIXLEN;
    } else if (!strcmp(trafficDistPar,"trfApp")) {
        trafficDist = TRF_APP;
        dstSeqMode = DST_SEQ_ONCE;
        dstBHS = false;
    } else if (!strcmp(trafficDistPar,"trfPingPong")){
        trafficDist = TRF_PP;
    } else {
        opp_error(" Unknown traffic Distribution " );
    }

    // destination related parameters
    if (dstSeqMode != DST_BY_PARAM) {
        const char *dstSeqVecFile = par("dstSeqVecFile");
        const int   dstSeqIndex = par("dstSeqIndex");
        vecFiles   *vecMgr = vecFiles::get();
        dstSeq = vecMgr->getIntVec(dstSeqVecFile, dstSeqIndex);

        if (dstSeq == NULL) {
            opp_error("fail to obtain dstSeq vector");
        }
        if (!ev.isDisabled())
            ev << "-I- Defined sequence of " << dstSeq->size() << " LIDs" << endl;
    }

    if(virtualLaneFile) {
        const char *vlSeqVecFile = par("vlSeqVecFile");
        const int   vlSeqIndex = par("vlSeqIndex");
        vecFiles   *vecMgr = vecFiles::get();
        vlSeq = vecMgr->getIntVec(vlSeqVecFile, vlSeqIndex);
        if (vlSeq == NULL) {
            opp_error("failed to obtain vlSeq vector %s, %d", vlSeqVecFile, vlSeqIndex);
        }
        else {
        	ev << "-I- " << getFullPath() << " Obtained VL mapping of size:" << vlSeq->size() << endl;
         	ev << "TEST: " << getFullPath() << " ";
                std::copy(vlSeq->begin(), vlSeq->end(), std::ostream_iterator<int>(ev.getOStream(), " "));
                ev << endl;
        }
    }

    if (dstBHS) {
        dstBurstHSDel = par("dstBurstHSDel");
        dstBurstHSDel = dstBurstHSDel *1e-3; // (convert to s)
        dstBurstHSInterDel = par("dstBurstHSInterDel");
        dstBurstHSInterDel = dstBurstHSInterDel *1e-3; // (convert to s)
    }

    // Traffic related parameters
    PktLenTotProb = 0;
    if (trafficDist == TRF_MIXLEN) {
        parseIntListParam("PktLenDist",PktLenDist);
        parseIntListParam("PktLenProb",PktLenProb);
        for (unsigned i = 0; i<PktLenProb.size(); i++) {
            PktLenTotProb += PktLenProb[i];
        }
        if (PktLenDist.size() !=PktLenProb.size() || PktLenDist.empty()) {
            opp_error("Error in ini file: PktLenDist has %d elements, PktLenProb has %d",PktLenDist.size(),PktLenProb.size());
        }
        if (PktLenTotProb == 0) {
            opp_error("PktLen prob all defined with priority 0");
        }
    }

    if (trafficDist == TRF_APP) {
        dstSeqMode = DST_SEQ_ONCE;
        const char *sizeSeqVecFile = par("sizeSeqVecFile");
        const int   sizeSeqIndex =par("dstSeqIndex");
        vecFiles   *vecMgr = vecFiles::get();
        sizeSeq = vecMgr->getIntVec(sizeSeqVecFile, sizeSeqIndex);
        if (sizeSeq == NULL) {
            opp_error("fail to obtain sizeSeq vector");
        }
        if (!ev.isDisabled())
            ev << "-I- Defined sequence of " << sizeSeq->size() << " message size" << endl;
    }

    // Read dynamic hotspot information from file (if present) and schedule
    // the corresponding changes in hotspot traffic pattern
    if(dynamicHSFile) {
        const char *dynHSSeqVecFile = par("dynHSSeqVecFile");
        const int   dynHSSeqIndex =par("dstSeqIndex");
        int size=0;
        std::vector<float> *dynHS; // sequence of HS change triples (time,hotspotlid,percent) for this gen
        vecFiles   *vecMgr = vecFiles::get();

        dynHS = vecMgr->getFloatVec(dynHSSeqVecFile, dynHSSeqIndex);
        if (dynHS == NULL) {
        	throw cRuntimeError("Failed to obtain dynamic hotspot reconfiguration vector, (file %s, index %d)", dynHSSeqVecFile, dynHSSeqIndex);
        } else {
        	size = dynHS->size();
			if(size%3 == 0) {
				int i=0;
				IBGenReconfig* p_reconfig;

				if (!ev.isDisabled())
					ev << "-I- Read " << (size/3) << " reconfig messages" << endl;

				while(i<size) {
					// we know it is dividable by 3...

				    char name[128];
				    sprintf(name, "dynReconfig-%d-%d", dynHSSeqIndex, i);
				    p_reconfig = new IBGenReconfig(name, IB_GEN_RECONFIG);
				    p_reconfig->setDstLid((*dynHS)[i+1]);
				    p_reconfig->setDstHSPerc((*dynHS)[i+2]);
				    scheduleAt((*dynHS)[i],p_reconfig);
				    i = i + 3;
				}
			} else
				throw cRuntimeError("The dynamic hotspot reconfiguration file %s (index %d) does not have the proper format.", dynHSSeqVecFile, dynHSSeqIndex);
        }
    }  // END dynamicHSFile section

    // Start interval logging
    scheduleAt(simTime() + logInterval, new cMessage(NULL, IB_LOG_TIMER_MSG));

    // Queue first packets to send
    if (trafficDist == TRF_FLOOD || trafficDist == TRF_APP) {
        ev << "-I- " << getFullPath() << " working in flood/application mode" << endl;
        if(!virtualLaneFile) {
            const char *floodVLsStr = par("floodVLs");
            char *tmpBuf = new char[strlen(floodVLsStr)+1];
            strcpy(tmpBuf, floodVLsStr);
            char *vlStr = strtok(tmpBuf, " ,");
            while (vlStr) {
                floodVLQ(atoi(vlStr),1);
                vlStr = strtok(NULL, " ,");
            }
        }

        //it
        else {
            std::map<int,bool> alreadyFlooded;

            for(std::vector<int>::iterator it = vlSeq->begin(); it!=vlSeq->end(); ++it) {
                if(*it != 255 && alreadyFlooded.at(*it)==false){ //not flooding self, not flooding already flooded
                	alreadyFlooded[*it]=true;
                	floodVLQ(*it,1);
                }
                else
                    continue;
            }

            /*const char *floodVLsStr = par("floodVLs");
            char *tmpBuf = new char[strlen(floodVLsStr)+1];
            strcpy(tmpBuf, floodVLsStr);
            char *vlStr = strtok(tmpBuf, " ,");
            while (vlStr) {
                floodVLQ(atoi(vlStr),1);
                vlStr = strtok(NULL, " ,");
            }*/

        }

    } else {
        // The delay from one credit to the other in usec is defined as
        // 1/1gbps * 1usec * 10bits/byte * numBytes / ifc-width / ifc-speed-gbps
        genDelay_us = 1.0e-3 * 10 * creditSize / width / speed;

        // set starting packet parameters
        initIBPacketParams();

        // schedule first packet of first burst
        if (burstNumber>0 || (trafficDist == TRF_FLOOD) || (trafficDist == TRF_APP) || (trafficDist == TRF_PP)) {
            scheduleAt(simTime()+genStartTime, new cMessage);
        }
    }
}

// find the VLA and check it HoQ is free...
// NB!!! If you call this method and it returns 'true' you NEED(!!) to send
// data afterwards as the corresponding HoQ at the vlarb is then reserved!.
// If you fail to do some, you may deadlock/starve the generator!
int IBGenerator::isRemoteHoQFree(int vl){

    if (!ev.isDisabled())
    	ev << "FUNC:  isRemoteHoQFree " << endl;

    if (!ev.isDisabled())
    	ev << "packetVL in isRemoteHoQFree:" << vl << endl;

    // find the VLA connected to the given port and
    // call its method for checking and setting HoQ
    cGate *p_gate = gate("out")->getPathEndGate();
    IBVLArb *p_vla = dynamic_cast<IBVLArb *>(p_gate->getOwnerModule());
    if ((p_vla == NULL) || strcmp(p_vla->getName(), "vlarb")) {
        ev << "-E- " << getFullPath() << " cannot get VLA for out port" << endl;
        ev.flush();
        exit(3);
    }

    int remotePortNum = p_gate->getIndex();
    return(p_vla->isHoQFree(remotePortNum, vl));
}

unsigned IBGenerator::getIntraBurstDelay()
{
    if (!ev.isDisabled())
    	ev << "FUNC:  getIntraBurstDelay " << endl;
    if(slowStart && targetIntraBurstDelay < intraBurstDelay)
    {
        /* calculate the new intraBurstDelay */
        intraBurstDelay = max(targetIntraBurstDelay,
                slowStart - int((simTime()-starttime)/timeperstep));
    }

    return intraBurstDelay;
}

void IBGenerator::sendDataOut(IBDataMsg *p_msg)
{
    if (!ev.isDisabled())
    	ev << "FUNC:  sendDataOut " << endl;

    if (!ev.isDisabled())
    	ev << "packetVL in sendDataOut:" << p_msg->getVL() << endl;

    simtime_t delay = 0;
    // In case we are in flood mode, we want to be able
    // to send packet with delay between them. So far the intraburst delay
    // was not use in flood mode. This is our way to use it
    if (trafficDist == TRF_FLOOD || trafficDist == TRF_APP) {
        if(p_msg->getCreditSn() == 0)
        {
            delay = getIntraBurstDelay();   // in nsec
            delay *= p_msg->getPacketLength()/3;
        }
        else
            delay = 0;

        TimeLastSent = simTime() + delay*1e-9;
    }
    else if(trafficDist == TRF_UNI)
    {
        TimeLastSent = simTime();
        delay = 0;
    }

    ev << "-I- remain " << RemainMsgLength << " max " << MsgLength << " packetlenbytes " << p_msg->getPacketLengthBytes() << endl;

    // first packet in message
    if(RemainMsgLength == MsgLength)
    {
        if(!firstPacketSent && p_msg->getCreditSn() == 0)
        {
            msgId = getMsgId();
            /* first packet in burst */
            // calculate BW
            double mBW;

            mBW = curBytesSent / (simTime() - bwLastSent);
            ev << "-I- " << getFullPath() << " curgenbw " << mBW << endl;
          //  bwStats.collect(mBW);
        //    bwVecStats.record(mBW);
            curBytesSent = 0;
            bwLastSent = simTime();

            delay += par("interBurstDelay");
            ev << "-I- " << getFullPath() << " resetting start time " << endl;
            MsgStartTime = simTime() + delay*1e-9;
        }
    }

    // store remaining bytes
    p_msg->setMsgRemaining(RemainMsgLength - p_msg->getPacketLengthBytes());

    // time stamp to enable tracking time in Fabric
    p_msg->setMsgTimestamp(MsgStartTime);
    p_msg->setTimestamp(TimeLastSent);
    assert(delay >= 0);
    sendDelayed(p_msg, delay*1e-9, "out");

    if (!ev.isDisabled())
        ev << "-I- " << getFullPath()
        << " sending " << p_msg->getName()
        << " packetlength(B):" << p_msg->getPacketLengthBytes()
        << " creditsn:" << p_msg->getCreditSn()
        << " dstLid:" << p_msg->getDstLid()
        << " vl:" << p_msg->getVL()
        << " @time(ns):" << SIMTIME_STR((simTime()+delay*1e-9))
        << endl;

    // For oBW calculations
    if (firstPacketSent) {
        firstPacketSent = 0;
        FirstPktSendTime = bwLastSent = lastIntLogTime = simTime();
    }
    AccBytesSent += p_msg->getBitLength()/8;
    curBytesSent += p_msg->getBitLength()/8;

    // Keeping track of the amount of traffic sent to !HS nodes
    if(dstSeqMode == DST_HOT_SPOT && p_msg->getDstLid() != parDstLid) {
    	NoneHSBytesSent += p_msg->getBitLength()/8;
    }
}

void IBGenerator::handlePushPP(IBDataMsg *p_msg){
    if (!ev.isDisabled())
    	ev << "FUNC:  handlePushPP " << endl;

	IBDataMsg *p_cred;
    simtime_t delay;

	if (trafficDist == TRF_PP) {
        if (!ev.isDisabled()) {
			ev << " ----This is send/receive model---- " << endl;
			ev << " srcLid : " << p_msg->getSrcLid() << endl;
			ev << " dataSendComplete : " << dataSendComplete << endl;
			ev << " sendComplete : " << sendComplete << endl;
			ev << " creditCounter : " << creditCounter << endl;
        }
        if (p_msg->getPpResponse()) { // if this is ACK message
        	if ((dataSendComplete) || ((mtuInMsgIdx == 0) && (creditCounter == 0))) { // if current data sending is done

				if (p_msg->getSinkToGen() && (sendComplete == 0)) { // when ACK message is in progress and received another package from the other nodes
	        		delay = 2*1e-3;
	        		delay += 1.0e-3 * 10 * creditSize*8 / 8 / width / speed;
	        		scheduleAt(simTime()+delay*1e-6, p_msg);		// keep looping until the previous ACK messages are done
				} else if (p_msg->getSinkToGen()&& sendComplete) { // If current data sending is done, and required to ACK the other end nodes through notification from Sink
        			p_msg->setSinkToGen(0); // proceed with the rest of the ACK data
        			sendComplete = 0;  // sending ACK data in progress
					scheduleAt(simTime()+delay*1e-6, p_msg);
        		} else {
					p_cred = getNewAckMsg(p_msg);  // create new ACK message to the source
					delay = 1.0e-3 * 10 * p_cred->getBitLength() / 8 / width / speed;
// WL - for single packet ACK
					if ((mtuAckId) == msgLengthInAckMTUs-1)
						p_cred->setEoMsg(1);   // if this is last packet of the message
					else if ((mtuAckId == 0) && (mtuAckId != msgLengthInAckMTUs-1))
						p_cred->setEoMsg(0);// if this is not the last packet of the message
					// is the Q free?
					if (Q[packetVL].empty() && isRemoteHoQFree(packetVL)) {
						sendDataOut(p_cred);
					} else {
						// we can not send so we Q
						if (!ev.isDisabled())
							ev << "-I- " << getFullPath()
							<< " queue packet:" << p_cred->getName()
							<< " Time:" << simTime()
							<< endl;

						Q[packetVL].insert(p_cred);
					}
					// Do we need a new IB packet?
					ev << " mtuAckId : " << mtuAckId << " msgLengthInMTUs :" << msgLengthInMTUs << endl;
					if (++creditCounter == packetLength) {
						creditCounter = 0;
						p_msg->setPacketId(getPacketId());
						if (mtuAckId+1 != msgLengthInAckMTUs) {
							sendComplete = 0;
							mtuAckId++;
							ev << " handlePushPP -- burstCounter : " << burstCounter << " burstNumber :" << burstNumber << endl;
							scheduleAt(simTime()+delay*1e-6, p_msg);
						} else {
							mtuAckId =0;
							sendComplete = 1;
						}
					} else {
						scheduleAt(simTime()+delay*1e-6, p_msg);
					}
        		}
        	} else {
        		delay = 2*1e-3;
        		delay += 1.0e-3 * 10 * creditSize*8 / 8 / width / speed;
        		scheduleAt(simTime()+delay*1e-6, p_msg);
        	}
        } else { // if has received the ACK message from the destination
        	if ((sendComplete)|| ((creditCounter == 0) && (mtuAckId == 0))) { // if ACK message has finished
        		dataSendComplete = 0;
        		handlePush(p_msg); // proceed with new destination in the vector  file
        	} else { // wait until the ACK message finished sending
        		delay = 20*1e-3;
        		delay += 1.0e-3 * 10 * creditSize*8 / 8 / width / speed;
        		scheduleAt(simTime()+delay*1e-6, p_msg);
        	}
        }
	} else
		delete p_msg;
}

// push is an internal event for generating a new packet
// should only be happening if floodMode == 0
void IBGenerator::handlePush(cMessage *p_msg){

    if (!ev.isDisabled())
    	ev << "FUNC:  handlePush " << endl;

    if (!ev.isDisabled())
    	ev << "packetVL in handlePush:" << packetVL << endl;

    if (trafficDist == TRF_FLOOD || trafficDist == TRF_APP) {
        opp_error("handlePush called with TRF_FLOOD / TRF_APP");
    }
    simtime_t delay;
    IBDataMsg *p_cred;

    //ev << "-I- " << " mtuInMsgIdx: " << mtuInMsgIdx << endl;
    // get the new credit
    if ((p_msg->getKind()== IB_DATA_MSG) && (trafficDist == TRF_PP))  { // if data from handlePushPP - means need to proceed to the next data in the vector list
        dataSendComplete = 0; // sending data is in progress
        initIBPacketParams();
        p_msg = new cMessage(); // proceed to the next message
        //ev << " handlePush -- Data Msg -- burstCounter : " << burstCounter << " burstNumber :" << burstNumber << endl;
        scheduleAt(simTime(), p_msg);
    } else {
        dataSendComplete = 0;
        p_cred = getNewDataMsg();
        //ev << "-I- " << " Dest LID: " << p_cred->getDstLid() << endl;
        // indicate this is the last packet of the message
        if (((mtuInMsgIdx) == msgLengthInMTUs-1) && (trafficDist == TRF_PP))
            p_cred->setEoMsg(1);
        // indicate this is still not the last packet of the message
        if ((mtuInMsgIdx < msgLengthInMTUs-1) && (trafficDist == TRF_PP))
            p_cred->setEoMsg(0);
        delay = 1.0e-3 * 10 * p_cred->getBitLength() / 8 / width / speed;

        // is the Q free?
        if (Q[packetVL].empty() && isRemoteHoQFree(packetVL)) {
            sendDataOut(p_cred);
        } else {
            // we can not send so we Q
            if (!ev.isDisabled())
                ev << "-I- " << getFullPath()
                    << " queue packet:" << p_cred->getName()
                    << " Time:" << simTime()
                    << endl;

            Q[packetVL].insert(p_cred);
        }
        // Do we need a new IB packet?
        //ev << "-I- " << " creditCounter: " << creditCounter << endl;
        if ((++creditCounter == packetLength)) {
            creditCounter = 0;
            //ev << "-I- " << " enter when  creditCounter == Packet Length : " << creditCounter << endl;
            //ev << "-I- " << " mtuInMsgIdx: " << mtuInMsgIdx << "MsgLength"  << MsgLength << endl;
            if ((mtuInMsgIdx != msgLengthInMTUs-1) || (trafficDist != TRF_PP)) {

                initIBPacketParams();
                //  Is it last packet of burst ?
                //ev << " handlePush -- burstCounter : " << burstCounter << " burstNumber :" << burstNumber << endl;
                if (trafficDist == TRF_PP || trafficDist == TRF_FLOOD)
                {
                    scheduleAt(simTime()+delay*1e-6, p_msg);
                }
                else if (--burstCounter == 0)
                {
                    burstNumber--;

                    // we need to send more burst?
                    if ((trafficDist == TRF_UNI) || trafficDist == TRF_APP || burstNumber>0) {
                        burstCounter = burstLength;
                        delay = par("interBurstDelay");
                        delay = delay*1e-3;
                        delay += 1.0e-3 * 10 * p_cred->getBitLength()/ 8 / width / speed;
                        scheduleAt(simTime()+delay*1e-6, p_msg);
                    }
                }
                else
                {
                    // schedule next sending within burst
                    delay = getIntraBurstDelay();
                    delay = delay*1e-3;
                    delay += 1.0e-3 * 10 * p_cred->getBitLength()/ 8 / width / speed;
                    scheduleAt(simTime()+delay*1e-6, p_msg);
                }
            }
            else
            {
                dataSendComplete = 1;
            }
        }
        else if (creditCounter != packetLength)
        {
            // but not in addition to the current data being sent
            scheduleAt(simTime()+delay*1e-6, p_msg);
        }
    }
}

// keep at least 10 credits on this vL Q
// send the first one if HoQ is empty.
void IBGenerator::floodVLQ(int vl, int duringInit){
    if (!ev.isDisabled())
    	ev << "FUNC:  floodVLQ " << endl;

    if (!ev.isDisabled())
    	ev << "packetVL in floodVLQ:" << vl << endl;
    IBDataMsg *p_cred;
    int wasEmpty;
    int doneMakingAll = 0;
	bool scheduleRefill = false;


    wasEmpty = Q[vl].empty();

    if ((dstSeqMode == DST_SEQ_ONCE) && (dstSeqDone != 0))
        doneMakingAll = 1;
    else
        doneMakingAll = 0;

    while (!doneMakingAll && (Q[vl].length() < 10)) {
    	// generate a new packet

    	if(!initIBPacketParams(vl)) {
    		// initIBPacketParams returns false ONLY IF
    		// cc is enabled AND it was not able to
    		// find a new (if needed) dstLid for our
    		// next Msg that is NOT held back by CC
    		// or NOT breaking the percentage
    		// invariant regarding how much traffic
    		// that should be sent to none HotSpots.
    		break;

    	}

    	// Fill the Q with credits ("flits") belonging to the current packet
    	for (; creditCounter < packetLength; creditCounter++ ) {
    		p_cred = getNewDataMsg();

    		if(p_cred->getVL()!=vl) {
    			if(!ev.isDisabled())
    				ev << "Changed p_cred VL to: " << vl << endl;
    			p_cred->setVL(vl);
    			p_cred->setSL(p_cred->getVL());
			}
    		Q[vl].insert(p_cred);
    	}
    }


    if (duringInit==0) {
    	// floodVLQ called by the handleSent method during regular operation.
    	// That means that the vlarb has just forwarded a flit. Send a new
    	// one if the Q is not empty.

        if (wasEmpty && !Q[vl].empty()) {
        	// During regular operation only pop and send if we filled
        	// an initially empty Q.
        	if(!(TimeLastSent > simTime()) && isRemoteHoQFree(vl)) {  // Is the TimeLastSent check redundant... or needed if more vl's are used?
				// We should send a packet now (to start the gen-vlarb loop again)
            	p_cred = (IBDataMsg *)Q[vl].pop();
                sendDataOut(p_cred);
        	} else {
        		// We have something to send, but are not allowed to do it yet.
        		// We need to try again later...
        		scheduleRefill = true;
        	}
        }

        if(Q[vl].empty()) {
        	// We were not able to put anything into the queue. Try again later
        	scheduleRefill = true;
        }

    } else if (duringInit==2) {
    	// floodVLQ called by the handleGenRefill method.

    	// (Should duringInit==2 be mergede with duringInit==0?)

        if(Q[vl].empty()) {
        	// We were not able to put anything into the queue. Try again later
        	scheduleRefill = true;
        } else {
        	// Q is not empty...:
        	if(TimeLastSent < simTime() && isRemoteHoQFree(vl)) {
        		// We should send something to start the gen-vlarb loop again:
            	p_cred = (IBDataMsg *)Q[vl].pop();
                sendDataOut(p_cred);
        	} else {
        		// We have something to send, but are not allowed to do it yet.
        		// We need to try again later...
        		scheduleRefill = true;
        	}
        }


    } else {
    	// The very first call to the floodVLQ for this vl.
        // We need to schedule somehow a late "sent"
        IBSentMsg *p_sent = new IBSentMsg("self-sent-on-init", IB_SENT_MSG);
        //if(virtualLaneFile && duringInit==0)
        //	p_sent->setVL(vlSeq->at(dstLid));
        //else
        	p_sent->setVL(vl);

        if (!ev.isDisabled())
        	ev << "packetVL in floodVLQ (p_sent)" << p_sent->getVL() << endl;

        scheduleAt(simTime()+10e-9+genStartTime, p_sent);
    }

    if(scheduleRefill) {
    	// Either we have something to send but are not allowed to do it yet,
    	// or we gave up generating a new packet even while the Q is empty:
    	// We need to make sure we try again a little later...
	    char name[128];
	    sprintf(name, "Refill-scheduled-for-vl%d", vl);
    	IBGenFloodRefill *p_refill;

    	p_refill = new IBGenFloodRefill(name, IB_GEN_FLOOD_REFILL);
    	p_refill->setVL(vl);

    	if (!ev.isDisabled())
    		ev << "packetVL in floodVLQ (p_refill due to 'givenUp' and Q being empty) " << p_refill->getVL() << endl;

    	if(TimeLastSent > simTime())
    		scheduleAt(TimeLastSent + 1e-9, p_refill); // We try next nsec after the current packet is sent
    	else
    		scheduleAt(simTime() + 1e-9, p_refill);  // ...or we try next nsec
    }



}

void IBGenerator::handleSent(IBSentMsg *p_sent){

    if (!ev.isDisabled())
    	ev << "FUNC:  handleSent " << endl;

	int vl = p_sent->getVL();

    if (!ev.isDisabled())
    	ev << "packetVL in handleSent" << vl << endl;

	// We can not just send - need to see if the HoQ is free...
	// NOTE : since we LOCK the HoQ when asking if HoQ is free we
	// must make sure we have something to send before we ask about it
	if (!Q[vl].empty()) {
		if (isRemoteHoQFree(vl)) {
			IBDataMsg *p_msg = (IBDataMsg *)Q[vl].pop();
			if (!ev.isDisabled())
				ev << "-I- " << getFullPath() << " de-queue packet:" << p_msg->getName()<< " at time " << simTime() << endl;
			sendDataOut(p_msg);
		} else {
			if (!ev.isDisabled())
				ev << "-I- " << getFullPath() << " HoQ not free for vl:" << vl << endl;
		}
	} else {
		if (!ev.isDisabled())
			ev << "-I- " << getFullPath() << " nothing to send on vl:" << vl << endl;
	}
	if (trafficDist == TRF_FLOOD || trafficDist == TRF_APP) floodVLQ(vl);

	delete p_sent;
}

void IBGenerator::handleIntervalLogging(cMessage *p_msg)
{

    if (!ev.isDisabled())
    	ev << "FUNC:  handleIntervalLogging " << endl;

	simtime_t now = simTime();

	if(lastIntLogTime >= now)
		return; // Nothing new to log/something is rotten

	// The BW for the last interval
	double oBW = ((AccBytesSent - lastIntLogAccBytesSent)/(now - lastIntLogTime));

	//intervalBwVecStats.record(oBW);

	ev << "STAT - INTERVAL (" << logInterval << ") : " << getFullPath() << " Gen Interval Output BW (B/s):  " << oBW << endl;

	// Update bookkeeping variables
	lastIntLogAccBytesSent = AccBytesSent;
	lastIntLogTime = now;

    // Rescheduling interval logging
    scheduleAt(now + logInterval, p_msg);
}

// Handle message reconfiguring the destination lid and percentage of
// trffic going to that lid (the HotSpot)
void IBGenerator::handleGenReconfig(IBGenReconfig *p_reconf)
{
    if (!ev.isDisabled())
    	ev << "FUNC:  handleGenReconfig " << endl;

    parDstLid = p_reconf->getDstLid();
    dstHotSpotPerc = p_reconf->getDstHSPerc();

    delete p_reconf;
}

// Handle message initiating a new refill of the Q when running in
// flooding mode. VL given by the message.
void IBGenerator::handleGenRefill(IBGenFloodRefill *p_refill)
{
    if (!ev.isDisabled())
    	ev << "FUNC:  handleGenRefill " << endl;

    floodVLQ(p_refill->getVL(), 2);

	delete p_refill;
}

void IBGenerator::handleMessage(cMessage *p_msg){

    if (!ev.isDisabled())
    	ev << "FUNC:  handleMessage " << endl;
    int msgType = p_msg->getKind();
    if ( msgType == IB_SENT_MSG ) {
        handleSent((IBSentMsg *)p_msg);
    } else if (msgType == IB_DATA_MSG) {
    	handlePushPP((IBDataMsg *)p_msg);
    } else if (msgType == IB_LOG_TIMER_MSG) {
    	handleIntervalLogging(p_msg);
    } else if (msgType == IB_GEN_RECONFIG) {
    	handleGenReconfig((IBGenReconfig *)p_msg);
    } else if (msgType == IB_GEN_FLOOD_REFILL) {
    	handleGenRefill((IBGenFloodRefill *)p_msg);
    } else {
        handlePush(p_msg);
    }
}


void IBGenerator::finish()
{
    if (!ev.isDisabled())
    	ev << "FUNC:  finish " << endl;

    double oBW = AccBytesSent / (simTime() - FirstPktSendTime);
	char bufHCAGen[128]; // holds the string describing the HCA's gen

//	ev << "STAT: " << getFullPath() << " Generated BW: num/avg/max/std:"
//		<< bwStats.getCount() << " / " << bwStats.getMean() << " / "
//		<< bwStats.getMax() << " / "  << bwStats.getStddev() << endl;

    ev << "STAT: " << getFullPath() << " Gen Output BW (B/s):" << oBW  << endl;
	sprintf(bufHCAGen, "%s Gen Bandwidth (B/s)", getFullPath().c_str());
	recordScalar(bufHCAGen, oBW);
}
