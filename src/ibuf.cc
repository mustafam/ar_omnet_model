//////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2004 Mellanox Technologies LTD. All rights reserved.
//
// This software is available to you under a choice of one of two
// licenses.  You may choose to be licensed under the terms of the GNU
// General Public License (GPL) Version 2, available from the file
// COPYING in the main directory of this source tree, or the
// OpenIB.org BSD license below:
//
//     Redistribution and use in source and binary forms, with or
//     without modification, are permitted provided that the following
//     conditions are met:
//
//      - Redistributions of source code must retain the above
//        copyright notice, this list of conditions and the following
//        disclaimer.
//
//      - Redistributions in binary form must reproduce the above
//        copyright notice, this list of conditions and the following
//        disclaimer in the documentation and/or other materials
//        provided with the distribution.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////////
//
//
// The IBInBuf implements an IB Input Buffer
// See functional description in the header file.
//
#include "ib_m.h"
#include "ibuf.h"
#include "vlarb.h"
#include "ccmgr.h"
#include <vec_file.h>

Define_Module( IBInBuf);

long IBInBuf::getDoneMsgId() {
    static long id = 0;
    return (id++);
}

void IBInBuf::parseIntListParam(char *parName, int numEntries,
        std::vector<int> &out) {
    int cnt = 0;
    const char *str = par(parName);
    char *tmpBuf = new char[strlen(str) + 1];
    strcpy(tmpBuf, str);
    char *entStr = strtok(tmpBuf, " ,");
    while (entStr) {
        cnt++;
        out.push_back(atoi(entStr));
        entStr = strtok(NULL, " ,");
    }
    for (; cnt < numEntries; cnt++)
        out.push_back(0);
}

void IBInBuf::initialize() {
    maxVL = par("maxVL");
    lastAccessCounter = 0;
    Q = new cQueue*[gateSize("out")];
    for (int pn = 0; pn < gateSize("out"); pn++) {
        Q[pn] = new cQueue[maxVL + 1];
    }
    const char* moduleName =
            getParentModule()->getParentModule()->getFullName();
    recordVectors = par("recordVectors");
    maxBeingSent = par("maxBeingSent");
    numPorts = par("numPorts");
    totalBufferSize = par("totalBufferSize");

    width = par("width");
    totalBufferSize = totalBufferSize * width / 4;

    hcaIBuf = par("isHcaIBuf");
    if (hcaIBuf) {
        ev << "-I- " << getFullPath() << " is HCA IBuf" << endl;
        FDB = NULL;
    } else {
        ev << "-I- " << getFullPath() << " is Switch IBuf " << getId() << endl;
        Switch = getParentModule()->getParentModule();
        if (Switch == NULL) {
            opp_error("Could not find grand parent Switch module");
        }
        // RR will be used to select output port based on Round robin policy
        RR = 0;
        ISWDelay = Switch->par("ISWDelay");
        const char *fdbsFile = Switch->par("fdbsVecFile");
        int fdbIdx = Switch->par("fdbIndex");
        vecFiles *vecMgr = vecFiles::get();

        FDB = vecMgr->getIntVec(fdbsFile, fdbIdx);
        if (FDB == NULL) {
            opp_error("-E- Failed to obtain an FDB %s, %d", fdbsFile, fdbIdx);
        } else {
            ev << "-I- " << getFullPath() << " Obtained FDB of size:"
                    << FDB->size() << endl;
        }
    }

    cc = par("cc_enabled");
    ar = par("ar_enabled");
    adaptive_factor = par("adaptive_factor");

    if (cc) {
        cModule *calleeModule = getParentModule()->getSubmodule("ccmgr");
        ccmgr = check_and_cast<CCMgr *>(calleeModule);
    }

    // track how many parallel sends the IBUF do:
    numBeingSent = 0;
    WATCH(numBeingSent);

    // read Max Static parameters
    int totStatic = 0;
    int val;
    for (int vl = 0; vl < maxVL + 1; vl++) {
        char parName[12];
        sprintf(parName, "maxStatic%d", vl);
        val = par(parName);
        val = val * width / 4;
        maxStatic.push_back(val);
        totStatic += val;
    }

    if (totStatic > totalBufferSize) {
        ev << "-E- " << getFullPath() << " can not define total static ("
                << totStatic << ") > totalBufferSize (" << totalBufferSize
                << ")" << endl;
        ev.flush();
        exit(1);
    }

    // Initiazlize the statistical collection elements
    for (int vl = 0; vl < maxVL + 1; vl++) {
        char histName[40];
        sprintf(histName, "Used Static Credits for VL:%d", vl);
        staticUsageHist[vl].setName(histName);
        staticUsageHist[vl].setRangeAutoUpper(0, 10, 1);
    }

    // Initialize the data structures
    for (int vl = 0; vl < maxVL + 1; vl++) {
        ABR.push_back(0);
        staticFree.push_back(maxStatic[vl]);
        sendRxCred(vl, 1e-9);
    }

    WATCH_VECTOR(ABR);
    WATCH_VECTOR(staticFree);
    usedStaticCredits.setName("static credits used");

    // latency.setName("Latency time per flit");
    //fabric_latency.setName("Fabric Latency per packet");
    //bufSize.setName("Buffer size port 0");
    //bufSize_5.setName("Buffer size port 5");
//    bufVec.setName("Bffer 0 size vector");
//    buf_5_Vec.setName("Bffer 5 size vector");
    curPacketId = 0;
    curPacketCredits = 0;
    curPacketVL = -1;
    curPacketOutPort = -1;
}

int IBInBuf::incrBusyUsedPorts() {
    if (numBeingSent < maxBeingSent) {
        numBeingSent++;
        if (!ev.isDisabled())
            ev << "-I- " << getFullPath() << " increase numBeingSent to:"
                    << numBeingSent << endl;
        return 1;
    }
    if (!ev.isDisabled())
        ev << "-I- " << getFullPath() << " already sending:" << numBeingSent
                << endl;

    return 0;
}
;

// calculate FCCL and send to the OBUF
void IBInBuf::sendRxCred(int vl, simtime_t delay = 0) {
    IBRxCredMsg *p_msg = new IBRxCredMsg("rxCred", IB_RXCRED_MSG);
    p_msg->setVL(vl);
    p_msg->setFCCL(ABR[vl] + staticFree[vl]);

    if (delay > 0)
        sendDelayed(p_msg, delay, "rxCred");
    else
        send(p_msg, "rxCred");
}

// Forward the FCCL received in flow control packet to the VLA
void IBInBuf::sendTxCred(int vl, long FCCL) {
    IBTxCredMsg *p_msg = new IBTxCredMsg("txCred", IB_TXCRED_MSG);
    p_msg->setVL(vl);
    p_msg->setFCCL(FCCL);
    send(p_msg, "txCred");
}

// Try to send the HoQ to the VLA
void IBInBuf::updateVLAHoQ(short int portNum, short vl) {
    IBVLArb *p_vla;

    if (Q[portNum][vl].empty())
        return;

    // find the VLA connected to the given port and
    // call its method for checking and setting HoQ
    cGate *p_gate = gate("out", portNum)->getPathEndGate();
    if (!hcaIBuf) {
        int remotePortNum = p_gate->getIndex();
        p_vla = dynamic_cast<IBVLArb *>(p_gate->getOwnerModule());
        if ((p_vla == NULL) || strcmp(p_vla->getName(), "vlarb")) {
            ev << "-E- " << getFullPath() << " fail to get VLA from out port:"
                    << portNum << endl;
            ev.flush();
            exit(3);
        }
        if (!p_vla->isHoQFree(remotePortNum, vl)) {
            return;
        }

        if (!ev.isDisabled())
            ev << "-I- " << getFullPath() << " free HoQ on VLA:"
                    << p_vla->getFullPath() << " port:" << remotePortNum
                    << " vl:" << vl << endl;

    }

    IBDataMsg *p_msg = (IBDataMsg *) Q[portNum][vl].pop();

    if (!hcaIBuf) {
        // If CC enabled, check if congestion state should be toggled
        if (cc) {
            // Data is about to be forwarded to the vla/output port - update the
            // CCMgr (of the _output_ port) about the new fill ratio of the VoQ(vl)...
            p_vla->ccmgr->updateCONG(vl, getParentModule()->getIndex(),
                    Q[portNum][vl].getLength(), maxStatic[vl]);
        }
        sendDelayed(p_msg, ISWDelay * 1e-9, "out", portNum);
    } else {
        send(p_msg, "out", portNum);
    }
}

// Handle Push message
void IBInBuf::handlePush(IBWireMsg *p_msg) {
    int msgType = p_msg->getKind();
    if (msgType == IB_FLOWCTRL_MSG) {
        // FlowControl:
        // * The FCCL is delivered through the TxCred to the VLA
        // * ABR is overwritten with FCTBS (can cause RxCred send to OBUF
        //   FCCL = ABR + FREE is provided to the OBUF through the RxCred)
        IBFlowControl *p_flowMsg = (IBFlowControl *) p_msg;
        int vl = p_flowMsg->getVL();

        if (!ev.isDisabled())
            ev << "-I- " << getFullPath() << " received flow control message:"
                    << p_flowMsg->getName() << " vl:" << vl << " FCTBS:"
                    << p_flowMsg->getFCTBS() << " FCCL:" << p_flowMsg->getFCCL()
                    << endl;

        sendTxCred(vl, p_flowMsg->getFCCL());

        // update ABR and send RxCred
        if (ABR[vl] > p_flowMsg->getFCTBS()) {
            ev << "-E- " << getFullPath() << " how come we have ABR:" << ABR[vl]
                    << " > wire FCTBS" << p_flowMsg->getFCTBS() << "?" << endl;
        } else if (ABR[vl] < p_flowMsg->getFCTBS()) {
            ev << "-W- " << getFullPath() << " how come we have ABR:" << ABR[vl]
                    << " < wire FCTBS" << p_flowMsg->getFCTBS()
                    << " in lossles wires?" << endl;
            ABR[vl] = p_flowMsg->getFCTBS();
        }

        sendRxCred(vl);
        cancelAndDelete(p_msg);
    } else if (msgType == IB_DATA_MSG) {
        // Data Packet:
        IBDataMsg *p_dataMsg = (IBDataMsg *) p_msg;
        if (hcaIBuf) {
            // track the time the credit spent in the network
            simtime_t d = simTime() - p_dataMsg->getTimestamp();
            // These values used for
            /*  const char * n1 = getParentModule()->getName();
             double stimep = simTime().dbl();
             double dd = d.dbl();*/
            //    latency.collect(d);
            if (p_dataMsg->getCreditSn() + 1 == p_dataMsg->getPacketLength()) {
                /*const char * n1 = getParentModule()->getName();
                 int cn = p_dataMsg->getCreditSn();
                 int pl = p_dataMsg->getPacketLength();
                 int msgrem = p_dataMsg->getMsgRemaining();
                 simtime_t d = simTime() - p_dataMsg->getMsgTimestamp();
                 double tst = d.dbl();
                 double stime = simTime().dbl();
                 double ss = p_dataMsg->getMsgTimestamp().dbl();*/
                simtime_t d = simTime() - p_dataMsg->getMsgTimestamp();
                //     fabric_latency.collect(d);
            }
        }
        const char* st = getParentModule()->getParentModule()->getFullName();
        if (p_dataMsg->getCreditSn() == 0) {
            curPacketId = p_dataMsg->getPacketId();
            curPacketCredits = p_dataMsg->getPacketLength();
            curPacketVL = p_dataMsg->getVL();
            unsigned short dLid = p_dataMsg->getDstLid();

            if (dLid == 0) {
                opp_error("Error: dLid should not be 0 for %s",
                        p_dataMsg->getName());
            }

            if ((curPacketVL < 0) || (curPacketVL > maxVL + 1)) {
                ev << "-E- " << getFullPath() << " VL out of range:"
                        << curPacketVL << endl;
                ev.flush();
                exit(1);
            }

            // do we have enough credits?
            if (curPacketCredits > staticFree[curPacketVL]) {
                ev << "-E- " << getFullPath() << " Credits overflow. Required:"
                        << curPacketCredits << " available:"
                        << staticFree[curPacketVL] << endl;
                ev.flush();
                exit(1);
            }

            // lookup out port  on the first credit of a packet
            if (numPorts > 1) {
                if (dLid < FDB->size()) {
                    curPacketOutPort = outputSelectionLogic(dLid, curPacketVL);
                    ev << "-I- " << "already using the queue" << endl;
                } else {
                    // this is an error flow we need to pass the current message
                    // to /dev/null
                    curPacketOutPort = -1;

                }

            } else {
                curPacketOutPort = 0;
            }
        } else {
            // Continuation Credit

            // check the packet is the expected one:
            if (curPacketId != p_dataMsg->getPacketId()) {
                ev << "-E- " << getFullPath() << " got unexpected packet:"
                        << p_dataMsg->getName() << " id:"
                        << p_dataMsg->getPacketId() << " during packet:"
                        << curPacketId << endl;
                ev.flush();
                exit(2);
            }
        }

        // check out port is valid
        if ((curPacketOutPort < 0) || (curPacketOutPort >= numPorts)) {
            ev << "-E- " << getFullPath() << " dropping packet:"
                    << p_dataMsg->getName() << " by FDB mapping to port:"
                    << curPacketOutPort << endl;
            cancelAndDelete(p_dataMsg);
            return;
        }

        // Now consume a credit
        staticFree[curPacketVL]--;
        staticUsageHist[curPacketVL].collect(staticFree[curPacketVL]);
        ABR[curPacketVL]++;
        if (!ev.isDisabled()) {
            ev << "-I- " << getFullPath() << " New Static ABR[" << curPacketVL
                    << "]:" << ABR[curPacketVL] << endl;
            ev << "-I- " << getFullPath() << " static queued msg:"
                    << p_dataMsg->getName() << " vl:" << curPacketVL
                    << ". still free:" << staticFree[curPacketVL] << endl;
        }

        // For every DATA "credit" (not only first one)
        // - Queue the Data in the Q[V]
        Q[curPacketOutPort][curPacketVL].insert(p_dataMsg);
        /*******************************/
//        // collect statistic of output buffer status
//        int sumCreditsForPort = getSumCreditsNumber(0, 0);
//        bufSize.collect(sumCreditsForPort);
//        bufVec.record(sumCreditsForPort);
//        int sumCreditsForPort_5 = getSumCreditsNumber(5, 0);
//        bufSize_5.collect(sumCreditsForPort_5);
//        buf_5_Vec.record(sumCreditsForPort_5);
        /*******************************/
        // If CC enabled, check if congestion state should be toggled
        if (cc && !hcaIBuf) {
            cGate *p_gate = gate("out", curPacketOutPort)->getPathEndGate();
            IBVLArb *p_vla = dynamic_cast<IBVLArb *>(p_gate->getOwnerModule());

            if ((p_vla == NULL) || strcmp(p_vla->getName(), "vlarb")) {
                ev << "-E- " << getFullPath()
                        << " fail to get VLA from out port:" << curPacketOutPort
                        << endl;
                ev.flush();
                exit(3);
            }

            // New data received - update the CCMgr (of the _output_ port the data is headed for)
            // about the new fill ratio of the VoQ(vl)...
            p_vla->ccmgr->updateCONG(curPacketVL, getParentModule()->getIndex(),
                    Q[curPacketOutPort][curPacketVL].getLength(),
                    maxStatic[curPacketVL]);

        }

        // - Send RxCred with updated ABR[VL] and FREE[VL] - only if the sum has
        //   changed which becomes the FCCL of the sent flow control
        sendRxCred(curPacketVL);

        // - If HoQ in the target VLA is empty - send the push event out.
        //   when the last packet is sent the "done" event has to be sent to all output
        //   ports, Note this also dequeue and send
        updateVLAHoQ(curPacketOutPort, curPacketVL);
    } else {
        ev << "-E- " << getFullPath()
                << " push does not know how to handle message:" << msgType
                << endl;
        cancelAndDelete(p_msg);
    }
}

// simple free static credits as required
void IBInBuf::simpleCredFree(int vl) {
    // simply return the static credit first
    if (staticFree[vl] < maxStatic[vl]) {
        staticFree[vl]++;
        // need to update the OBUF we have one free...
        sendRxCred(vl);
    } else {
        opp_error(
                "Error: got a credit leak? trying to add credits to full buffer on vl  %d",
                vl);
    }
}

// Handle Sent Message
// A HoQ was sent by the VLA
void IBInBuf::handleSent(IBSentMsg *p_msg) {
    // first calculate the total used static
    int totalUsedStatics = 0;
    for (int vli = 0; vli < maxVL + 1; vli++) {
        totalUsedStatics += maxStatic[vli] - staticFree[vli];
    }

    if (!ev.isDisabled())
        recordVectors = par("recordVectors");

    if (recordVectors) {
        usedStaticCredits.record(totalUsedStatics);
    }

    // update the free credits acordingly:
    int vl = p_msg->getVL();
    simpleCredFree(vl);

    // Only on switch ibuf we need to do the following...
    if (!hcaIBuf) {
        // if this was the last message we need to schedule a "done"
        // on each of the output ports
        if (p_msg->getWasLast()) {
            // first we decrement the number of outstanding sends
            if (numBeingSent <= 0) {
                ev << "-E- " << getFullPath()
                        << " got last message when numBeingSent:"
                        << numBeingSent << endl;
                ev.flush();
                exit(1);
            }

            numBeingSent--;
            if (!ev.isDisabled())
                ev << "-I- " << getFullPath() << " completed send. down to:"
                        << numBeingSent << " sends" << endl;
            // inform all arbiters we drive
            int numOutPorts = gateSize("out");
            for (int pn = 0; pn < numOutPorts; pn++) {
                char name[32];
                sprintf(name, "done-%ld", getDoneMsgId());
                IBDoneMsg *p_doneMsg = new IBDoneMsg(name, IB_DONE_MSG);
                send(p_doneMsg, "out", pn);
            }
        }

        // if the data was sent we can expect the HoQ to be empty...
        updateVLAHoQ(p_msg->getArrivalGate()->getIndex(), p_msg->getVL());
    }

    cancelAndDelete(p_msg);
}

void IBInBuf::handleMessage(cMessage *p_msg) {
    int msgType = p_msg->getKind();
    if (msgType == IB_SENT_MSG) {
        handleSent((IBSentMsg *) p_msg);
    } else if ((msgType == IB_DATA_MSG) || (msgType == IB_FLOWCTRL_MSG)) {
        if (msgType == IB_DATA_MSG && cc && hcaIBuf) { // CC is active and we've received (a part of) a data packet at an HCA:
            IBDataMsg *d_msg = (IBDataMsg *) p_msg;

            if (d_msg->getCreditSn() == 0) { // We're at the first flit. Do BECN/FECN stuff :)
                if (d_msg->getBECN()) {
                    ccmgr->checkBECN(p_msg);
                    delete p_msg;
                    return;
                } else
                    ccmgr->checkFECN(p_msg);
            }
        }
        handlePush((IBWireMsg*) p_msg);
    } else {
        ev << "-E- " << getFullPath() << " does not know how to handle message:"
                << msgType << endl;
        if (p_msg->isSelfMessage())
            cancelAndDelete(p_msg);
        else
            delete p_msg;
    }
}

short IBInBuf::outputSelectionLogic(unsigned short dLid, int curPacketVL) {
    // 1. Routing function : Supplies a set of suitable routing options to reach the destination
    // 2. Output Selection Function: Choose one port from the set received at 1
    /*
     * A) RR(Cyclic Priority) - Round Robin algorithm
     * B)  LBC(Less Busy Channel) with LRU(Least-recently-used:
     *       a. In the former step, the output physical channel that has the
     *         smallest number of multiplexed packets among available
     *         physical channels is selected as well.
     *       b. When more than one physical channel has
     *         the same number of packets, it selects one with the largest
     *         interval time since the last transferred packet. This
     *         minimize the times of multiplexing in a physical channel.
     *         The link transmission delay is also influenced by the
     *         number of packets being multiplexed on a physical channel. Thus, the a + b, try to not only
     *         distribute the traffic uniformly among physical channels, but
     *         also minimize the link transmission delay.
     * C)  MC (More free credits available): The MC selects the link which has the highest number of credits available
     *
     */

    // A - RR
#if 0
    if (ar) {

        // switch between ports by Round Robin policy.
        // ToDo provide numbef choices, replace hard coded 2
        // switch to different port based on - RR
        // If we don't have multuple options don't increase the RR. That's because
        //  we don't want to affect the adaptivity between real multiple-options.
        int setSize = FDB->size() / adaptive_factor;
        int outPorts[OPTIONS]= {-1};

        for (int i = 0; i < adaptive_factor; i++) {
            outPorts[i] = (*FDB)[setSize * i + dLid];
        }
        // enough to check if first two entries are equal
        if(outPorts[0] == outPorts[1]) {
            // We have one option
            curPacketOutPort = outPorts[0];
        }
        else {
            RR = RR % OPTIONS;
            curPacketOutPort = outPorts[RR];
            RR++;
            // Inform all IBUF with the new RR value.
            propagateRR(RR);
        }
    } else
    // deterministic routing
    curPacketOutPort = (*FDB)[dLid];
#endif

#if 1
    // B -  LBC + LRU
    if (ar) {

        // get output ports corresponding with the dLid
        // Assume we know how many routing options we have - 2
        int setSize = FDB->size() / adaptive_factor;
        int outPort;
        int maxFreeSpots = -1;
        short selection = 0;
        IBVLArb* selectionVlarb;
        int numEuals = 0;
        // get port with maximum free spots
        for (int i = 0; i < adaptive_factor; i++) {
            outPort = (*FDB)[setSize * i + dLid];
            cGate *p_gate = gate("out", outPort)->getPathEndGate();
            IBVLArb *p_vla = dynamic_cast<IBVLArb *>(p_gate->getOwnerModule());
            int freeSpots = p_vla->getFreeSpots(0);
            if (freeSpots > maxFreeSpots) {
                maxFreeSpots = p_vla->getFreeSpots(0);
                selection = outPort;
                selectionVlarb = p_vla;
            }
            else if (freeSpots == maxFreeSpots) {
                numEuals++;
            }
            p_vla->incLRU();
        }
        if(numEuals == adaptive_factor - 1) {
            // All options are equal, select based on LRU
            int maxLRU = -1;
            for (int i = 0; i < adaptive_factor; i++) {
                outPort = (*FDB)[setSize * i + dLid];
                cGate *p_gate = gate("out", outPort)->getPathEndGate();
                IBVLArb *p_vla =
                        dynamic_cast<IBVLArb *>(p_gate->getOwnerModule());
                if (p_vla->getLRU() > maxLRU) {
                    maxLRU = p_vla->getLRU();
                    selection = outPort;
                    selectionVlarb = p_vla;

                }
            }
        }
        curPacketOutPort = selection;
        selectionVlarb->resetLRU();
    }

    else
        // deterministic routing
        curPacketOutPort = (*FDB)[dLid];
#endif


#if 0
    // D -  minimal input buffer(MIB) + LRU
    if (ar) {
        int OPTIONS = 2th the dLid
        // Assume we know how many routing options we have - 2
        int setSize = FDB->size() / adaptive_factor;

        //op3Port = (*FDB)[op3];
        //...
        /***********************************************************/
        /*                                                         */
        /* The code below should be changed to improve performance */
        /*                                                         */
        /***********************************************************/
        // Select output port with more free credits per VL
        // find the VLA connected to the given port and
        // call its method for checking and setting HoQ
        int outPort;
        short selection = -1;
        IBVLArb* selectionVlarb;
        int numEuals = 0;
        // get port with maximum free spots

        int minSumCredits = INT16_MAX;
         for(int i = 0; i<OPTIONS;i++) {
             outPort = (*FDB)[setSize * i + dLid];
             cGate *p_gate = gate("out", outPort)->getPathEndGate();
             IBVLArb *p_vla = dynamic_cast<IBVLArb *>(p_gate->getOwnerModule());
             int sumCreditsForPort = getSumCreditsNumber(outPort, 0);
             if(sumCreditsForPort < minSumCredits) {
                 minSumCredits = sumCreditsForPort;
                 selection = outPort;
                 selectionVlarb = p_vla;
             }
             else if (sumCreditsForPort == minSumCredits) {
                 numEuals++;

             }
             p_vla->incLRU();
         }

        if(numEuals == OPTIONS - 1) {
            // All options are equal, select based on LRU
            int maxLRU = -1;
            for (int i = 0; i < OPTIONS; i++) {
                outPort = (*FDB)[setSize * i + dLid];
                cGate *p_gate = gate("out", outPort)->getPathEndGate();
                IBVLArb *p_vla =
                        dynamic_cast<IBVLArb *>(p_gate->getOwnerModule());
                if (p_vla->getLRU() > maxLRU) {
                    maxLRU = p_vla->getLRU();
                    selection = outPort;
                    selectionVlarb = p_vla;

                }
            }
        }
        curPacketOutPort = selection;
        selectionVlarb->resetLRU();
    }

    else
        // deterministic routing
        curPacketOutPort = (*FDB)[dLid];
#endif


#if 0   // C - MC
    if (ar) {
        int setSize = FDB->size() / adaptive_factor;

        short outPort;
        int minSumCredits = INT16_MAX;
        short selection = -1;
        for(int i = 0; i<adaptive_factor;i++) {
            outPort = (*FDB)[setSize * i + dLid];
            int sumCreditsForPort = getSumCreditsNumber(outPort, 0);
            if(sumCreditsForPort < minSumCredits) {
                minSumCredits = sumCreditsForPort;
                selection = outPort;
            }
        }
        curPacketOutPort = selection;
    }
    else {
        curPacketOutPort = (*FDB)[dLid];
    }
#endif

    // deterministic routing
    return curPacketOutPort;

}

int IBInBuf::getFreeCredits(int vl) {
    return staticFree[vl];
}

void IBInBuf::propagateRR(int RR) {

    for (int i = 0; i < numPorts; i++) {
        // parent=switchPort->parent=switch->another switch port
        cModule *ibufTemp = getParentModule()->getParentModule()->getSubmodule(
                "port", i)->getSubmodule("ibuf");
        IBInBuf *inBuf = check_and_cast<IBInBuf *>(ibufTemp);
        inBuf->RR = RR;
    }
}

int IBInBuf::getSumCreditsNumber(short outputPort, short vl) {

    int creditsNumberSum = 0;
    for (int i = 0; i < numPorts; i++) {
        // parent=switchPort->parent=switch->another switch port->ibuf
        cModule *ibufTemp = getParentModule()->getParentModule()->getSubmodule(
                "port", i)->getSubmodule("ibuf");
        if (ibufTemp == NULL)
            return 0;
        IBInBuf *inBuf = check_and_cast<IBInBuf *>(ibufTemp);
        creditsNumberSum += inBuf->getCreditsNumberForPort(outputPort, 0);
    }
    return creditsNumberSum;
}

cQueue **Q; // Incoming packets Q per VL per out port
void IBInBuf::finish() {
    for (int vl = 0; vl < maxVL + 1; vl++) {
        ev << "STAT: " << getFullPath() << " VL:" << vl;
        ev << " Used Static Credits num/avg/max/std:"
                << staticUsageHist[vl].getCount() << " / "
                << staticUsageHist[vl].getMean() << " / "
                << staticUsageHist[vl].getMax() << " / "
                << staticUsageHist[vl].getStddev() << endl;

    }

}

/*int getCreditsNumberForPort(int outPort, int vl) {
 if(!Q[outPort])
 return 0;
 return Q[outPort][vl].length();
 }*/
